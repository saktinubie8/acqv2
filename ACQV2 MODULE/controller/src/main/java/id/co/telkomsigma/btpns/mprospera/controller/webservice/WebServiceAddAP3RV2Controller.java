package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.product.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.AP3RRequest;
import id.co.telkomsigma.btpns.mprospera.request.FundedThingsReq;
import id.co.telkomsigma.btpns.mprospera.response.AP3RResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webserviceAP3RController")
public class WebServiceAddAP3RV2Controller extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private SWService swService;

    @Autowired
    private AP3RService ap3rService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private UserService userService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    JsonUtils jsonUtils = new JsonUtils();

    /* 
     * @author : Ilham
     * @Since  : 20191126
     * @category patch 
     *   - add default disbursment_date value from request divice or default 19000101 
     */    
    // SUBMIT AP3R DATA API
    @RequestMapping(value = WebGuiConstant.AP3R_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AP3RResponse doAdd(@RequestBody AP3RRequest request,
                       @PathVariable("apkVersion") String apkVersion) throws Exception {

        final AP3RResponse responseCode = new AP3RResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String ap3rId = "";
        try {
            log.info("addAp3r INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else {
                if (request.getAp3rId() != null) {
                    if (!"".equals(request.getAp3rId())) {
                        AP3R ap3r = ap3rService.findByLocalId(request.getAp3rId());
                        if (ap3r != null) {
                            if (request.getAction().equals("insert")) {
                                responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
                                log.error("DATA DUPLICATE");
                                String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_AP3R_ALREADY_EXIST);
                                responseCode.setResponseMessage(label);
                                responseCode.setAp3rId(request.getAp3rId());
                                responseCode.setLocalId(request.getLocalId());
                                responseCode.setStatus(request.getStatus());
                                return responseCode;
                            }
                            // validasi user delete data AP3R jika AP3R tersebut sudah di approved
                            if ("delete".equals(request.getAction()) && ap3r.getStatus()
                                    .equals(WebGuiConstant.STATUS_APPROVED)) {
                                log.error("STATUS AP3R SUDAH APPROVED");
                                responseCode.setResponseCode(WebGuiConstant.RC_AP3R_ALREADY_APPROVED);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                        } else {
                            log.info("SET AP3R DATA");
                            ap3r = new AP3R();
                        }
                        ap3r.setLatitude(request.getLatitude());
                        ap3r.setLongitude(request.getLongitude());
                        if (request.getSwProductId() != null) {
                            if (!request.getSwProductId().equals("")) {
                                SWProductMapping swProductMapLocal = swService
                                        .findProductMapByLocalId(request.getSwProductId());
                                if (swProductMapLocal == null) {
                                    log.error("PRODUCT SW NULL");
                                    responseCode.setResponseCode(WebGuiConstant.RC_SW_PRODUCT_NULL);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                                ap3r.setSwId(swProductMapLocal.getSwId());
                                /*
                                 * set disbursmentdate from request or divice
                                 */
                                SurveyWawancara sw = swService.getSWById(ap3r.getSwId().toString());
                                if(sw.getDisbursementDate() != null){
                                    ap3r.setDisbursementDate(sw.getDisbursementDate());//Tanggal Cair
                                }else {
                                	if(request.getDisburseDate() != null) {
                                		if(!"".equals(request.getDisburseDate())) {
                                    	ap3r.setDisbursementDate(formatter.parse(request.getDisburseDate()));	
                                		}else {
                                    		ap3r.setDisbursementDate(formatter.parse("19000101"));
                                    	}
                                	}else {
                                		ap3r.setDisbursementDate(formatter.parse("19000101"));
                                	}
                                }
                                ap3r.setSwProductId(swProductMapLocal.getMappingId());
                            }
                        }
                        ap3r.setLocalId(request.getAp3rId());
                        if (request.getHaveAccount() != null)
                            if (!request.getHaveAccount().equals(""))
                                ap3r.setHasSaving(request.getHaveAccount().charAt(0));// FLAG MEMILIKI TABUNGAN ATAU TIDAK
                        ap3r.setOpenSavingReason(request.getAccountPurpose());// TUJUAN BUKA TABUNGAN
                        if (request.getFundSource() != null)
                            if (!request.getFundSource().equals(""))
                                ap3r.setFundSource(request.getFundSource());// SUMBER DANA
                        if (request.getYearlyTransaction() != null)
                            if (!request.getYearlyTransaction().equals(""))
                                ap3r.setTransactionInYear(request.getYearlyTransaction().charAt(0));// TRANSAKSI SETAHUN
                        if (request.getFinancingPurpose() != null)
                            if (!request.getFinancingPurpose().equals(""))
                                ap3r.setLoanReason(request.getFinancingPurpose());// TUJUAN PEMBIAYAAN
                        ap3r.setBusinessField(request.getBusinessTypeId());// JENIS USAHA
                        if (request.getHighRiskCustomer() != null)
                            if (!request.getHighRiskCustomer().equals(""))
                                ap3r.setHighRiskCustomer(request.getHighRiskCustomer().charAt(0));// HIGH RISK CUSTOMER
                        if (request.getHighRiskBusiness() != null)
                            if (!request.getHighRiskBusiness().equals(""))
                                ap3r.setHighRiskBusiness(request.getHighRiskBusiness().charAt(0));// HIGH RISK BUSINESS
                        ap3r.setStatus(WebGuiConstant.STATUS_APPROVED);
                        ap3r.setCreatedBy(request.getUsername());
                        ap3r.setCreatedDate(new Date());
                        if (request.getDueDate() != null)
                            if (!request.getDueDate().equals(""))
                                ap3r.setDueDate(formatter.parse(request.getDueDate()));// TANGGAL JATUH TEMPO
                        if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                            if (request.getAction().equals("delete")) {
                                List<FundedThings> fundedThings = ap3rService.findByAp3rId(ap3r);
                                for (FundedThings fundThing : fundedThings) {
                                    ap3rService.delete(fundThing);
                                }
                                ap3r.setIsDeleted(true);
                                ap3r.setUpdatedDate(new Date());
                                ap3r.setUpdatedBy(request.getUsername());
                            }
                            ap3r.setUpdatedDate(new Date());
                            ap3r.setUpdatedBy(request.getUsername());
                        }
                        ap3rService.save(ap3r);
                        ap3rId = ap3r.getLocalId();
                        if (request.getAction().equals("insert")) {
                            for (FundedThingsReq product : request.getFinancedGoods()) {
                                FundedThings productAdd = new FundedThings();
                                productAdd.setProductName(product.getName());
                                if (product.getPrice() != null || product.getPrice().equals(""))
                                    productAdd.setPrice(new BigDecimal(product.getPrice()));
                                productAdd.setAp3rId(ap3r);
                                ap3rService.save(productAdd);
                            }
                        }
                        if (request.getAction().equals("update")) {
                            List<FundedThings> fundedThings = ap3rService.findByAp3rId(ap3r);
                            for (FundedThings fundThing : fundedThings) {
                                ap3rService.delete(fundThing);
                            }
                            for (FundedThingsReq product : request.getFinancedGoods()) {
                                FundedThings productAdd = new FundedThings();
                                productAdd.setProductName(product.getName());
                                if (product.getPrice() != null || product.getPrice().equals(""))
                                    productAdd.setPrice(new BigDecimal(product.getPrice()));
                                productAdd.setAp3rId(ap3r);
                                ap3rService.save(productAdd);
                            }
                        }
                        responseCode.setStatus(ap3r.getStatus());
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.RC_AP3R_NULL);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_AP3R_NULL);
                }
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            responseCode.setAp3rId(ap3rId);
            responseCode.setLocalId(request.getLocalId());
        } catch (Exception e) {
            log.error("addAp3r error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("addAp3r error: " + e.getMessage());
        } finally {
            try {
                log.info("Trying to CREATE Terminal Activity....");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_AP3R);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_AP3R);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        if (!request.toString().equals(null) && !(request.toString().length() < 1000)
                                || !request.toString().equals("") && !(request.toString().length() < 1000))
                            auditLog.setDescription(
                                    request.toString().substring(0, 1000) + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.info("AUDIT LOG LOGGING SUCCESS...");
                        log.info("Updating AUDIT LOG");
                    }
                });
                log.info("addAp3r RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("addAp3r saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

}