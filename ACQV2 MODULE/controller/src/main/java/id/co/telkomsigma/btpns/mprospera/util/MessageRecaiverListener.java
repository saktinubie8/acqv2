package id.co.telkomsigma.btpns.mprospera.util;

import java.math.BigDecimal;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineNotif;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.product.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sw.DataNOS;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwForEmail;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.MessageNosPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.ScoringRuleNosPojo;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SentEmailNotifAfterNosPOJO;
import id.co.telkomsigma.btpns.mprospera.service.CustomerService;
import id.co.telkomsigma.btpns.mprospera.service.DataNosService;
import id.co.telkomsigma.btpns.mprospera.service.LocationService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.SentraService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import org.springframework.context.annotation.Bean;

/*
 * @author : Ilham
 * @Since  : 20190220 
 */

@Component
public class MessageRecaiverListener {

    @Autowired
    private DataNosService dataNosService;

    @Autowired
    private SWService swService;

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private LocationService locationService;

    protected final Log LOGGER = LogFactory.getLog(getClass());

    JsonUtils jsonUtils = new JsonUtils();

    @Bean
    public String nosScoringResult() {
        return dataNosService.findByParamName(WebGuiConstant.PARAM_LISTENER).getParamValue();
    }
    
    /*
     * @author : Ilham
     * @Since  : 20191126
     * @category patch 
     *   - Penambahan Timestamp untuk email  
     */
    @JmsListener(destination = "#{@nosScoringResult}")
    public String receiveMessage(final Message jsonMessage) throws JMSException {
        String messageData = null;
        LOGGER.info("Received message " + jsonMessage);
        String response = null;
        int maxRetry = 0 ;
        try {
            if (jsonMessage instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) jsonMessage;
                messageData = textMessage.getText();
                LOGGER.debug("Received Message is : " + textMessage.getText());
                // String to json objek
                String nosmeseage = textMessage.getText();
                Object obj = new JsonUtils().fromJson(nosmeseage, MessageNosPOJO.class);
                LOGGER.info("From ActiveMQ : " + jsonUtils.toJson(nosmeseage));
                // -----save to DB
                final MessageNosPOJO messageFromNos = (MessageNosPOJO) obj;
                DataNOS messageNOStoDB = new DataNOS();
                messageNOStoDB.setFormatJson(nosmeseage);
                messageNOStoDB.setRetrievalReferenceNumber(messageFromNos.getRetrievalReferenceNumber());
                messageNOStoDB.setStatus("not sent");
                dataNosService.save(messageNOStoDB);
                // -----update status, sw for email 'SCORING DONE'
                SwForEmail updateStatus = dataNosService.findByFlagStatusScoring(WebGuiConstant.SCORING_WAITING,
                        messageFromNos.getRetrievalReferenceNumber());
                if (updateStatus.getRetrievalReferenceNumber().equals(messageFromNos.getRetrievalReferenceNumber())) {
                    dataNosService.update(updateStatus);
                    LOGGER.debug("Data Sudah terUpdate");
                }
                //
                SurveyWawancara findSWwithRrn = dataNosService.findByRrn(messageFromNos.getRetrievalReferenceNumber());
                User findUser = userService.findUserByUsername(findSWwithRrn.getCreatedBy());
                Customer findIdCustomer = customerService.findById(findSWwithRrn.getCustomerId().toString());
                Group findsentraId = sentraService.findById(findIdCustomer.getGroup().getGroupId().toString());
                Location findMms = locationService.findByLocationId(findIdCustomer.getSentraLocation().getLocationId().toString());
                List<User> findUsername = userService.findUsernameCoandBm
                        (findIdCustomer.getSentraLocation().getLocationId().toString(), WebGuiConstant.STATUS_ROLE_USER, findSWwithRrn.getCreatedBy());
                //kondisi Approved/Rejected
                if ("APPROVED".equals(messageFromNos.getStatus())) {
                    LOGGER.debug("SEND EMAIL APPROVED");
                    //List<User> findEmail = userService.findEmail(findUser.getOfficeCode(), WebGuiConstant.STATUS_ROLE_USER);
                    SWSentEmailPOJO requ = new SWSentEmailPOJO();
                    SystemParameter valueParam = swService.findByparamName(WebGuiConstant.PLAFON_PARAM);
                    Long nilaiParam = Long.valueOf(valueParam.getParamValue());
                    SWProductMapping productRecPlacon = swService.findByTopBySwid(findSWwithRrn.getSwId());
                    BigDecimal plafonrec = productRecPlacon.getRecommendedPlafon();
                    Long convertPlafonrec = plafonrec.longValue();
                    requ.setTransmissionDateAndTime(null);
                    requ.setRetrievalReferenceNumber(findSWwithRrn.getRrn());
                    requ.setUserName(findSWwithRrn.getCreatedBy());
                    Long swId = findSWwithRrn.getSwId();
                    requ.setSwId(String.valueOf(swId));
                    for (User userEmail : findUsername) {
                        BigDecimal slimitUser = new BigDecimal(userEmail.getLimit() == null ? "0.0000" : userEmail.getLimit());
                        LOGGER.debug("Limit String User : " + slimitUser);
                        Long limitUser = slimitUser.longValue();
                        LOGGER.debug("Limit per User : " + limitUser);
                        LOGGER.debug("convertPlafonrec = " + convertPlafonrec);
                        LOGGER.debug("nilaiParam       = " + nilaiParam);
                        if (userEmail.getLimit() != null) {
                            if (convertPlafonrec > limitUser) {
                                requ.setIsForwarded(true);
                            } else {
                                requ.setIsForwarded(false);
                            }
                        } else {
                            if (convertPlafonrec > nilaiParam) {
                                requ.setIsForwarded(true);
                            } else {
                                requ.setIsForwarded(false);
                            }
                        }
                        LOGGER.debug("REQUEST ISFORWARDED             : " + requ.getIsForwarded());
                        String email = userEmail.getEmail();
                        requ.setEmailAddress(email);
                        requ.setApprovalUsername(userEmail.getUsername());
                        if ("APPROVED".equals(messageFromNos.getStatus())) {
                            requ.setNote(messageFromNos.getStatus());
                        }
                        requ.setKodeDeviasi("");
                        requ.setIsInReview(false);
                        String mailResponse = dataNosService.generateNotSend(requ);
                        LOGGER.debug("RESPONSE : " + mailResponse);
                        if (swService.hasIdPhoto(findSWwithRrn.getSwId().toString()).equals(false) &&
                                swService.hasSurveyPhoto(findSWwithRrn.getSwId().toString()).equals(false)) {
                            LOGGER.error("Photo Not Found");
                            //save to retry for scheduler
                            MailEngineRetail swRetail = new MailEngineRetail();
                            swRetail.setStatus("not sent");
                            swRetail.setSwId(Long.valueOf(requ.getSwId()));
                            swRetail.setUsername(userEmail.getUsername());
                            swRetail.setRetrievalReferenceNumber(findSWwithRrn.getRrn());
                            swRetail.setEmailAddres(email);
                            swRetail.setIsForwarded(requ.getIsForwarded());
                            swRetail.setCustumerName(findSWwithRrn.getCustomerName());
                            swRetail.setTransmissionDateAndTime(null);
                            swRetail.setApprovalUsername(requ.getApprovalUsername());
                            swRetail.setNote(requ.getNote());
                            swRetail.setKodeDeviasi(requ.getKodeDeviasi());
                            swRetail.setInReview(requ.getIsInReview());
                            swRetail.setMaxRetry(maxRetry);
                            swRetail.setCreatedt(new Date());
                            swRetail.setCreateby(findSWwithRrn.getCreatedBy());
                            swRetail.setUpdateby(null);
                            swRetail.setUpdatedt(null);
                            LOGGER.info("JSON SAVE SCEDULER "+jsonUtils.toJson(swRetail));
                            swService.save(swRetail);
                            LOGGER.debug("Send Email Notif Unkown Foto");
                        } else if (mailResponse.contains(WebGuiConstant.RC_SUCCESS)) {
                            LOGGER.info("Send Email : " + WebGuiConstant.RC_SUCCESS + jsonUtils.toJson(requ));
                        } else {
                            LOGGER.error("Send Email : " + WebGuiConstant.RC_GENERAL_ERROR + jsonUtils.toJson(requ));
                            MailEngineRetail swRetail = new MailEngineRetail();
                            swRetail.setStatus("not sent");
                            swRetail.setSwId(Long.valueOf(requ.getSwId()));
                            swRetail.setUsername(requ.getUserName());
                            swRetail.setRetrievalReferenceNumber(requ.getRetrievalReferenceNumber());
                            swRetail.setEmailAddres(email);
                            swRetail.setIsForwarded(requ.getIsForwarded());
                            swRetail.setCustumerName(requ.getCustomerName());
                            swRetail.setTransmissionDateAndTime(requ.getTransmissionDateAndTime());
                            swRetail.setApprovalUsername(requ.getApprovalUsername());
                            swRetail.setNote(requ.getNote());
                            swRetail.setKodeDeviasi(requ.getKodeDeviasi());
                            swRetail.setInReview(requ.getIsInReview());
                            swRetail.setMaxRetry(maxRetry);
                            swRetail.setCreatedt(new Date());
                            swRetail.setCreateby(findSWwithRrn.getCreatedBy());
                            swRetail.setUpdateby(null);
                            swRetail.setUpdatedt(null);
                            LOGGER.info("JSON SAVE SCEDULER "+jsonUtils.toJson(swRetail));
                            swService.save(swRetail);
                        }
                    }
                    findSWwithRrn.setIsNeedDevition(false);
                    swService.save(findSWwithRrn);
                } else {
                    LOGGER.info("Send Email With Deviation");
                    HashMap<String, String> scoringResoutltMap = new HashMap<String, String>();
                    List<ScoringRuleNosPojo> scoringParamList = messageFromNos.getScoringResult();
                    for (ScoringRuleNosPojo scroParam : scoringParamList) {
                        scoringResoutltMap.put(scroParam.getScoringRule(), scroParam.getResult());
                    }
                    List<ScoringRuleNosPojo> scoringResultFromNos = messageFromNos.getScoringResult();
                    if (("FAILED(Autoreject)".equals(scoringResoutltMap.get("MIN_PRS_DISBURSEMENT_PLAN")))
                            || ("FAILED(Autoreject)".equals(scoringResoutltMap.get("MAX_PLAFOND_HAS_BUSINESS")))) {
                        LOGGER.info("SEND EMAIL DEVISIASI");
                        List<User> findEmail = userService.findEmail(findUser.getOfficeCode(), WebGuiConstant.STATUS_ROLE_USER);
                        SWSentEmailPOJO requ = new SWSentEmailPOJO();
                        SystemParameter valueParam = swService.findByparamName(WebGuiConstant.PLAFON_PARAM);
                        Long nilaiParam = Long.valueOf(valueParam.getParamValue());
                        SWProductMapping productRecPlacon = swService.findByTopBySwid(findSWwithRrn.getSwId());
                        BigDecimal plafonrec = productRecPlacon.getRecommendedPlafon();
                        Long convertPlafonrec = plafonrec.longValue();
                        requ.setTransmissionDateAndTime(null);
                        requ.setRetrievalReferenceNumber(findSWwithRrn.getRrn());
                        requ.setUserName(findSWwithRrn.getCreatedBy());
                        Long swId = findSWwithRrn.getSwId();
                        requ.setSwId(String.valueOf(swId));
                        for (User userEmail : findEmail) {
                            String email = userEmail.getEmail();
                            BigDecimal slimitUser = new BigDecimal(userEmail.getLimit() == null ? "0.0000" : userEmail.getLimit());
                            LOGGER.debug("Limit String User : " + slimitUser);
                            Long limitUser = slimitUser.longValue();
                            LOGGER.debug("Limit per User : " + limitUser);
                            LOGGER.debug("convertPlafonrec = " + convertPlafonrec);
                            LOGGER.debug("nilaiParam       = " + nilaiParam);
                            if (userEmail.getLimit() != null) {
                                if (convertPlafonrec > limitUser) {
                                    requ.setIsForwarded(true);
                                } else {
                                    requ.setIsForwarded(false);
                                }
                            } else {
                                if (convertPlafonrec > nilaiParam) {
                                    requ.setIsForwarded(true);
                                } else {
                                    requ.setIsForwarded(false);
                                }
                            }
                            LOGGER.debug("REQUEST ISFORWARDED             : " + requ.getIsForwarded());
                            requ.setEmailAddress(email);
                            requ.setApprovalUsername(userEmail.getUsername());
                            String note = "";
                            for (ScoringRuleNosPojo ws : scoringResultFromNos) {
                                if (ws.getResult().equals("FAILED(Autoreject)") || ws.getResult().equals("FAILED")) {
                                    note = note + ", " + ws.getRejectDesc();
                                    requ.setKodeDeviasi(ws.getDeviationCode());
                                }
                            }
                            requ.setIsInReview(true);
                            requ.setNote(note);
                            String mailResponse = dataNosService.generateNotSend(requ);
                            LOGGER.debug("RESPONSE : " + mailResponse);
                            if (swService.hasIdPhoto(findSWwithRrn.getSwId().toString()).equals(false) &&
                                    swService.hasSurveyPhoto(findSWwithRrn.getSwId().toString()).equals(false)) {
                                LOGGER.error("Photo Not Found");
                                //save email
                                MailEngineRetail swRetail = new MailEngineRetail();
                                swRetail.setStatus("not sent");
                                swRetail.setSwId(Long.valueOf(requ.getSwId()));
                                swRetail.setUsername(userEmail.getUsername());
                                swRetail.setRetrievalReferenceNumber(findSWwithRrn.getRrn());
                                swRetail.setEmailAddres(email);
                                swRetail.setIsForwarded(requ.getIsForwarded());
                                swRetail.setCustumerName(findSWwithRrn.getCustomerName());
                                swRetail.setTransmissionDateAndTime(null);
                                swRetail.setApprovalUsername(requ.getApprovalUsername());
                                swRetail.setNote(requ.getNote());
                                swRetail.setKodeDeviasi(requ.getKodeDeviasi());
                                swRetail.setInReview(requ.getIsInReview());
                                swRetail.setMaxRetry(maxRetry);
                                swRetail.setCreatedt(new Date());
                                swRetail.setCreateby(findSWwithRrn.getCreatedBy());
                                swRetail.setUpdateby(null);
                                swRetail.setUpdatedt(null);
                                swService.save(swRetail);
                            } else if (mailResponse.contains(WebGuiConstant.RC_SUCCESS)) {
                                LOGGER.info("Send Email : " + WebGuiConstant.RC_SUCCESS + jsonUtils.toJson(requ));
                            } else {
                                LOGGER.error("Send Email : " + WebGuiConstant.RC_GENERAL_ERROR + jsonUtils.toJson(requ));
                                MailEngineRetail swRetail = new MailEngineRetail();
                                swRetail.setStatus("not sent");
                                swRetail.setSwId(Long.valueOf(requ.getSwId()));
                                swRetail.setUsername(requ.getUserName());
                                swRetail.setRetrievalReferenceNumber(requ.getRetrievalReferenceNumber());
                                swRetail.setEmailAddres(email);
                                swRetail.setIsForwarded(requ.getIsForwarded());
                                swRetail.setCustumerName(requ.getCustomerName());
                                swRetail.setTransmissionDateAndTime(requ.getTransmissionDateAndTime());
                                swRetail.setApprovalUsername(requ.getApprovalUsername());
                                swRetail.setNote(requ.getNote());
                                swRetail.setKodeDeviasi(requ.getKodeDeviasi());
                                swRetail.setInReview(requ.getIsInReview());
                                swRetail.setMaxRetry(maxRetry);
                                swRetail.setCreatedt(new Date());
                                swRetail.setCreateby(findSWwithRrn.getCreatedBy());
                                swRetail.setUpdateby(null);
                                swRetail.setUpdatedt(null);
                                swService.save(swRetail);
                            }
                        }
                        findSWwithRrn.setIsNeedDevition(true);
                        swService.save(findSWwithRrn);
                    } else {
                        // send notif Request
                        LOGGER.info("SEND EMAIL NOTIF");
                        SentEmailNotifAfterNosPOJO requestAfterNos = new SentEmailNotifAfterNosPOJO();
                        for (User use : findUsername) {
                            String UsernameUse = use.getUsername();
                            requestAfterNos.setTransmissionDateAndTime(null);
                            requestAfterNos.setRetrievalReferenceNumber(findSWwithRrn.getRrn());
                            requestAfterNos.setUserName(UsernameUse);
                            requestAfterNos.setCustomerName(findSWwithRrn.getCustomerName());
                            requestAfterNos.setAppidNo(findIdCustomer.getCustomerCifNumber());
                            requestAfterNos.setSentra(findsentraId.getSentra().getSentraName());
                            String note = "";
                            for (ScoringRuleNosPojo ws : scoringResultFromNos) {
                                if (ws.getResult().equals("FAILED(Autoreject)") || ws.getResult().equals("FAILED")) {
                                    note = note + ", " + ws.getRejectDesc();
                                }
                            }
                            requestAfterNos.setAlasan(note);
                            requestAfterNos.setMms(findMms.getName());
                            //send email
                            String mailResponseNotif = dataNosService.generateSendNotifAfterNOS(requestAfterNos);
                            LOGGER.info("RESPONSE NOTIF : " + mailResponseNotif);
                            if (mailResponseNotif.contains(WebGuiConstant.RC_SUCCESS)) {
                                LOGGER.info("Send Email : " + WebGuiConstant.RC_SUCCESS + jsonUtils.toJson(requestAfterNos));
                            } else {
                                LOGGER.error("Send Email : " + WebGuiConstant.RC_GENERAL_ERROR + jsonUtils.toJson(requestAfterNos));
                                MailEngineNotif swRetailNotif = new MailEngineNotif();
                                swRetailNotif.setStatus("not sent");
                                swRetailNotif.setTransmissionDateAndTime(null);
                                swRetailNotif.setRetrievalReferenceNumber(requestAfterNos.getRetrievalReferenceNumber());
                                swRetailNotif.setUsername(UsernameUse);
                                swRetailNotif.setCustumerName(requestAfterNos.getCustomerName());
                                swRetailNotif.setAppidNo(requestAfterNos.getAppidNo());
                                swRetailNotif.setSentra(requestAfterNos.getSentra());
                                swRetailNotif.setAlasan(requestAfterNos.getAlasan());
                                swRetailNotif.setMms(requestAfterNos.getMms());
                                swRetailNotif.setCreatedt(new Date());
                                swRetailNotif.setUpdateby(null);
                                swRetailNotif.setUpdatedt(null);
                                swRetailNotif.setCreateby(findSWwithRrn.getCreatedBy());
                                swService.save(swRetailNotif);
                            }
                            LOGGER.debug("Send Email Notif Rejected Finish");
                        }
                        findSWwithRrn.setIsNeedDevition(false);
                        swService.save(findSWwithRrn);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return response;
    }

}