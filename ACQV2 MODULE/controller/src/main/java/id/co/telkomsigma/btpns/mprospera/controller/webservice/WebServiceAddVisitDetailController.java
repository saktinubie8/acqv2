package id.co.telkomsigma.btpns.mprospera.controller.webservice;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.dao.JumlahKunjunganDao;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sw.JumlahKunjungan;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.VisitDetailRequest;
import id.co.telkomsigma.btpns.mprospera.response.VisitDetailResponse;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webServiceAddVisitDetailController")
public class WebServiceAddVisitDetailController extends GenericController{
	JsonUtils jsonUtils = new JsonUtils();

	@Autowired
	private WSValidationService wsValidationService;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Autowired
	private TerminalService terminalSerivce;

	@Autowired
	private TerminalActivityService terminalActivityService;
	
	@Autowired
	private JumlahKunjunganDao visitNumberDao;

	@PostMapping(value = WebGuiConstant.ADD_VISIT_DETAIL_REQUEST)
	public @ResponseBody VisitDetailResponse doAddVisitDetail(
			@RequestBody final VisitDetailRequest request, @PathVariable("apkVersion") String apkVersion) {
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
		final VisitDetailResponse responseCode = new VisitDetailResponse();
		try {
			responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
			log.info("ADD Visit Detail INCOMING MESSAGE : " + jsonUtils.toJson(request));
			String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
			if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
				responseCode.setResponseCode(validation);
				String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
				log.error("Validation Failed for Username : " + request.getUsername() + " , imei : " + request.getImei() + " , sessionKey : "
						+ request.getSessionKey() + label);
			} else {
					JumlahKunjungan data = new JumlahKunjungan();
					data.setCifNumber(request.getCifNumber());
					data.setCreatedBy(request.getUsername());
					Date visitdate = formatDate.parse(request.getVisitDate());
					data.setVisitDate(visitdate);
					if("true".equals(request.getMeetCustDirect())){
						data.setMeetCustDirect(true);
					}else{
						data.setMeetCustDirect(false);
					}
					if("true".equals(request.getCustBusinessExists())){
						data.setCustBusinessExists(true);
					}else{
						data.setCustBusinessExists(false);
					}
					if("true".equals(request.getCustBusinessRun())){
						data.setCustBusinessRun(true);
					}else{
						data.setCustBusinessRun(false);
					}
					if("true".equals(request.getCustApplyLoan())){
						data.setCustApplyLoan(true);
					}else{
						data.setCustApplyLoan(false);
					}
					data.setNotApplyLoanReason(request.getNotApplyLoanReason());
					visitNumberDao.save(data);
			}
            log.info("ADD Visit Number FINISHED");
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
		} catch (Exception e) {
			log.error("ADD Visit Detail ERROR : " + e.getMessage());
			responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
			responseCode.setResponseMessage("ADD Visit Detail ERROR : " + e.getMessage());
		} finally {
			log.info("Trying to Create Terminal Activity");
			try {
			threadPoolTaskExecutor.execute(new Runnable() {

				@Override
				public void run() {
					Terminal terminal = terminalSerivce.loadTerminalByImei(request.getImei());
					TerminalActivity terminalActivity = new TerminalActivity();
					terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
					terminalActivity.setActivityType(TerminalActivity.ACTIVITY_ADD_VISIT_DETAIL);
					terminalActivity.setCreatedDate(new Date());
					terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
					terminalActivity.setSessionKey(request.getSessionKey());
					terminalActivity.setTerminal(terminal);
					terminalActivity.setUsername(request.getUsername().trim());
					List<MessageLogs> messageLogs = new ArrayList<>();
					terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
				}

			});
			log.info("Finishing create Terminal Activity");
			log.info("ADD Visit Detail RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
			}catch(Exception e) {
				log.error("ADD Visit Detail saveTerminalActivityAndMessageLogs ERROR : "+e.getMessage());
			}
		}
		return responseCode;
	}
}
