package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerDetail;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.pojo.PotentialCustomerDetailPojo;
import id.co.telkomsigma.btpns.mprospera.pojo.ProductEntity;
import id.co.telkomsigma.btpns.mprospera.pojo.ProductPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.RecPlafondEntity;
import id.co.telkomsigma.btpns.mprospera.request.GetDetailPotentialCustomerRequest;
import id.co.telkomsigma.btpns.mprospera.service.SyncPotentialCustomerService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webserviceGetDetailPotentialCustomerController")
public class WebServiceGetDetailPotentialCustomerController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalService terminalSerivce;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private SyncPotentialCustomerService syncPotentialCustomerService;
    
    @PostMapping(value = WebGuiConstant.POTENTIAL_CUSTOMER_GET_DETAIL_REQUEST)
    public @ResponseBody
    PotentialCustomerDetailPojo doGetDetailPotentialCus(
            @RequestBody final GetDetailPotentialCustomerRequest request, @PathVariable("apkVersion") String apkVersion) {

        PotentialCustomerDetailPojo responseCode = new PotentialCustomerDetailPojo();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        try {
            log.info("Get Detail Potential Customer INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                log.error("Validation Failed for Username : " + request.getUsername() + " , imei : " + request.getImei() + " , sessionKey : "
                        + request.getSessionKey());
            } else {
                List<PotentialCustomerDetail> syncDetail = syncPotentialCustomerService.getPotentialCustomerDetail(request.getCifNumber());
                for (PotentialCustomerDetail piece : syncDetail) {
                    responseCode.setCustomerId(piece.getCustomerId().toString());
                    responseCode.setCifNumber(piece.getCifNumber());
                    responseCode.setAddress(piece.getAddress());
                    responseCode.setPhoneNumber(piece.getPhoneNumber());
                    responseCode.setIirMax((piece.getPbu().multiply(piece.getIir().multiply(new BigDecimal("0.01")))).setScale(2,BigDecimal.ROUND_HALF_UP));
                    List<ProductEntity> syncProduct = syncPotentialCustomerService.getProduct(request.getCifNumber());
                    if (syncProduct == null) {
                        ProductPOJO product = new ProductPOJO();
                        product.setProductName("");
                        product.setAngsuran("");
                        product.setMaturityDate("");
                        product.setPlafond("");
                        product.setTenor("");
                        responseCode.getProduct().add(product);
                    } else {
                        for (ProductEntity pieceProd : syncProduct) {
                            ProductPOJO product = new ProductPOJO();
                            product.setProductName(pieceProd.getProductName() == null ? " " : pieceProd.getProductName());
                            product.setAngsuran((pieceProd.getAngsuran() == null ? " " : pieceProd.getAngsuran().toString()));
                            product.setMaturityDate((pieceProd.getMaturityDate() == null ? " " : (formatDate.format(pieceProd.getMaturityDate()))));
                            product.setPlafond((pieceProd.getPlafond() == null ? " " : pieceProd.getPlafond().toString()));
                            product.setTenor((pieceProd.getTenor() == null ? " " : pieceProd.getTenor().toString()));
                            responseCode.getProduct().add(product);
                        }
                    }
                    BigDecimal totalIns = BigDecimal.ZERO;
                    for (ProductEntity prod : syncProduct) {
                        if (prod.getAngsuran() != null)
                           totalIns = totalIns.add(BigDecimal.valueOf(prod.getAngsuran() * 2L));
                    }
                    responseCode.setTotalInstallment(totalIns);
                    responseCode.setPbu((piece.getPbu() == null ? BigDecimal.ZERO : piece.getPbu()));
                    if (piece.getPbu() != null && !"".equals(piece.getPbu().toString())) {
                        BigDecimal maxAngsuran = (responseCode.getIirMax().subtract(totalIns)).divide(new BigDecimal(2));
                        List<RecPlafondEntity> plafond = syncPotentialCustomerService.getPlafond(maxAngsuran.setScale(2, BigDecimal.ROUND_HALF_UP));
                        responseCode.setListPlafondRecMax(plafond);
                    } else {
                        responseCode.setListPlafondRecMax(null);
                    }
                }
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
            }
        } catch (Exception e) {
            log.error("Get Detail Potential Customer ERROR : " + e.getMessage());
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("syncCustomerPotential ERROR : " + e.getMessage());
        } finally {
            log.debug("Trying to Create Terminal Activity");
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalSerivce.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_DETAIL_POTENTIAL_CUSTOMER);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }

                });
                log.debug("Finishing create Terminal Activity");
                log.info("Get Detail Potential Customer RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("Get Detail Potential Customer saveTerminalActivityAndMessageLogs ERROR : " + e.getMessage());
            }
        }
        return responseCode;
    }
}
