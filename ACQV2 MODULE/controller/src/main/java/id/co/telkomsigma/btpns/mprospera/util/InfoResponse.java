package id.co.telkomsigma.btpns.mprospera.util;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("deprecation")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class InfoResponse {

    private String lastBuild;
    private String buildBranch;
    private String buildVersion;
    private String buildChanges;

    public String getLastBuild() {
        return lastBuild;
    }

    public void setLastBuild(String lastBuild) {
        this.lastBuild = lastBuild;
    }

    public String getBuildBranch() {
        return buildBranch;
    }

    public void setBuildBranch(String buildBranch) {
        this.buildBranch = buildBranch;
    }

    public String getBuildChanges() {
        return buildChanges;
    }

    public void setBuildChanges(String buildChanges) {
        this.buildChanges = buildChanges;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        this.buildVersion = buildVersion;
    }

    public String toString(String e) {
        return "{\"lastBuild\": \"" + lastBuild + "\", \"error\": \"" + e + "\"}";
    }

}
