package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.CustomerConstant;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import id.co.telkomsigma.btpns.mprospera.model.ppi.FamilyDataPPI;
import id.co.telkomsigma.btpns.mprospera.model.ppi.PPI;
import id.co.telkomsigma.btpns.mprospera.model.product.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.product.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.*;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.AddSWResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProductListResponse;
import id.co.telkomsigma.btpns.mprospera.response.SWForEmailResponse;
import id.co.telkomsigma.btpns.mprospera.response.SWResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.FieldValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.SendMessageUtilMQ;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceAddSWController")
public class WebServiceAddSWController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private UserService userService;

    @Autowired
    private SendMessageUtilMQ sendMessageUtilMQ;

    @Autowired
    private SWService swService;

    @Autowired
    private PMService pmService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private PPIService ppiService;

    @Autowired
    private SentEmailService sentEmailService;

    private static final String CREATE_SW_FAILED = "CREATE SW FAILED";
    
    /*
     * @author : Ilham
     * @Since  : 20190220
     * @category patch 
     *   - Add Sw with send Email 
     *   
     * @author : Ilham
     * @Since  : 20191126
     * @category patch 
     *   - Penambahan Timestamp untuk email 
     */ 
    @RequestMapping(value = WebGuiConstant.SW_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AddSWResponse doAdd(@RequestBody String requestString,
                        @PathVariable("apkVersion") String apkVersion) throws Exception {
        SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        String swId = "";
        String swProductId = "";
        List<ProductListResponse> productIdListResponses = new ArrayList<>();
        Object obj = new JsonUtils().fromJson(requestString, AddSWRequest.class);
        final AddSWRequest request = (AddSWRequest) obj;
        final AddSWResponse responseCode = new AddSWResponse();
        // Set Response Sukses terlebih dulu
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        // Validasi messaging request dari Android
        try {
            log.info("addSW INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                // insert ke tabel SW
                User userPs = userService.findUserByUsername(request.getUsername());
                SWPOJO swPojo = request.getSw();
                // Validasi SW Lokal ID android sudah ada di server atau belum
                if (swPojo.getSwId() == null || "".equals(swPojo.getSwId())) {
                    log.error("UNKNOWN SW ID");
                    responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                SurveyWawancara sw = swService.findSwByLocalId(swPojo.getSwId());
                if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_INSERT) && sw != null) {
                    log.error("SW ALREADY EXIST");
                    responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
                    String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_SW_ALREADY_EXIST);
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                if (!request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_INSERT)) {
                    log.info("SW Existing to update/delete");
                    if (sw == null) {
                        log.error("UNKNOWN SW ID");
                        responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    } else if (WebGuiConstant.STATUS_APPROVED.equals(sw.getStatus())) {
                        log.error("SW ID SUDAH DI APPROVED ");
                        responseCode.setResponseCode(WebGuiConstant.RC_SW_ALREADY_APPROVED);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    } else if (WebGuiConstant.STATUS_REJECTED.equals(sw.getStatus())) {
                        log.error("SW ID SUDAH DI REJECTED ");
                        responseCode.setResponseCode(WebGuiConstant.RC_ALREADY_REJECTED);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    sw.setUpdatedDate(new Date());
                    sw.setUpdatedBy(request.getUsername());
                }
                if (sw == null) {
                    sw = new SurveyWawancara();
                }
                sw.setSwPPI(true);
                sw.setCreatedDate(new Date()); // timestamp current date
                sw.setCreatedBy(request.getUsername()); // user yang input data
                if (swPojo.getDisbursementDate() != null) {
                    if (!"".equals(swPojo.getDisbursementDate())) {
                        sw.setDisbursementDate(formatter.parse(swPojo.getDisbursementDate()));
                    }
                }
                sw.setLocalId(swPojo.getSwId());// Local ID SW from device
                if (swPojo.getPmId() != null) {
                    if (!swPojo.getPmId().equals("")) {
                        ProjectionMeeting pm = pmService.findById(swPojo.getPmId());
                        sw.setPmId(pm);// set PM ID Relation
                    }
                }
                sw.setWismaId(userPs.getOfficeCode());// set Wisma
                if (swPojo.getCustomerType() != null)
                    sw.setCustomerRegistrationType(swPojo.getCustomerType().charAt(0));// Set Tipe Nasabah (baru/lama)
                sw.setCustomerIdName(swPojo.getIdCardName());// Set Nama di KTP
                sw.setCustomerIdNumber(swPojo.getIdCardNumber());// Set NIK
                if (swPojo.getSurveyDate() != null) {
                    if (!swPojo.getSurveyDate().equals(""))
                        sw.setSurveyDate(formatDateTime.parse(swPojo.getSurveyDate()));// Set Tanggal SW
                }
                if ("true".equals(swPojo.getIsNasabahPotensial())) {
                    sw.setNabasahPotensial(true);
                }
                // set data profile SW
                SWProfilePOJO swProfile = swPojo.getProfile();
                if (swPojo.getCustomerId() != null && !swPojo.getCustomerId().equals("")) {
                    if (!swPojo.getCustomerId().equals("")) {
                        sw.setCustomerId(Long.parseLong(swPojo.getCustomerId()));// Set Customer ID jika nasabah lama
                        Customer customer = customerService.findById(swPojo.getCustomerId());
                        if (customer == null) {
                            log.error("UNKNOWN CUSTOMER ID");
                            responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_CUSTOMER_ID);
                            String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_UNKNOWN_CUSTOMER_ID);
                            responseCode.setResponseMessage(label);
                            return responseCode;
                        }
                        if (swProfile.getLongName() != null) {
                            if (!swProfile.getLongName().equals("")) {
                                sw.setCustomerName(swProfile.getLongName());// set nama lengkap nasabah
                            } else {
                                if (customer.getCustomerName() != null) {
                                    sw.setCustomerName(customer.getCustomerName());
                                } else {
                                    sw.setCustomerName(swPojo.getCustomerName());
                                }
                            }
                        } else {
                            if (customer.getCustomerName() != null) {
                                sw.setCustomerName(customer.getCustomerName());
                            } else {
                                sw.setCustomerName(swPojo.getCustomerName());
                            }
                        }
                    }
                } else {
                    sw.setCustomerName(swProfile.getLongName());
                }
                sw.setCustomerAliasName(swProfile.getAlias());// set nama singkat nasabah
                sw.setGender(swProfile.getGender());// set jenis kelamin
                if (swProfile.getBirthDay() != null) {
                    if (!swProfile.getBirthDay().equals(""))
                        sw.setBirthDate(formatter.parse(swProfile.getBirthDay()));// set tanggal lahir
                }
                sw.setBirthPlace(swProfile.getBirthPlace());// set id tempat lahir
                sw.setBirthPlaceRegencyName(swProfile.getBirthPlaceRegencyName());// set nama tempat lahir
                if (swProfile.getIdCardExpired() != null) {
                    if (!swProfile.getIdCardExpired().equals(""))
                        sw.setIdExpiryDate(formatter.parse(swProfile.getIdCardExpired()));// set masa berlaku KTP
                }
                sw.setMaritalStatus(swProfile.getMarriedStatus());// set status perkawinan
                sw.setWorkType(swProfile.getWorkStatus());// set status pekerjaan
                sw.setReligion(swProfile.getReligion());// set agama nasabah
                sw.setPhoneNumber(swProfile.getPhoneNumber());// set nomor telepon nasabah
                sw.setNpwp(swProfile.getNpwp());// set NPWP
                sw.setMotherName(swProfile.getMotherName());// set nama ibu kandung
                sw.setDependentCount(Integer
                        .parseInt(FieldValidationUtil.integerRequestValueValidation(swProfile.getDependants())));// set
                sw.setEducation(swProfile.getEducation());// set pendidikan terakhir nasabah
                sw.setLifeTime(FieldValidationUtil.booleanRequestValueValidation(swProfile.getIsLifeTime()));// set
                SWAddressPOJO swAddress = swPojo.getAddress();
                sw.setAddress(swAddress.getName());// set alamat
                sw.setRtrw(swAddress.getStreet());// set rtrw
                sw.setPostalCode(swAddress.getPostCode());// set kode pos
                sw.setProvince(swAddress.getProvinceId());// set provinsi
                sw.setCity(swAddress.getRegencyId());// set kota
                sw.setKecamatan(swAddress.getDistrictId());// set kecamatan
                sw.setKelurahan(swAddress.getVillageId());// set kelurahan
                sw.setDomisiliAddress(swAddress.getAdditionalAddress());// set alamat domisili
                if (swAddress.getPlaceOwnerShip() != null)
                    sw.setHouseType(swAddress.getPlaceOwnerShip());// set status kepemilikan rumah
                sw.setHouseCertificate(swAddress.getPlaceCertificate().charAt(0));// set bukti kepemilikan rumah
                // set data pasangan SW
                SWSpousePOJO swPartner = swPojo.getPartner();
                sw.setCoupleName(swPartner.getName());// set nama pasangan
                sw.setCoupleBirthPlace(swPartner.getBirthPlaceId());// set id tempat lahir pasangan
                sw.setCoupleBirthPlaceRegencyName(swPartner.getBirthPlace());// set tempat lahir pasangan
                if (swPartner.getBirthDay() != null) {
                    if (!swPartner.getBirthDay().equals("")) {
                        sw.setCoupleBirthDate(formatter.parse(swPartner.getBirthDay()));// set tanggal lahir
                        // pasangan
                    }
                }
                if (swPartner.getJob() != null) {
                    if (!swPartner.getJob().equals("")) {
                        sw.setCoupleJob(swPartner.getJob().charAt(0));// set pekerjaan pasangan
                    }
                }
                // set data usaha SW
                SWBusinessPOJO swBusiness = swPojo.getBusiness();
                sw.setBusinessField(swBusiness.getType());
                sw.setBusinessDescription(swBusiness.getDesc());
                sw.setBusinessOwnerStatus(swBusiness.getBusinessOwnership().charAt(0));
                sw.setBusinessShariaType(swBusiness.getBusinessShariaCompliance().charAt(0));
                sw.setBusinessName(swBusiness.getName());
                sw.setBusinessLocation(swBusiness.getBusinessLocation().charAt(0));
                sw.setBusinessAddress(swBusiness.getAddress());
                sw.setBusinessAgeYear(Integer
                        .parseInt(FieldValidationUtil.integerRequestValueValidation(swBusiness.getAgeYear())));
                sw.setBusinessAgeMonth(Integer
                        .parseInt(FieldValidationUtil.integerRequestValueValidation(swBusiness.getAgeMonth())));
                sw.setBusinessWorkStatus(swBusiness.getBusinessRunner().charAt(0));
                if (swBusiness.getHasOtherBankLoan() != null) {
                    sw.setHasOtherBankLoan(swBusiness.getHasOtherBankLoan().charAt(0));
                }
                sw.setOtherBankLoanName(swBusiness.getOtherBankName());
                // set paramater kalkulasi
                SWCalculationVariablePOJO swCalcVariable = swPojo.getCalcVariable();
                sw.setBusinessCycle(swCalcVariable.getJenisSiklus().charAt(0));
                sw.setBusinessDaysOperation(swCalcVariable.getHariOperasional());
                if (swCalcVariable.getPendapatanLainnya() != null
                        || !swCalcVariable.getPendapatanLainnya().equals(""))
                    sw.setOtherIncome(new BigDecimal(swCalcVariable.getPendapatanLainnya()));
                if (swCalcVariable.getPendapatanRamai() != null || !swCalcVariable.getPendapatanRamai().equals(""))
                    sw.setOneDayBusyIncome(new BigDecimal(swCalcVariable.getPendapatanRamai()));
                if (swCalcVariable.getJumlahhariramai() != null || !swCalcVariable.getJumlahhariramai().equals(""))
                    sw.setTotalDaysInMonthBusy(Integer.parseInt(swCalcVariable.getJumlahhariramai()));
                if (swCalcVariable.getPendapatanSepi() != null || !swCalcVariable.getPendapatanSepi().equals(""))
                    sw.setOneDayLessIncome(new BigDecimal(swCalcVariable.getPendapatanSepi()));
                if (swCalcVariable.getJumlahharisepi() != null || !swCalcVariable.getJumlahharisepi().equals(""))
                    sw.setTotalDaysInMonthLess(Integer.parseInt(swCalcVariable.getJumlahharisepi()));
                if (swCalcVariable.getPendapatanPerbulanRamaiSepi() != null
                        || !swCalcVariable.getPendapatanPerbulanRamaiSepi().equals(""))
                    sw.setTotalLessBusyIncome(new BigDecimal(swCalcVariable.getPendapatanPerbulanRamaiSepi()));
                if (swCalcVariable.getPeriode1() != null || !swCalcVariable.getPeriode1().equals(""))
                    sw.setFirstDayIncome(new BigDecimal(swCalcVariable.getPeriode1()));
                if (swCalcVariable.getPeriode2() != null || !swCalcVariable.getPeriode2().equals(""))
                    sw.setSecondDayIncome(new BigDecimal(swCalcVariable.getPeriode2()));
                if (swCalcVariable.getPeriode3() != null || !swCalcVariable.getPeriode3().equals(""))
                    sw.setThirdDayIncome(new BigDecimal(swCalcVariable.getPeriode3()));
                if (swCalcVariable.getPendapatanPerbulan3Periode() != null
                        || !swCalcVariable.getPendapatanPerbulan3Periode().equals(""))
                    sw.setThreePeriodIncome(new BigDecimal(swCalcVariable.getPendapatanPerbulan3Periode()));
                if (swCalcVariable.getJamBuka() != null) {
                    if (!swCalcVariable.getJamBuka().equals("")) {
                        if (!swCalcVariable.getJamBuka().equals("0"))
                            sw.setOpenTime(
                                    new java.sql.Time(formatTime.parse(swCalcVariable.getJamBuka()).getTime()));
                    }
                }
                if (swCalcVariable.getJamTutup() != null) {
                    if (!swCalcVariable.getJamTutup().equals("")) {
                        if (!swCalcVariable.getJamTutup().equals("0"))
                            sw.setCloseTime(
                                    new java.sql.Time(formatTime.parse(swCalcVariable.getJamTutup()).getTime()));
                    }
                }
                if (swCalcVariable.getWaktuKerja() != null || !swCalcVariable.getWaktuKerja().equals(""))
                    sw.setWorkTime(Integer.parseInt(swCalcVariable.getWaktuKerja()));
                if (swCalcVariable.getWaktukerjaSetelahbuka() != null
                        || !swCalcVariable.getWaktukerjaSetelahbuka().equals(""))
                    sw.setWorkTimeAfterOpen(Integer.parseInt(swCalcVariable.getWaktukerjaSetelahbuka()));
                if (swCalcVariable.getUangkasBoxdiBuka() != null
                        || !swCalcVariable.getUangkasBoxdiBuka().equals(""))
                    sw.setCashOpenTime(new BigDecimal(swCalcVariable.getUangkasBoxdiBuka()));
                if (swCalcVariable.getUangKasBoxSekarang() != null
                        || !swCalcVariable.getUangKasBoxSekarang().equals(""))
                    sw.setCashCurrentTime(new BigDecimal(swCalcVariable.getUangKasBoxSekarang()));
                if (swCalcVariable.getUangBelanja() != null || !swCalcVariable.getUangBelanja().equals(""))
                    sw.setCashExpense(new BigDecimal(swCalcVariable.getUangBelanja()));
                if (swCalcVariable.getPendapatanPerbulanPerkiraanSehari() != null
                        || !swCalcVariable.getPendapatanPerbulanPerkiraanSehari().equals(""))
                    sw.setOneDayPredictIncome(
                            new BigDecimal(swCalcVariable.getPendapatanPerbulanPerkiraanSehari()));
                if (swCalcVariable.getPendapatanPerbulanKasSehari() != null
                        || !swCalcVariable.getPendapatanPerbulanKasSehari().equals(""))
                    sw.setOneDayCashIncome(new BigDecimal(swCalcVariable.getPendapatanPerbulanKasSehari()));
                if (swCalcVariable.getPendapatanRata2TigaPeriode() != null
                        || !swCalcVariable.getPendapatanRata2TigaPeriode().equals(""))
                    sw.setAvgThreePeriodIncome(new BigDecimal(swCalcVariable.getPendapatanRata2TigaPeriode()));
                if (request.getTotalPendapatan() != null || swPojo.getTotalPendapatan() != null
                        || swCalcVariable.getTotalPendapatan() != null
                        || !swCalcVariable.getTotalPendapatan().equals(""))
                    sw.setTotalIncome((new BigDecimal(swCalcVariable.getTotalPendapatan()).setScale(4,
                            BigDecimal.ROUND_HALF_UP)));
                if (swCalcVariable.getPendapatanLainnya() != null
                        || !swCalcVariable.getPendapatanLainnya().equals(""))
                    sw.setOtherIncome(new BigDecimal(swCalcVariable.getPendapatanLainnya()));
                if (swPojo.getIncome() != null || !swPojo.getIncome().equals(""))
                    sw.setTotalIncome(new BigDecimal(swPojo.getIncome()).setScale(4, BigDecimal.ROUND_HALF_UP));
                if (swPojo.getNetIncome() != null || !swPojo.getNetIncome().equals(""))
                    sw.setNettIncome(new BigDecimal(swPojo.getNetIncome()).setScale(4, BigDecimal.ROUND_HALF_UP));
                if (swPojo.getSpending() != null || !swPojo.getSpending().equals(""))
                    sw.setExpenditure(new BigDecimal(swPojo.getSpending()).setScale(4, BigDecimal.ROUND_HALF_UP));
                // set data pengeluaran SW
                SWExpensePOJO swExpense = swPojo.getExpense();
                if (swExpense.getTransportUsaha() != null || !swExpense.getTransportUsaha().equals(""))
                    sw.setTransportCost(new BigDecimal(swExpense.getTransportUsaha()));
                if (swExpense.getUtilitasUsaha() != null || !swExpense.getUtilitasUsaha().equals(""))
                    sw.setUtilityCost(new BigDecimal(swExpense.getUtilitasUsaha()));
                if (swExpense.getGajiUsaha() != null || !swExpense.getGajiUsaha().equals(""))
                    sw.setStaffSalary(new BigDecimal(swExpense.getGajiUsaha()));
                if (swExpense.getSewaUsaha() != null || !swExpense.getSewaUsaha().equals(""))
                    sw.setRentCost(new BigDecimal(swExpense.getSewaUsaha()));
                if (swExpense.getMakanNonUsaha() != null || !swExpense.getMakanNonUsaha().equals(""))
                    sw.setFoodCost(new BigDecimal(swExpense.getMakanNonUsaha()));
                if (swExpense.getUtitlitasNonUsaha() != null || !swExpense.getUtitlitasNonUsaha().equals(""))
                    sw.setPrivateUtilityCost(new BigDecimal(swExpense.getUtitlitasNonUsaha()));
                if (swExpense.getPendidikanNonUsaha() != null || !swExpense.getPendidikanNonUsaha().equals(""))
                    sw.setEducationCost(new BigDecimal(swExpense.getPendidikanNonUsaha()));
                if (swExpense.getKesehatanNonUsaha() != null || !swExpense.getKesehatanNonUsaha().equals(""))
                    sw.setHealthCost(new BigDecimal(swExpense.getKesehatanNonUsaha()));
                if (swExpense.getTransportNonUsaha() != null || !swExpense.getTransportNonUsaha().equals(""))
                    sw.setPrivateTransportCost(new BigDecimal(swExpense.getTransportNonUsaha()));
                if (swExpense.getAngsuranNonUsaha() != null || !swExpense.getAngsuranNonUsaha().equals(""))
                    sw.setInstallmentCost(new BigDecimal(swExpense.getAngsuranNonUsaha()));
                if (swExpense.getLainlainNonUsaha() != null || !swExpense.getLainlainNonUsaha().equals(""))
                    sw.setOtherCost(new BigDecimal(swExpense.getLainlainNonUsaha()));
                BigDecimal totalExpense = sw.getTransportCost().add(sw.getUtilityCost()).add(sw.getStaffSalary())
                        .add(sw.getRentCost());
                sw.setBusinessCost(totalExpense);
                sw.setLongitude(request.getLongitude());
                sw.setLatitude(request.getLatitude());
                sw.setRrn(request.getRetrievalReferenceNumber());
                sw.setTotalBuy(sw.getExpenditure().subtract(sw.getBusinessCost()));
                sw.setNonBusinessCost(sw.getFoodCost().add(sw.getPrivateUtilityCost()).add(sw.getEducationCost())
                        .add(sw.getHealthCost()).add(sw.getPrivateTransportCost()).add(sw.getInstallmentCost())
                        .add(sw.getOtherCost()));
                sw.setResidualIncome(sw.getNettIncome().add(sw.getOtherIncome()).subtract(sw.getNonBusinessCost()));
                /*sw.setIir(sw.getNettIncome().multiply(new BigDecimal(40).divide(new BigDecimal(100)))
                        .subtract(sw.getInstallmentCost()));*/
                if (swPojo.getIir() != null) {
                    if ("".equals(swPojo.getIir())) {
                        sw.setIir(new BigDecimal(swPojo.getIir()));
                    }
                }
                sw.setIdir(sw.getResidualIncome());
                PPI ppi = ppiService.findPPIbySwId(sw.getSwId());
                // jika melakukan delete SW set flag is deleted
                if (WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(request.getAction())) {
                    sw.setIsDeleted(true);
                    if (ppi != null) {
                        ppi.setDeleted(true);
                        ppi.setDeletedBy(request.getUsername());
                        ppi.setDeletedDate(new Date());
                    }
                }
                // jika melakukan delete atau update SW set terlebih dahulu SW
                // ID-nya
                if (WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(request.getAction())
                        || WebGuiConstant.ACTION_UPDATE.equalsIgnoreCase(request.getAction())) {
                    if (swPojo.getSwId() != null) {
                        if (!swPojo.getSwId().equals("")) {
                            String localId = swPojo.getSwId();
                            SurveyWawancara temp = swService.findSwByLocalId(localId);
                            if (temp != null && temp.getSwId() != null) {
                                sw.setSwId(temp.getSwId());
                                swId = temp.getSwId().toString();
                            } else {
                                log.error("UNKNOWN SW ID");
                                responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                        }
                    } else {
                        log.error("UNKNOWN SW ID");
                        responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    }
                }
                // jika sw tersebut sudah di approved
                if (WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(request.getAction())
                        && swService.getSWById(swId).getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                    log.error("SW ALREADY APPROVED");
                    responseCode.setResponseCode(WebGuiConstant.RC_SW_ALREADY_APPROVED);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                log.info("FINISH MAPPING SW DATA");
                // jika sw tersebut tidak ada di database Mprospera
                if ((WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(request.getAction())
                        || WebGuiConstant.ACTION_UPDATE.equalsIgnoreCase(request.getAction()))
                        && !swService.isValidSw(swPojo.getSwId())) {
                    log.error("UNKNOWN SW ID");
                    responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                } else {
                    // Saving data SW ke DB dengan kondisi RRN tidak sama
                    if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addSW")) {
                        // set status SW baru dengan WAITING FOR APPROVAL
                        sw.setStatus(swPojo.getStatus());
                        swService.save(sw);
                        if (ppi != null && ppi.getDeleted() == true) {
                            ppiService.savePPI(ppi);
                            List<FamilyDataPPI> familyDataPPIList = ppiService.findFamilyDataByPPIId(ppi.getPpiId());
                            for (FamilyDataPPI familyDataPPI : familyDataPPIList) {
                                familyDataPPI.setDeleted(true);
                                familyDataPPI.setDeletedDate(new Date());
                                familyDataPPI.setDeletedBy(request.getUsername());
                                ppiService.saveFamilyData(familyDataPPI);
                            }
                        }
                        responseCode.setSwId(swPojo.getSwId());
                        log.debug("sw id yang terbentuk : " + sw.getSwId().toString());
                        swId = sw.getSwId().toString();
                        // mapping sw dengan produk
                        for (SWProductMapPOJO productList : swPojo.getSwProducts()) {
                            SWProductMapping swProductMap = swService
                                    .findProductMapByLocalId(productList.getSwProductId());
                            if (swProductMap == null)
                                swProductMap = new SWProductMapping();
                            ProductListResponse productIdListResponse = new ProductListResponse();
                            LoanProduct loanProduct = swService.findByProductId(productList.getProductId());
                            log.debug("Produk Pembiayaan yang akan disimpan : " + loanProduct.getProductName());
                            swProductMap.setProductId(loanProduct);
                            if (productList.getSelectedPlafon() != null || !productList.getSelectedPlafon().equals(""))
                                swProductMap.setPlafon(new BigDecimal(productList.getSelectedPlafon()));
                            log.debug("swId yang akan disimpan : " + swId);
                            swProductMap.setSwId(Long.parseLong(swId));
                            swProductMap
                                    .setRecommendedProductId(Long.parseLong(productList.getRecommendationProductId()));
                            if (productList.getRecommendationSelectedPlafon() != null
                                    || !productList.getRecommendationSelectedPlafon().equals(""))
                                swProductMap.setRecommendedPlafon(
                                        new BigDecimal(productList.getRecommendationSelectedPlafon()));
                            if (productList.getInstallment() != null) {
                                if (!"".equals(productList.getInstallment())) {
                                    swProductMap.setInstallment(new BigDecimal(productList.getInstallment()));
                                }
                            }
                            if (productList.getReInstallment() != null) {
                                if (!"".equals(productList.getReInstallment())) {
                                    swProductMap.setReInstallment(new BigDecimal(productList.getReInstallment()));
                                }
                            }
                            swProductMap.setLocalId(productList.getSwProductId());
                            // jika melakukan delete SW set flag is deleted
                            if ("delete".equals(productList.getAction()))
                                swProductMap.setDeleted(true);
                            // jika melakukan delete atau update SW set terlebih
                            // dahulu SW ID-nya
                            if (WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(productList.getAction())
                                    || WebGuiConstant.ACTION_UPDATE.equalsIgnoreCase(productList.getAction())) {
                                if (null != productList.getSwProductId()) {
                                    if (!productList.getSwProductId().equals("")) {
                                        swProductMap.setMappingId(swProductMap.getMappingId());
                                    }
                                } else {
                                    log.error("SW PRODUCT NULL");
                                    responseCode.setResponseCode(WebGuiConstant.RC_SW_PRODUCT_NULL);
                                }
                            }
                            try {
                                swService.save(swProductMap);
                                productIdListResponse.setProductId(loanProduct.getProductId().toString());
                                swProductId = swProductMap.getLocalId();
                                productIdListResponse.setSwProductId(swProductId);
                                productIdListResponse
                                        .setRecommendationProductId(swProductMap.getRecommendedProductId().toString());
                                productIdListResponses.add(productIdListResponse);
                            } catch (Exception e) {
                                e.printStackTrace();
                                sw.setIsDeleted(true);
                                swService.save(sw);
                                log.error(CREATE_SW_FAILED);
                                responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                        }
                        responseCode.setSwProducts(productIdListResponses);
                        // mapping sw dengan pembelian langsung
                        if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_UPDATE)
                                || request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_DELETE)) {
                            List<DirectBuyThings> directBuyThingsList = swService.findProductBySwId(sw);
                            for (DirectBuyThings directBuyThings : directBuyThingsList) {
                                swService.deleteDirectBuy(directBuyThings);
                            }
                            for (SWDirectBuyPOJO directBuy : swPojo.getDirectPurchasing()) {
                                DirectBuyThings newDirectBuy = new DirectBuyThings();
                                newDirectBuy.setNamaBarang(directBuy.getItemName());
                                if (directBuy.getPrice() != null || !directBuy.getPrice().equals(""))
                                    newDirectBuy.setPrice(directBuy.getPrice());
                                if (directBuy.getFrequency() != null || !directBuy.getFrequency().equals(""))
                                    newDirectBuy.setFrequency(Integer.parseInt(directBuy.getFrequency()));
                                if (directBuy.getSellingPrice() != null || !directBuy.getSellingPrice().equals(""))
                                    newDirectBuy.setSellingPrice(directBuy.getSellingPrice());
                                newDirectBuy.setIndex(Integer.parseInt(directBuy.getIndex()));
                                newDirectBuy.setTotalItem(Integer.parseInt(directBuy.getTotalItem()));
                                newDirectBuy.setType(Integer.parseInt(directBuy.getType()));
                                newDirectBuy.setSwId(sw);
                                try {
                                    swService.save(newDirectBuy);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error(CREATE_SW_FAILED);
                                    responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        } else {
                            for (SWDirectBuyPOJO directBuy : swPojo.getDirectPurchasing()) {
                                DirectBuyThings direct = new DirectBuyThings();
                                direct.setNamaBarang(directBuy.getItemName());
                                if (directBuy.getSellingPrice() != null || !directBuy.getSellingPrice().equals(""))
                                    direct.setPrice(directBuy.getPrice());
                                if (directBuy.getFrequency() != null || !directBuy.getFrequency().equals(""))
                                    direct.setFrequency(Integer.parseInt(directBuy.getFrequency()));
                                if (directBuy.getSellingPrice() != null || !directBuy.getSellingPrice().equals(""))
                                    direct.setSellingPrice(directBuy.getSellingPrice());
                                direct.setIndex(Integer.parseInt(directBuy.getIndex()));
                                direct.setTotalItem(Integer.parseInt(directBuy.getTotalItem()));
                                direct.setType(Integer.parseInt(directBuy.getType()));
                                direct.setSwId(sw);
                                try {
                                    swService.save(direct);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error(CREATE_SW_FAILED);
                                    responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        }
                        // mapping sw dengan pembelian AWGM
                        if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_UPDATE)
                                || request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_DELETE)) {
                            List<AWGMBuyThings> awgmList = swService.findAwgmProductBySwId(sw);
                            for (AWGMBuyThings awgm : awgmList) {
                                swService.deleteAwgmBuy(awgm);
                            }
                            for (SWAWGMBuyPOJO awgmBuy : swPojo.getAwgmPurchasing()) {
                                AWGMBuyThings newAwgmBuy = new AWGMBuyThings();
                                newAwgmBuy.setNamaBarang(awgmBuy.getItemName());
                                if (awgmBuy.getFrequency() != null || !awgmBuy.getFrequency().equals(""))
                                    newAwgmBuy.setFrequency(Integer.parseInt(awgmBuy.getFrequency()));
                                if (awgmBuy.getSellingPrice() != null || !awgmBuy.getSellingPrice().equals(""))
                                    newAwgmBuy.setSellPrice(awgmBuy.getSellingPrice());
                                newAwgmBuy.setIndex(Integer.parseInt(awgmBuy.getIndex()));
                                if (awgmBuy.getPrice() != null || !awgmBuy.getPrice().equals(""))
                                    newAwgmBuy.setBuyPrice(awgmBuy.getPrice());
                                newAwgmBuy.setTotal(Integer.parseInt(awgmBuy.getTotalItem()));
                                newAwgmBuy.setType(Integer.parseInt(awgmBuy.getType()));
                                newAwgmBuy.setSwId(sw);
                                try {
                                    swService.save(newAwgmBuy);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error(CREATE_SW_FAILED);
                                    responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        } else {
                            for (SWAWGMBuyPOJO awgmBuy : swPojo.getAwgmPurchasing()) {
                                AWGMBuyThings awgm = new AWGMBuyThings();
                                awgm.setNamaBarang(awgmBuy.getItemName());
                                if (awgmBuy.getFrequency() != null || !awgmBuy.getFrequency().equals(""))
                                    awgm.setFrequency(Integer.parseInt(awgmBuy.getFrequency()));
                                if (awgmBuy.getSellingPrice() != null || !awgmBuy.getSellingPrice().equals(""))
                                    awgm.setSellPrice(awgmBuy.getSellingPrice());
                                awgm.setIndex(Integer.parseInt(awgmBuy.getIndex()));
                                if (awgmBuy.getPrice() != null || !awgmBuy.getPrice().equals(""))
                                    awgm.setBuyPrice(awgmBuy.getPrice());
                                awgm.setTotal(Integer.parseInt(awgmBuy.getTotalItem()));
                                awgm.setType(Integer.parseInt(awgmBuy.getType()));
                                awgm.setSwId(sw);
                                try {
                                    swService.save(awgm);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error(CREATE_SW_FAILED);
                                    responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        }
                        // mapping sw dengan tetangga nasabah SW
                        if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_UPDATE)
                                || request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_DELETE)) {
                            List<NeighborRecommendation> refList = swService.findNeighborBySwId(sw);
                            for (NeighborRecommendation reff : refList) {
                                swService.deleteNeigbourReff(reff);
                            }
                            for (SWNeighborRecommendationPOJO neighborReff : swPojo.getReferenceList()) {
                                NeighborRecommendation newNeighbourReff = new NeighborRecommendation();
                                newNeighbourReff.setNeighborName(neighborReff.getName());
                                newNeighbourReff.setNeighborAddress(neighborReff.getAddress());
                                newNeighbourReff.setNeighborRelation(neighborReff.getGoodNeighbour().charAt(0));
                                newNeighbourReff.setNeighborOutstanding(neighborReff.getVisitedByLandshark().charAt(0));
                                newNeighbourReff.setNeighborBusiness(neighborReff.getHaveBusiness().charAt(0));
                                newNeighbourReff.setNeighborRecomend(neighborReff.getRecommended().charAt(0));
                                if (neighborReff.getLengthOfStay() != null) {
                                    newNeighbourReff.setLengthOfStay(neighborReff.getLengthOfStay().charAt(0));
                                }
                                newNeighbourReff.setSwId(sw);
                                try {
                                    swService.save(newNeighbourReff);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error(CREATE_SW_FAILED);
                                    responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        } else {
                            for (SWNeighborRecommendationPOJO neighborReff : swPojo.getReferenceList()) {
                                NeighborRecommendation neighbor = new NeighborRecommendation();
                                neighbor.setNeighborName(neighborReff.getName());
                                neighbor.setNeighborAddress(neighborReff.getAddress());
                                neighbor.setNeighborRelation(neighborReff.getGoodNeighbour().charAt(0));
                                neighbor.setNeighborOutstanding(neighborReff.getVisitedByLandshark().charAt(0));
                                neighbor.setNeighborBusiness(neighborReff.getHaveBusiness().charAt(0));
                                neighbor.setNeighborRecomend(neighborReff.getRecommended().charAt(0));
                                if (neighborReff.getLengthOfStay() != null) {
                                    neighbor.setLengthOfStay(neighborReff.getLengthOfStay().charAt(0));
                                }
                                neighbor.setSwId(sw);
                                try {
                                    swService.save(neighbor);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error(CREATE_SW_FAILED);
                                    responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        }
                        // mapping calculator pendapatan lainnya
                        List<SWOtherItemCalculationPOJO> pendapatanLainList = swCalcVariable.getItemCalcLains();
                        if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_UPDATE)
                                || request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_DELETE)) {
                            List<OtherItemCalculation> otherCalcList = swService.findOtherItemCalcBySwId(sw.getSwId());
                            for (OtherItemCalculation otherCalc : otherCalcList) {
                                swService.deleteOtherItemCalc(otherCalc);
                            }
                            for (SWOtherItemCalculationPOJO pendapatanLain : pendapatanLainList) {
                                OtherItemCalculation newOtherCalc = new OtherItemCalculation();
                                newOtherCalc.setName(pendapatanLain.getName());
                                if (pendapatanLain.getAmount() != null || !pendapatanLain.getAmount().equals(""))
                                    newOtherCalc.setAmount(new BigDecimal(pendapatanLain.getAmount()));
                                if (pendapatanLain.getPeriode() != null || !pendapatanLain.getPeriode().equals(""))
                                    newOtherCalc.setPeriode(new BigDecimal(pendapatanLain.getPeriode()));
                                if (pendapatanLain.getPendapatanPerbulan() != null
                                        || !pendapatanLain.getPendapatanPerbulan().equals(""))
                                    newOtherCalc.setPendapatanPerbulan(
                                            new BigDecimal(pendapatanLain.getPendapatanPerbulan()));
                                newOtherCalc.setSwId(sw.getSwId());
                                try {
                                    swService.save(newOtherCalc);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error(CREATE_SW_FAILED);
                                    responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        } else {
                            for (SWOtherItemCalculationPOJO pendapatanLain : pendapatanLainList) {
                                OtherItemCalculation otherItemCalc = new OtherItemCalculation();
                                otherItemCalc.setName(pendapatanLain.getName());
                                if (pendapatanLain.getAmount() != null || !pendapatanLain.getAmount().equals(""))
                                    otherItemCalc.setAmount(new BigDecimal(pendapatanLain.getAmount()));
                                if (pendapatanLain.getPeriode() != null || !pendapatanLain.getPeriode().equals(""))
                                    otherItemCalc.setPeriode(new BigDecimal(pendapatanLain.getPeriode()));
                                if (pendapatanLain.getPendapatanPerbulan() != null
                                        || !pendapatanLain.getPendapatanPerbulan().equals(""))
                                    otherItemCalc.setPendapatanPerbulan(
                                            new BigDecimal(pendapatanLain.getPendapatanPerbulan()));
                                otherItemCalc.setSwId(sw.getSwId());
                                try {
                                    swService.save(otherItemCalc);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error(CREATE_SW_FAILED);
                                    responseCode.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        }
                        
                        //kondisi Send Mail Approval
                        if (swPojo.getCustomerType().equals("2") || sw.getCustomerRegistrationType().equals('2')) {
                            //save to tabel
                            SwForEmail swFormail = new SwForEmail();
                            swFormail.setFlagStatusScoring("Waiting For Scoring");
                            swFormail.setSwId(sw.getSwId());
                            swFormail.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                            swService.save(swFormail);
                            //Send to NOS
                            SWForEmailResponse fileSwForMail = new SWForEmailResponse();
                            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                            fileSwForMail.setFinancingReferenceId(responseCode.getRetrievalReferenceNumber());
                            fileSwForMail.setChannelId("MPR");
                            if (sw.getCustomerId() != null) {
                                Customer cifNumber = customerService.findById(sw.getCustomerId().toString());
                                if (cifNumber == null || cifNumber.equals("")) {
                                    fileSwForMail.setCif("");
                                } else {
                                    fileSwForMail.setCif(cifNumber.getCustomerCifNumber());
                                }
                            }
                            fileSwForMail.setBirthDate(dateFormat.format(sw.getBirthDate()));
                            if (sw.getMaritalStatus().equals("1")) {
                                fileSwForMail.setMaritalStatus(CustomerConstant.Marital_Menikah);
                            } else if (sw.getMaritalStatus().equals("2")) {
                                fileSwForMail.setMaritalStatus(CustomerConstant.Marital_Single);
                            } else {
                                fileSwForMail.setMaritalStatus(CustomerConstant.Marital_Tidak_Menikah);
                            }
                            if (sw.getSwId() != null) {
                                List<SWProductMapping> swIdPM = swService.findProductMapBySwId(sw.getSwId());
                                for (SWProductMapping swIdppm : swIdPM) {
                                    if (swIdppm.getProductId() != null) {
                                        LoanProduct loanProduList = swService.findByProductId(swIdppm.getRecommendedProductId().toString());
                                        fileSwForMail.setProductCode(loanProduList.getProductCode());
                                        if (sw.getBusinessField() == null
                                                || sw.getBusinessField().equals("0")
                                                || sw.getBusinessField().equals("")) {
                                            fileSwForMail.setBusinessFlag(false);
                                        } else {
                                            fileSwForMail.setBusinessFlag(true);
                                        }
                                        fileSwForMail.setPlafond(swIdppm.getRecommendedPlafon());
                                        fileSwForMail.setTenor(loanProduList.getInstallmentCount());
                                        fileSwForMail.setApplicationDate(dateFormat.format(sw.getSurveyDate()));
                                        fileSwForMail.setNotes("-");
                                        fileSwForMail.setFinancingStatus("2");
                                        fileSwForMail.setDisbursementDate(dateFormat.format(sw.getDisbursementDate()));
                                        fileSwForMail.setNoAppid(sw.getSwId().toString());
                                        fileSwForMail.setDeviationFlag(false);
                                    }

                                }
                            }
                            //send data to activeMq
                            sendMessageUtilMQ.sendMessage(" " + jsonUtils.toJson(fileSwForMail));
                            log.info("Send To ActiveMQ : " + jsonUtils.toJson(fileSwForMail));
                        } else {
                        	 int maxRetry = 0 ;
                            User findUser = userService.findUserByUsername(request.getUsername());
                            List<User> findUsernameAndEmail = userService.findUsernameCoandBm(findUser.getOfficeCode().toString(),
                                    WebGuiConstant.STATUS_ROLE_USER, request.getUsername());
                            SWSentEmailPOJO requestEmail = new SWSentEmailPOJO();
                            SystemParameter valueParam = swService.findByparamName(WebGuiConstant.PLAFON_PARAM);
                            Long nilaiParam = Long.valueOf(valueParam.getParamValue());
                            SWProductMapping productRecPlacon = swService.findByTopBySwid(sw.getSwId());
                            BigDecimal plafonrec = productRecPlacon.getRecommendedPlafon();
                            Long convertPlafonrec = plafonrec.longValue();
                            requestEmail.setUserName(request.getUsername());
                            requestEmail.setCustomerName(sw.getCustomerName());
                            requestEmail.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                            requestEmail.setSwId(String.valueOf(sw.getSwId()));
                            requestEmail.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                            for (User userEmail : findUsernameAndEmail) {
                                String email = userEmail.getEmail();
                                BigDecimal slimitUser = new BigDecimal(userEmail.getLimit() == null ? "0.0000" : userEmail.getLimit());
                                log.debug("Limit String User : " + slimitUser);
                                Long limitUser = slimitUser.longValue();
                                log.debug("Limit per User : " + limitUser);
                                log.debug("convertPlafonrec = " + convertPlafonrec);
                                log.debug("nilaiParam       = " + nilaiParam);
                                if (userEmail.getLimit() != null) {
                                    if (convertPlafonrec > limitUser) {
                                        requestEmail.setIsForwarded(true);
                                    } else {
                                        requestEmail.setIsForwarded(false);
                                    }
                                } else {
                                    if (convertPlafonrec > nilaiParam) {
                                        requestEmail.setIsForwarded(true);
                                    } else {
                                        requestEmail.setIsForwarded(false);
                                    }
                                }
                                requestEmail.setEmailAddress(email);
                                requestEmail.setApprovalUsername(userEmail.getUsername());
                                requestEmail.setIsInReview(false);
                                requestEmail.setKodeDeviasi("");	
                                requestEmail.setNote("-");
                                try {
                                    if (swService.hasIdPhoto(sw.getSwId().toString()).equals(false) &&
                                            swService.hasSurveyPhoto(sw.getSwId().toString()).equals(false)) {
                                        log.error("Photo Not Found");
                                        SentEmailNotifAfterNosPOJO requestForNotifFoto = new SentEmailNotifAfterNosPOJO();
                                        requestForNotifFoto.setTransmissionDateAndTime(null);
                                        requestForNotifFoto.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                                        requestForNotifFoto.setUserName(userEmail.getUsername());
                                        requestForNotifFoto.setCustomerName(sw.getCustomerName());
                                        requestForNotifFoto.setAppidNo("-");
                                        requestForNotifFoto.setSentra("-");
                                        requestForNotifFoto.setAlasan("-");
                                        requestForNotifFoto.setMms("-");
                                        //save to retry scheduler
                                        MailEngineRetail swRetail = new MailEngineRetail();
                                        swRetail.setStatus("not sent");
                                        swRetail.setSwId(Long.valueOf(requestEmail.getSwId()));
                                        swRetail.setUsername(requestForNotifFoto.getUserName());
                                        swRetail.setRetrievalReferenceNumber(requestForNotifFoto.getRetrievalReferenceNumber());
                                        swRetail.setEmailAddres(email);
                                        swRetail.setIsForwarded(requestEmail.getIsForwarded());
                                        swRetail.setCustumerName(requestForNotifFoto.getCustomerName());
                                        swRetail.setTransmissionDateAndTime(requestForNotifFoto.getTransmissionDateAndTime());
                                        swRetail.setApprovalUsername(requestEmail.getApprovalUsername());
                                        swRetail.setNote(requestEmail.getNote());
                                        swRetail.setKodeDeviasi(requestEmail.getKodeDeviasi());
                                        swRetail.setInReview(requestEmail.getIsInReview());
                                        swRetail.setMaxRetry(maxRetry);
                                        swRetail.setCreatedt(new Date());
                                        swRetail.setCreateby(request.getUsername());
                                        swRetail.setUpdateby(null);
                                        swRetail.setUpdatedt(null);
                                        swService.save(swRetail);
                                        log.debug("Send Email Notif Unkown Foto");
                                    } else if (sentEmailService.wsMailEngineValidation(requestEmail).contains(WebGuiConstant.RC_GENERAL_ERROR)) {
                                        log.error("Request Error " + jsonUtils.toJson(requestEmail));
                                        //save to retry scheduler
                                        MailEngineRetail swRetail = new MailEngineRetail();
                                        swRetail.setStatus("not sent");
                                        swRetail.setSwId(Long.valueOf(requestEmail.getSwId()));
                                        swRetail.setUsername(requestEmail.getUserName());
                                        swRetail.setRetrievalReferenceNumber(requestEmail.getRetrievalReferenceNumber());
                                        swRetail.setEmailAddres(email);
                                        swRetail.setIsForwarded(requestEmail.getIsForwarded());
                                        swRetail.setCustumerName(requestEmail.getCustomerName());
                                        swRetail.setTransmissionDateAndTime(requestEmail.getTransmissionDateAndTime());
                                        swRetail.setApprovalUsername(requestEmail.getApprovalUsername());
                                        swRetail.setNote(requestEmail.getNote());
                                        swRetail.setKodeDeviasi(requestEmail.getKodeDeviasi());
                                        swRetail.setInReview(requestEmail.getIsInReview());
                                        swRetail.setMaxRetry(maxRetry);
                                        swRetail.setCreatedt(new Date());
                                        swRetail.setCreateby(request.getUsername());
                                        swRetail.setUpdateby(null);
                                        swRetail.setUpdatedt(null);
                                        swService.save(swRetail);
                                    } else { // jika sudah jalan
                                        log.info("Send Email : " + WebGuiConstant.RC_SUCCESS + jsonUtils.toJson(requestEmail));
                                    }
                                } catch (Exception e) {
                                    log.error(e.getMessage());
                                    log.error("REQUEST SOMETHING PROBLEM FOR " + sw.getSwId());
                                    MailEngineRetail swRetail = new MailEngineRetail();
                                        swRetail.setStatus("not sent");
                                        swRetail.setSwId(Long.valueOf(requestEmail.getSwId()));
                                        swRetail.setUsername(requestEmail.getUserName());
                                        swRetail.setRetrievalReferenceNumber(requestEmail.getRetrievalReferenceNumber());
                                        swRetail.setEmailAddres(email);
                                        swRetail.setIsForwarded(requestEmail.getIsForwarded());
                                        swRetail.setCustumerName(requestEmail.getCustomerName());
                                        swRetail.setTransmissionDateAndTime(requestEmail.getTransmissionDateAndTime());
                                        swRetail.setApprovalUsername(requestEmail.getApprovalUsername());
                                        swRetail.setNote(requestEmail.getNote());
                                        swRetail.setKodeDeviasi(requestEmail.getKodeDeviasi());
                                        swRetail.setInReview(requestEmail.getIsInReview());
                                        swRetail.setMaxRetry(maxRetry);
                                        swRetail.setCreatedt(new Date());
                                        swRetail.setCreateby(request.getUsername());
                                        swRetail.setUpdateby(null);
                                        swRetail.setUpdatedt(null);
                                        swService.save(swRetail);     
                                }
                            }
                        }
                    } else {
                        log.error("SW ALREADY EXISTS");
                        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_SW_ALREADY_EXIST);
                        responseCode.setResponseMessage(label);
                        responseCode.setSwId(swPojo.getSwId());
                        return responseCode;
                    }
                }
            }
            if (swId != null) {
                if (!swId.equals("")) {
                    SurveyWawancara swResponse = swService.getSWById(swId);
                    if (swResponse != null) {
                        if (swResponse.getLocalId() != null) {
                            responseCode.setSwId(swResponse.getLocalId());
                        }
                    }
                }
            }
            responseCode.setLocalId(request.getLocalId());
            log.info("FINISH DELAY PROGRAM");
            log.info("ADD SW FINISHED");
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
        } catch (Exception e) {
            log.error("addSW error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("addSW error: " + e.getMessage());
        } finally {
            try {
                log.trace("Try to create Terminal Activity and Audit Log");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_SW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addSW", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                        // save ke audit log
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SW);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        if (!request.toString().equals(null) && !(request.toString().length() < 1000)
                                || !request.toString().equals("") && !(request.toString().length() < 1000))
                            auditLog.setDescription(
                                    request.toString().substring(0, 1000) + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.info("AUDIT LOG LOGGING SUCCESS...");
                        log.info("Updating AUDIT LOG");
                    }
                });
                log.info("addSW RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("addSW ERROR terminalActivity, on SW_SUBMIT_DATA_REQUEST : " + e.getMessage());
            }
            return responseCode;
        }

    }

    /*
     * @author : Ilham
     * @Since  : 20190220
     * @category patch 
     *   - Approval By Email
     */   
    @RequestMapping(value = WebGuiConstant.SW_APPROVAL_DATA, method = {RequestMethod.POST})
    public @ResponseBody
    SWResponse doApproval(@RequestBody String requestString) throws Exception {
        String swId = "";
        Object obj = new JsonUtils().fromJson(requestString, SWRequest.class);
        final SWRequest request = (SWRequest) obj;
        final SWResponse responseCode = new SWResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        // Validasi messaging request dari Android
        try {
            log.info("approvalSW INCOMING MESSAGE : " + jsonUtils.toJson(request));
            // cek sw id yang akan di approve tidak kosong
            if (!request.getSwId().equals("")) {
                Long swIdd = Long.valueOf(request.getSwId());
                SurveyWawancara sw = swService.findSwByswId(swIdd);
                if (sw == null) {
                    log.error("UNKNOWN SW ID");
                    responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                User userApproval = userService.findUserByUsername(request.getUsername());
                if (sw.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                    log.error("SW ALREADY APPROVED");
                    String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_SW_ALREADY_APPROVED);
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                log.info("STARTING TO APPROVE SW");
                if (swService.hasIdPhoto(sw.getSwId().toString()).equals(false) &&
                        swService.hasSurveyPhoto(sw.getSwId().toString()).equals(false)) {
                    log.error("Photo Not Found");
                    responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                // jika status sudah approve
                if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                    sw.setStatus(WebGuiConstant.STATUS_APPROVED);
                    sw.setUpdatedDate(new Date());
                    sw.setUpdatedBy(request.getUsername());
                    if (userApproval.getRoleUser().equals("2")) {
                        sw.setHasApprovedMs(true);
                    }
                    List<SWApprovalHistoryReq> listApproval = request.getHistory();
                    for (SWApprovalHistoryReq approvalHist : listApproval) {
                        SWUserBWMPMapping swUserBwmpMap = swService.findBySwAndUser(sw.getSwId(),
                                userApproval.getUserId());
                        // jika approval langsung tanpa ada waiting for
                        // approval ke pejabat lainnya
                        if (swUserBwmpMap == null) {
                            swUserBwmpMap = new SWUserBWMPMapping();
                            swUserBwmpMap.setUserId(userApproval.getUserId());
                            swUserBwmpMap.setSwId(sw.getSwId());
                        }
                        if (swUserBwmpMap.getLevel().equals(approvalHist.getLevel())
                                && !swUserBwmpMap.getUserId().equals(userApproval.getUserId())) {
                            log.error("SW APPROVED FAILED");
                            responseCode.setResponseCode(WebGuiConstant.RC_SW_APPROVED_FAILED);
                            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                            responseCode.setResponseMessage(label);
                            return responseCode;
                        }
                        swUserBwmpMap.setStatus(WebGuiConstant.STATUS_APPROVED);
                        swUserBwmpMap.setCreatedDate(new Date());
                        swUserBwmpMap.setCreatedBy(request.getUsername());
                        if (request.getHistory() != null) {
                            swUserBwmpMap.setLevel(approvalHist.getLevel());
                            swUserBwmpMap.setDate(approvalHist.getDate());
                        }
                        swService.save(swUserBwmpMap);
                    }
                    // jika status reject
                } else if (request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                    sw.setStatus(WebGuiConstant.STATUS_REJECTED);
                    sw.setRejectedReason(request.getRejectedReason());
                    sw.setUpdatedBy(request.getUsername());
                    // jika status waiting for approval ke pejabat lainnya
                } else if (request.getStatus().equals(WebGuiConstant.STATUS_WAITING_APPROVAL)) {
                    sw.setStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
                    if (userApproval.getRoleUser().equals("2")) {
                        sw.setHasApprovedMs(true);
                    }
                    log.debug("SET APPROVAL HISTORY");
                    List<SWApprovalHistoryReq> listApproval = request.getHistory();
                    for (SWApprovalHistoryReq approvalHist : listApproval) {
                        SWUserBWMPMapping swUserBwmpMap = swService.findBySwAndLevel(sw.getSwId(),
                                approvalHist.getLevel());
                        if (swUserBwmpMap == null)
                            swUserBwmpMap = new SWUserBWMPMapping();
                        swUserBwmpMap.setSwId(sw.getSwId());
                        if (approvalHist.getSupervisorId() != null) {
                            swUserBwmpMap.setUserId(Long.parseLong(approvalHist.getSupervisorId()));
                        } else {
                            swUserBwmpMap.setUserId(userApproval.getUserId());
                        }
                        swUserBwmpMap.setCreatedBy(request.getUsername());
                        swUserBwmpMap.setCreatedDate(new Date());
                        swUserBwmpMap.setLevel(approvalHist.getLevel());
                        swUserBwmpMap.setDate(approvalHist.getDate());
                        swUserBwmpMap.setStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
                        swService.save(swUserBwmpMap);
                    }
                } else if (request.getStatus().equals(WebGuiConstant.STATUS_RETURN)) {
                    sw.setStatus(WebGuiConstant.STATUS_RETURN);
                    sw.setUpdatedDate(new Date());
                    sw.setUpdatedBy(request.getUsername());
                    if (userApproval.getRoleUser().equals("2")) {
                        sw.setHasApprovedMs(true);
                    }
                    log.debug("SET APPROVAL HISTORY");
                    List<SWApprovalHistoryReq> listApproval = request.getHistory();
                    for (SWApprovalHistoryReq approvalHist : listApproval) {
                        SWUserBWMPMapping swUserBwmpMap = swService.findBySwAndUser(sw.getSwId(),
                                userApproval.getUserId());
                        // jika approval langsung tanpa ada waiting for
                        // approval ke pejabat lainnya
                        if (swUserBwmpMap == null) {
                            swUserBwmpMap = new SWUserBWMPMapping();
                            swUserBwmpMap.setUserId(userApproval.getUserId());
                            swUserBwmpMap.setSwId(sw.getSwId());
                        }
                        if (swUserBwmpMap.getLevel().equals(approvalHist.getLevel())
                                && !swUserBwmpMap.getUserId().equals(userApproval.getUserId())) {
                            log.error("SW RETURNED FAILED");
                            responseCode.setResponseCode(WebGuiConstant.RC_SW_APPROVED_FAILED);
                            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                            responseCode.setResponseMessage(label);
                            return responseCode;
                        }
                        swUserBwmpMap.setStatus(WebGuiConstant.STATUS_RETURN);
                        swUserBwmpMap.setCreatedDate(new Date());
                        swUserBwmpMap.setCreatedBy(request.getUsername());
                        if (request.getHistory() != null) {
                            swUserBwmpMap.setLevel(approvalHist.getLevel());
                            swUserBwmpMap.setDate(approvalHist.getDate());
                        }
                        swService.save(swUserBwmpMap);
                    }
                } else {
                    log.error("UNKNOWN SW ID");
                    responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                }
                swId = sw.getLocalId();
                swService.save(sw);
                // mapping sw dengan customer existing
                if (sw.getSwId() != null) {
                    if (customerService.getBySwId(sw.getSwId()) == null) {
                        if (sw.getCustomerId() != null) {
                            Customer cust = customerService.findById(sw.getCustomerId().toString());
                            if (cust != null) {
                                cust.setCustomerId(cust.getCustomerId());
                                cust.setSwId(sw.getSwId());
                                cust.setApprovedDate(new Date());
                                cust.setCustomerName(sw.getCustomerName());
                                customerService.save(cust);
                            }
                        }
                    }
                }
            } else {
                log.error("UNKNOWN SW ID");
                responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                return responseCode;
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            responseCode.setSwId(swId);
        } catch (Exception e) {
            log.error("approvalSW ERROR  : " + e.getMessage(), e);
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("approvalSW error: " + e.getMessage());
        }
        return responseCode;
    }

}