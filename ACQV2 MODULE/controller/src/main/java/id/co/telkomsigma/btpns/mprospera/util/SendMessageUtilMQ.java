package id.co.telkomsigma.btpns.mprospera.util;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.ObjectMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.manager.DataNosManager;
import id.co.telkomsigma.btpns.mprospera.service.DataNosService;

@Component
public class SendMessageUtilMQ  {
	
	@Autowired
	private DataNosService dataNosService;
//	
	@Autowired
	private DataNosManager dataNosManager;

//	final String topicName = System.getProperty("topicName");
//	final String urlBroker = System.getProperty("urlBroker");	
//	final String tenantNOS = System.getProperty("tenantNOS");
//	final String urlBroker = dataNosService.findByParamName(WebGuiConstant.PARAM_URL_BROKER).getParamValue();
//	final String topicName = dataNosService.findByParamName(WebGuiConstant.PARAM_TOPICNAME).getParamValue();
//	final String tenantNOS = dataNosService.findByParamName(WebGuiConstant.PARAM_TENANTNOS).getParamValue();
	
	String urlBroker;
	String topicName;
	String tenantNOS;
	
	ActiveMQConnectionFactory connectionFactory ;
	Connection connection;
	Session session;
	Destination destination;
    MessageProducer producer;
    ObjectMessage om;
    TextMessage tm;
	
    @PostConstruct
    protected void init() {
    	 urlBroker = dataNosService.findByParamName(WebGuiConstant.PARAM_URL_BROKER).getParamValue();
    	 topicName = dataNosService.findByParamName(WebGuiConstant.PARAM_TOPICNAME).getParamValue();
    	 tenantNOS = dataNosService.findByParamName(WebGuiConstant.PARAM_TENANTNOS).getParamValue();
    	
    }
    

	private void start() throws JMSException {
		
		connectionFactory = new ActiveMQConnectionFactory(urlBroker);
		//connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
 
        destination = session.createQueue(topicName);
        producer = session.createProducer(destination);
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);      
	}
	
	private void close() throws JMSException {
		connection.close();
	}

	public void sendMessage(String msg) throws Exception {
		start();
		tm = session.createTextMessage();   
		tm.setStringProperty("action", "post-job");
		tm.setStringProperty("X-Tenant-Identifier",tenantNOS);
	    tm.setText(msg);         
	    producer.send(tm);
		close();
	}
		
	public void sendMessage(Serializable msg) throws Exception {   
		 start();
		 om = session.createObjectMessage();  
	     om.setObject(msg);         
         producer.send(om);	
		 close();
	}	
	
}
