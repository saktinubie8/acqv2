package id.co.telkomsigma.btpns.mprospera.util;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.service.DataNosService;

@Configuration
public class JmsConfig {
	@Autowired
	private DataNosService dataNosService;
	
//	final String urlBroker = System.getProperty("urlBroker");
//	String BROKER_USERNAME = "admin"; 
//	String BROKER_PASSWORD = "admin";

	@Bean
	public ActiveMQConnectionFactory connectionFactory(){
	    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
	    //connectionFactory.setBrokerURL("tcp://localhost:61616");
		String urlBroker = dataNosService.findByParamName(WebGuiConstant.PARAM_URL_BROKER).getParamValue();
		String usernameMQ = dataNosService.findByParamName(WebGuiConstant.PARAM_USERNAMEMQ).getParamValue();
		String passwordMQ = dataNosService.findByParamName(WebGuiConstant.PARAM_PASSWORDMQ).getParamValue();
	    connectionFactory.setBrokerURL(urlBroker);	    
	    connectionFactory.setPassword(usernameMQ);
	    connectionFactory.setUserName(passwordMQ);
	    return connectionFactory;
	}

	@Bean
	public JmsTemplate jmsTemplate(){
	    JmsTemplate template = new JmsTemplate();
	    template.setConnectionFactory(connectionFactory());
	    return template;
	}
	
	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
	    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
	    factory.setConnectionFactory(connectionFactory());
	    factory.setConcurrency("1-1");
	    return factory;
	}
	
}
