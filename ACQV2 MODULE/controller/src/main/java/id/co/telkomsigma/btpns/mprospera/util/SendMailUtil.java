package id.co.telkomsigma.btpns.mprospera.util;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.product.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.ScoringRuleNosPojo;
import id.co.telkomsigma.btpns.mprospera.service.DataNosService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.SentEmailService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

public class SendMailUtil {

    @Autowired
    private DataNosService dataNosService;

    @Autowired
    private SWService swService;

    @Autowired
    private UserService userService;

    @Autowired
    private SentEmailService sentEmailService;

    protected final Log LOGGER = LogFactory.getLog(getClass());

    public void sentemailApproval(String getOfficeCode, Long swIdd, String rrn, String username, String status, List<ScoringRuleNosPojo> scoringResult) {

        List<User> findEmail = userService.findEmail(getOfficeCode, WebGuiConstant.STATUS_ROLE_USER);
        SWSentEmailPOJO requ = new SWSentEmailPOJO();
        SystemParameter valueParam = swService.findByparamName(WebGuiConstant.PLAFON_PARAM);
        Long nilaiParam = Long.valueOf(valueParam.getParamValue());
        SWProductMapping productRecPlacon = swService.findByTopBySwid(swIdd);
        BigDecimal plafonrec = productRecPlacon.getRecommendedPlafon();
        Long convertPlafonrec = plafonrec.longValue();
        requ.setTransmissionDateAndTime(null);
        LOGGER.debug("transmisiion  requ              : " + requ.getTransmissionDateAndTime());
        requ.setRetrievalReferenceNumber(rrn);
        LOGGER.debug("RetrievalReferenceNumber requ   : " + requ.getRetrievalReferenceNumber());
        requ.setUserName(username);
        LOGGER.debug(" create by Reuqu                : " + requ.getUserName());
        Long swId = swIdd;
        requ.setSwId(String.valueOf(swId));
        LOGGER.debug("SWID                            : " + requ.getSwId());
        if (convertPlafonrec > nilaiParam) {
            requ.setIsForwarded(true);
        } else {
            requ.setIsForwarded(false);
        }
        LOGGER.debug("REQUEST ISFORWARDED             : " + requ.getIsForwarded());
        for (User userEmail : findEmail) {
            String email = userEmail.getEmail();
            requ.setEmailAddress(email);
            LOGGER.debug("Email Address               : " + requ.getEmailAddress());
            requ.setApprovalUsername(userEmail.getUsername());
            LOGGER.debug("Approval Username           : " + requ.getApprovalUsername());
            if ("APPROVED".equals(status)) {
                requ.setNote(status);
                LOGGER.debug("NOTE    : " + requ.getNote());
            } else {
                String note = "-";
                List<ScoringRuleNosPojo> noted = scoringResult;
                for (ScoringRuleNosPojo ws : noted) {
                    if (ws.getResult().equals("FAILED")) {
                        note = "" + ws.getRejectDesc() + ", " + ws.getScoringRule() + ", " + ws.getParam() + ", " + ws.getResult();
                    }
                }
                requ.setNote(note);
                LOGGER.debug("NOTE    : " + requ.getNote());
            }
            dataNosService.generateNotSend(requ);
        }
    }

}