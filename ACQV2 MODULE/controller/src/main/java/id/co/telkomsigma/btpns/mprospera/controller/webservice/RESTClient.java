package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.request.DeviationRequest;
import id.co.telkomsigma.btpns.mprospera.request.ESBLoanRequest;
import id.co.telkomsigma.btpns.mprospera.response.AddLoanResponse;
import id.co.telkomsigma.btpns.mprospera.response.DeviationResponse;
import id.co.telkomsigma.btpns.mprospera.service.CustomerService;
import id.co.telkomsigma.btpns.mprospera.service.LoanService;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import id.co.telkomsigma.btpns.mprospera.service.SentraService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

@Component("restClient")
public class RESTClient {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    private String HOST;
    private String PORT;
    private String MAIN_URI;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    ParameterService parameterService;

    @PostConstruct
    protected void init() {
        HOST = parameterService.loadParamByParamName("esb.host", "https://10.7.17.163");
        PORT = parameterService.loadParamByParamName("esb.port", "8443");
        MAIN_URI = parameterService.loadParamByParamName("esb.uri", "/btpns/mprospera");
    }

    public Boolean echoSuccess() {
        String url = HOST + ":" + PORT + MAIN_URI + "/echo";
        RestTemplate restTemplate = getRestTemplate();
        try {
            LOGGER.info("SEND ECHO...");
            ResponseEntity<String> response = restTemplate.postForEntity(url, null, String.class);
            LOGGER.info("ECHO SUCCESS...");
            return response.getStatusCode().is2xxSuccessful();
        } catch (HttpClientErrorException e) {
            HttpStatus httpStatus = e.getStatusCode();
            LOGGER.error("ECHO FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
            return e.getStatusCode().is2xxSuccessful();
        } catch (Exception e) {
            LOGGER.error("ECHO FAILED..., " + e.getMessage());
            return false;
        }
    }

    public AddLoanResponse addLoan(ESBLoanRequest request) throws NoSuchAlgorithmException, KeyManagementException {
        AddLoanResponse response = new AddLoanResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/addLoan";
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, AddLoanResponse.class);
                LOGGER.info("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addLoan ESB Error: " + e.getMessage());
                return response;
            } catch (Exception e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addLoan ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public DeviationResponse addDeviation(DeviationRequest request)
            throws NoSuchAlgorithmException, KeyManagementException {
        DeviationResponse response = new DeviationResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/claim/deviation";
            LOGGER.debug("POST URL : " + url);
            try {
                DeviationRequest restRequest = new DeviationRequest();
                restRequest.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                restRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                restRequest.setImei(request.getImei());
                restRequest.setUsername(request.getUsername());
                restRequest.setNoappid(request.getNoappid());
                restRequest.setUserApproval(request.getUserApproval());
                restRequest.setRole(request.getRole());
                response = getRestTemplate().postForObject(url, restRequest, DeviationResponse.class);
                LOGGER.info("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addDeviation ESB Error: " + e.getMessage());
                return response;
            } catch (Exception e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addDeviation ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    protected RestTemplate getRestTemplate() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                    throws CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                    throws CertificateException {
            }
        }};
        // Install the all-trusting trust manager
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e1) {
            LOGGER.debug("SSL INSTANCE FETCHING FAILED..., " + e1.getMessage());
        }
        try {
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            LOGGER.debug("SSL CONTEXT INITIALIZING FAILED..., " + e.getMessage());
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        final HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        RestTemplate restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                if (connection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) connection).setHostnameVerifier(allHostsValid);
                }
                super.prepareConnection(connection, httpMethod);
            }
        });
        return restTemplate;
    }

}
