package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomer;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerDropOut;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerTopUp;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.PotentialCustomerPOJO;
import id.co.telkomsigma.btpns.mprospera.request.SyncPotentialCustomerRequest;
import id.co.telkomsigma.btpns.mprospera.response.SyncPotentialCustomerResponse;
import id.co.telkomsigma.btpns.mprospera.service.SyncPotentialCustomerService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webServiceSyncPotentialCustomerController")
public class WebServiceSyncPotentialCustomerController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalService terminalSerivce;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private SyncPotentialCustomerService syncPotentialCustomerService;

    @Autowired
    private UserService userService;

    @PostMapping(value = WebGuiConstant.POTENTIAL_CUSTOMER_SYNC_REQUEST)
    public @ResponseBody
    SyncPotentialCustomerResponse doSyncPotentialCusWithGrade(
            @RequestBody final SyncPotentialCustomerRequest request,
            @PathVariable("apkVersion") String apkVersion) {

        final SyncPotentialCustomerResponse responseCode = new SyncPotentialCustomerResponse();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        try {
            log.info("syncPotentialCustomer INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                log.error("Validation Failed for Username : " + request.getUsername() + " , imei : " + request.getImei()
                        + " , sessionKey : " + request.getSessionKey() + label);
            } else {
                log.debug("Getting All Potential Customer Data");
                User user = userService.findUserByUsername(request.getUsername());
                List<PotentialCustomer> dataPotentialCustMaturity = syncPotentialCustomerService.getPotentialCustomer(Integer.parseInt(user.getOfficeCode()));
                if (!dataPotentialCustMaturity.isEmpty() || dataPotentialCustMaturity != null) {
                    for (PotentialCustomer cus : dataPotentialCustMaturity) {
                        PotentialCustomerPOJO potentialCUstomerPojo = new PotentialCustomerPOJO();
                        potentialCUstomerPojo.setCustomerId(cus.getCustomerId().toString());
                        potentialCUstomerPojo.setCustomerName(cus.getCustomerName());
                        potentialCUstomerPojo.setCifNumber(cus.getCifNumber());
                        Date duedate = new Date(cus.getMaturityDate().getTime());
                        potentialCUstomerPojo.setMaturityDate(formatDate.format(duedate));
                        potentialCUstomerPojo.setProductName(cus.getProductName());
                        potentialCUstomerPojo.setPlafond(cus.getPlafond());
                        potentialCUstomerPojo.setSentraName(cus.getSentraName());
                        potentialCUstomerPojo.setJmlKunjungan(cus.getNumberOfVisit());
                        potentialCUstomerPojo.setMaturityFlag(true);
                        responseCode.getCustomerList().add(potentialCUstomerPojo);
                    }
                }

                List<PotentialCustomerTopUp> dataPotentialCusTopUp = syncPotentialCustomerService.getPotentialCustomerTopUp(Integer.parseInt(user.getOfficeCode()));

                if (!dataPotentialCusTopUp.isEmpty() || dataPotentialCusTopUp != null) {
                    loopMaturity:
                    for (PotentialCustomerTopUp cus : dataPotentialCusTopUp) {
                        int c = -1;
                        for (PotentialCustomerPOJO p : responseCode.getCustomerList()) {
                            c++;
                            if (cus.getCifNumber().equals(p.getCifNumber())) {
                                responseCode.getCustomerList().get(c).setMaturityFlag(true);
                                responseCode.getCustomerList().get(c).setTopUpFlag(true);
                                if (c <= dataPotentialCusTopUp.size()) {
                                    continue loopMaturity;
                                }
                            }
                        }
                        if (cus.getJmlAngsur() != null) {
                            String grade = syncPotentialCustomerService.getGrade(cus.getJmlAngsur(),
                                    cus.getTransaksiBailout(), cus.getFrekwensiAbsen());
                            if ("A".equals(grade) || "B".equals(grade)) {
                                PotentialCustomerPOJO potentialCUstomerPojo = new PotentialCustomerPOJO();
                                potentialCUstomerPojo.setCustomerId(cus.getCustomerId().toString());
                                potentialCUstomerPojo.setCustomerName(cus.getCustomerName());
                                potentialCUstomerPojo.setCifNumber(cus.getCifNumber());
                                Date duedate = new Date(cus.getMaturityDate().getTime());
                                potentialCUstomerPojo.setMaturityDate(formatDate.format(duedate));
                                potentialCUstomerPojo.setProductName(cus.getProductName());
                                potentialCUstomerPojo.setPlafond(cus.getPlafond());
                                potentialCUstomerPojo.setSentraName(cus.getSentraName());
                                potentialCUstomerPojo.setJmlKunjungan(cus.getNumberOfVisit());
                                potentialCUstomerPojo.setTopUpFlag(true);
                                responseCode.getCustomerList().add(potentialCUstomerPojo);
                            }
                        }
                    }
                }
                List<PotentialCustomerDropOut> dataPotentialCustDropOut = syncPotentialCustomerService.getPotentialCustomerDropOut(Integer.parseInt(user.getOfficeCode()));
                if (!dataPotentialCustDropOut.isEmpty() || dataPotentialCustDropOut != null) {
                    loopDropout:
                    for (PotentialCustomerDropOut cus : dataPotentialCustDropOut) {
                        int c = -1;
                        for (PotentialCustomerPOJO p : responseCode.getCustomerList()) {
                            c++;
                            if (cus.getCifNumber().equals(p.getCifNumber())) {
                                responseCode.getCustomerList().get(c).setDropOutFlag(true);
                                if (c <= dataPotentialCustDropOut.size()) {
                                    continue loopDropout;
                                }
                            }
                        }
                        PotentialCustomerPOJO potentialCUstomerPojo = new PotentialCustomerPOJO();
                        potentialCUstomerPojo.setCustomerId(cus.getCustomerId().toString());
                        potentialCUstomerPojo.setCustomerName(cus.getCustomerName());
                        potentialCUstomerPojo.setCifNumber(cus.getCifNumber());
                        //Date duedate = new Date(cus.getMaturityDate().getTime());
                        String matur = cus.getMaturityDate() != null ? formatDate.format(cus.getMaturityDate()) : "";
                        String update = cus.getPrsUpdateDate() != null ? formatDate.format(cus.getPrsUpdateDate()) : "";
                        potentialCUstomerPojo.setMaturityDate(cus.getEarlyTerminationFlag() != null ? matur == update || cus.getEarlyTerminationFlag() == true ? update : matur : matur);
                        potentialCUstomerPojo.setProductName("");
                        potentialCUstomerPojo.setPlafond(0);
                        potentialCUstomerPojo.setSentraName(cus.getSentraName());
                        potentialCUstomerPojo.setJmlKunjungan(cus.getNumberOfVisit());
                        potentialCUstomerPojo.setDropOutFlag(true);
                        responseCode.getCustomerList().add(potentialCUstomerPojo);
                    }
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                }
            }
        } catch (Exception e) {
            log.error("sync Potential Customer ERROR : " + e.getMessage());
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("syncCustomerPotential ERROR : " + e.getMessage());
        } finally {
            log.debug("Trying to Create Terminal Activity");
            try {
                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {
                        Terminal terminal = terminalSerivce.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_POTENTIAL_CUSTOMER);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }

                });
                log.debug("Finishing create Terminal Activity");
                log.debug("syncPotentialCustomer RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("syncPotentialCustomer saveTerminalActivityAndMessageLogs ERROR : " + e.getMessage());
            }
        }
        return responseCode;
    }

}