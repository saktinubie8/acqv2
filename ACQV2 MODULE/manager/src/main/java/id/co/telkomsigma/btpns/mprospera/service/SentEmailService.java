package id.co.telkomsigma.btpns.mprospera.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.feign.MailEngineInterface;
import id.co.telkomsigma.btpns.mprospera.feign.SendMailNotiifAfterNosInterface;
import id.co.telkomsigma.btpns.mprospera.feign.SendNotificationInterface;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SentEmailNotifAfterNosPOJO;

@Service("sentEmailService")
public class SentEmailService extends GenericService {

    @Autowired
    private MailEngineInterface mailEngineInterface;

    @Autowired
    private SendNotificationInterface sendNotificationInterface;

    @Autowired
    private SendMailNotiifAfterNosInterface sendMailNotiifAfterNosInterface;

    public String wsMailEngineValidation(SWSentEmailPOJO request) { // send email Approval
        return mailEngineInterface.mailEngineInterface(request);
    }

    public String wsMailSendNotification(SentEmailNotifAfterNosPOJO sentEmailNotifAfterNosPOJO) { //notif tidak ada photo
        return sendNotificationInterface.sendMailNotificationInterface(sentEmailNotifAfterNosPOJO);
    }

    public String wsMailSendNotifAfterNos(SentEmailNotifAfterNosPOJO sentEmailNotifAfterNosPOJO) { //notif Rejected
        return sendMailNotiifAfterNosInterface.sendMailNotifAfterNosInterface(sentEmailNotifAfterNosPOJO);
    }

}