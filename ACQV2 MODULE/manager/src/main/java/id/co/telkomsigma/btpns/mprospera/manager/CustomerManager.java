package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

public interface CustomerManager {

    Integer countCustomerByUsername(String username);

    void clearCache();

    void save(Customer customer);

    Customer getBySwId(Long swId);

    Customer findById(long parseLong);

}
