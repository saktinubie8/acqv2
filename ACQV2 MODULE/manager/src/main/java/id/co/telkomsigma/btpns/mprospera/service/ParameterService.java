package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.ParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by Dzulfiqar on 11/12/15.
 */
@Service("parameterService")
public class ParameterService extends GenericService {
    @Autowired
    private ParamManager paramManager;

    public String loadParamByParamName(final String paramName, final String defaultValue) {
        SystemParameter param = paramManager.getParamByParamName(paramName);
        if (param != null)
            return param.getParamValue();
        return defaultValue;
    }
}