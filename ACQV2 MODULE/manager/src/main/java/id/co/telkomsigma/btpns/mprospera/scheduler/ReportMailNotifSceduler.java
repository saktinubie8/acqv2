package id.co.telkomsigma.btpns.mprospera.scheduler;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineNotif;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SentEmailNotifAfterNosPOJO;
import id.co.telkomsigma.btpns.mprospera.service.MailEngineService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.UtilCollection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 * @author : Ilham
 * @Since  : 20190220 
 */

@Component
public class ReportMailNotifSceduler {

    JsonUtils jsonUtils = new JsonUtils();

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    private MailEngineService mailEngineService;

    private UtilCollection util = UtilCollection.getInstance();

    @Bean
    public String getCronValueNotif() {
        return mailEngineService.findByParamName(WebGuiConstant.CRON_TIME_MAILENGINE).getParamValue();
    }

    @Scheduled(cron = "#{@getCronValueNotif}")
    public void runMailNotifScheduler() {
        try {
            String ipRun = mailEngineService.findByParamName(WebGuiConstant.IP_MAILENGINE_FROM_DB).getParamValue();
            LOGGER.info("Ip Dari DB : " + ipRun);
            String ip = util.getIpAddress();
            LOGGER.info("Ip Ambil Dari Server : " + ip);
            if (ip.equals(ipRun)) {
                LOGGER.info("NOTIF MaiL Scheduler running.....START...!!");
                mailEngineService.getbyStatus(WebGuiConstant.NOT_SENT);
                List<MailEngineNotif> mailNotifStatus = mailEngineService.getbyStatus(WebGuiConstant.NOT_SENT);
                for (MailEngineNotif mf : mailNotifStatus) {
                    SentEmailNotifAfterNosPOJO reque = new SentEmailNotifAfterNosPOJO();
                    reque.setTransmissionDateAndTime(null);
                    reque.setRetrievalReferenceNumber(mf.getRetrievalReferenceNumber());
                    reque.setUserName(mf.getUsername());
                    reque.setCustomerName(mf.getCustumerName());
                    reque.setAppidNo(mf.getAppidNo());
                    reque.setSentra(mf.getSentra());
                    reque.setAlasan(mf.getAlasan());
                    reque.setMms(mf.getMms());
                    //kondisi send ulang
                    if (mf.getStatus().equals(WebGuiConstant.NOT_SENT)) {
                        MailEngineNotif ubah = mailEngineService.getById(mf.getId());
                        String mailResponsenotif = mailEngineService.generatNotSendNotif(reque);
                        if (reque.getUserName() != null) {
                            if (mailResponsenotif.contains(WebGuiConstant.RC_SUCCESS)) {
                                if (ubah != null) {
                                    LOGGER.info(WebGuiConstant.RC_SUCCESS + jsonUtils.toJson(reque));
                                    mailEngineService.save(ubah);
                                } else {
                                    LOGGER.info(WebGuiConstant.RC_GENERAL_ERROR + jsonUtils.toJson(reque));
                                }
                            }
                        } else {
                            mailEngineService.save(ubah);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("getCronValueNotif error: " + e.getMessage());
        }
    }

}