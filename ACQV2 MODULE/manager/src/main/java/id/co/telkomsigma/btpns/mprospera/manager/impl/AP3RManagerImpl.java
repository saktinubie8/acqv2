package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.AP3RDao;
import id.co.telkomsigma.btpns.mprospera.dao.FundedThingsDao;
import id.co.telkomsigma.btpns.mprospera.manager.AP3RManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service("ap3rManager")
public class AP3RManagerImpl implements AP3RManager {

    @Autowired
    private FundedThingsDao fundedThingsDao;

    @Autowired
    private AP3RDao ap3rDao;

    @Override
    public int countAll() {
        return 0;
    }

    @Override
    public void save(FundedThings product) {
        // TODO Auto-generated method stub
        fundedThingsDao.save(product);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "ppiacq.ap3r.findAp3rById", key = "#ap3r.ap3rId"),
    })
    public void save(AP3R ap3r) {
        // TODO Auto-generated method stub
        ap3rDao.save(ap3r);
    }

    @Override
    public AP3R findByLocalId(String localId) {
        return ap3rDao.findByLocalId(localId);
    }

    @Override
    public BigDecimal sumFunds(AP3R ap3r) {
        BigDecimal total = new BigDecimal(0);
        for (FundedThings fundedThing : fundedThingsDao.findByAp3rId(ap3r)) {
            total = total.add(fundedThing.getPrice());
        }
        return total;
    }

    @Override
    public List<AP3R> findAp3rListBySwId(Long swId) {
        return ap3rDao.findBySwIdAndIsDeleted(swId, false);
    }
    
    @Override
    public void delete(FundedThings products) {
        fundedThingsDao.delete(products);
    }

    @Override
    public Page<AP3R> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        return null;
    }
    @Override
    public List<FundedThings> findByAp3rId(AP3R ap3r) {
        // TODO Auto-generated method stub
        return fundedThingsDao.findByAp3rId(ap3r);
    }

}
