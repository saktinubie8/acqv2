package id.co.telkomsigma.btpns.mprospera.dao;


import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ManageApkDao extends JpaRepository<ManageApk, Long> {

    ManageApk findByApkVersion(String version);

}
