package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;

import java.util.List;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
public interface ParamManager {

    SystemParameter getParamByParamName(String paramName);

    void clearCache();

}