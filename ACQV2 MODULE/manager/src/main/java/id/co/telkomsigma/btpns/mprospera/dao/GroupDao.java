package id.co.telkomsigma.btpns.mprospera.dao;


import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupDao extends JpaRepository<Group, Long> {

    Group findByRrn(String Rrn);

}
