package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sw.SWUserBWMPMapping;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Dzulfiqar on 13/04/2017.
 */
public interface SwUserBwmpMapDao extends JpaRepository<SWUserBWMPMapping, Long> {

    SWUserBWMPMapping findBySwIdAndUserId(Long sw, Long userId);
    
    SWUserBWMPMapping findBySwIdAndLevel(Long sw, String level);

    List<SWUserBWMPMapping> findBySwId(Long sw);

}
