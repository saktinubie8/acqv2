package id.co.telkomsigma.btpns.mprospera.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;

@FeignClient("mailEngine")
public interface MailEngineInterface {
	
    @HystrixCommand
    @HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds", value = "300000")
    @RequestMapping(value = "/mailengine/webservice/sendEmail", method = {RequestMethod.POST})
    String mailEngineInterface(
    		
    		@RequestBody SWSentEmailPOJO request
    		
    		);
}

