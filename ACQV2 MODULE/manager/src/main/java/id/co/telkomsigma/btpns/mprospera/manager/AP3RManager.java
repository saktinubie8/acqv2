package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface AP3RManager {

    int countAll();

    void save(FundedThings product);

    void save(AP3R ap3r);
    
    void delete(FundedThings products);
    
    List<FundedThings> findByAp3rId(AP3R ap3r);

    AP3R findByLocalId(String localId);

    Page<AP3R> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate);

    List<AP3R> findAp3rListBySwId(Long swId);

    BigDecimal sumFunds(AP3R ap3r);

}