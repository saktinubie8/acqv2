package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineNotif;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;

public interface MailEngineManager {

	List<MailEngineRetail> getMailEngineRetail ();
	
	List<MailEngineRetail> findbyStatus(String status);
	
	List<MailEngineNotif> getByStatus(String status);
	
    SystemParameter findByparamName (String nmparam);

	MailEngineRetail save (MailEngineRetail mail);
	
	MailEngineNotif save (MailEngineNotif notif);
	
	MailEngineRetail findById (Long swId);
	
	MailEngineNotif getById(Long id);
}
