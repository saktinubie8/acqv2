package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.sw.DataNOS;


public interface DataNosDao extends JpaRepository <DataNOS,Long>{
	
	DataNOS findByRetrievalReferenceNumber(String rrnumber);
	
	DataNOS findByStatus(String status);

}
