package id.co.telkomsigma.btpns.mprospera.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.LocationManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;

@Service("locationService")
public class LocationService {

	@Autowired
	private LocationManager locationManager;
	
	public Location findByLocationId(String id) {
		
		 if (id == null || "".equals(id))
	          return null;
	        return locationManager.findById(id);
	}
}
