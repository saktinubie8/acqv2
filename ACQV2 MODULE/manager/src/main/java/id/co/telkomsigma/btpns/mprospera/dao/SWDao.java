package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SWDao extends JpaRepository<SurveyWawancara, String> {

    @Query("SELECT COUNT(m) FROM SurveyWawancara m WHERE m.isDeleted = false AND m.localId is not null")
    int countAll();

    @Query("SELECT m FROM SurveyWawancara m WHERE m.isDeleted = false AND m.localId is not null ORDER BY m.createdBy ASC")
    Page<SurveyWawancara> findAll(Pageable pageable);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted = false AND areaId in :areaList AND m.localId is not null ORDER BY m.createdBy ASC")
    Page<SurveyWawancara> findByCreatedDate(@Param("areaList") List<String> kelIdList,
                                            @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

    @Query("SELECT COUNT(m) FROM SurveyWawancara m WHERE m.swId = :swId AND m.isDeleted = false AND m.localId is not null")
    Integer countBySwId(@Param("swId") Long swId);

    @Query("SELECT COUNT(m) FROM SurveyWawancara m WHERE m.localId = :localId AND m.isDeleted = false AND m.localId is not null")
    Integer countByLocalId(@Param("localId") String localId);

    @Query("SELECT a FROM SurveyWawancara a WHERE a.swId  = :swId AND a.isDeleted = false AND a.localId is not null ")
    SurveyWawancara findSwBySwId(@Param("swId") Long swId);

    SurveyWawancara findByLocalId(String localId);

    SurveyWawancara findBySwId(Long swId);

    SurveyWawancara findByRrnAndLocalIdIsNotNull(String rrn);

    SurveyWawancara findByRrn(String retriveNumber);

    Page<SurveyWawancara> findByAreaIdInAndLocalIdIsNotNullAndIsDeleted(List<String> kelIdList, Pageable pageRequest, Boolean deleted);

}