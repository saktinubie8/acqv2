package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.ScoringMatrixDetails;

public interface PotentialCustomerDao extends JpaRepository<ScoringMatrixDetails, Long>{

	
	@Query(value="select smd.grade "
			+ "from scoring_matrix sm "
			+ "inner join scoring_matrix_details smd ON sm.matrix_id=smd.matrix_id "
			+ "where sm.matrix_type=:tenor and smd.min_row<=:danaSolidaritas and smd.max_row>=:danaSolidaritas "
			+ "and smd.min_column<=:absensiPrs and smd.max_column>=:absensiPrs",nativeQuery=true)
	String getGrade(@Param("tenor") String tenorMatrix,@Param("danaSolidaritas") Integer danaSolidaritas,@Param("absensiPrs") Integer absensiPrs);
	
}
