package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerDao extends JpaRepository<Customer, Long> {

    Integer countByAssignedUsername(String assignedUsername);

    Customer findTopBySwId(Long swId);

    Customer findByRrn(String rrn);

    Customer findByCustomerId (SurveyWawancara customerId);
    
}
