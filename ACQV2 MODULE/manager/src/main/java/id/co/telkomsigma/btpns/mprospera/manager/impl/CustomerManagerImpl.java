package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.CustomerDao;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

@SuppressWarnings("RedundantIfStatement")
@Service("customerManager")
public class CustomerManagerImpl implements CustomerManager {

    @Autowired
    private CustomerDao customerDao;

    @Override
    @Cacheable(value = "ppiacq.customer.countCustomerByUsername", unless = "#result == null")
    public Integer countCustomerByUsername(String assignedUsername) {
        return customerDao.countByAssignedUsername(assignedUsername);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "getAllCustomerByUsernamePageable", allEntries = true),
            @CacheEvict(value = "getAllCustomerByUsername", allEntries = true),
            @CacheEvict(value = "getCustomerByRrn", allEntries = true),
            @CacheEvict(value = "getCustomerByCifId", allEntries = true),
            @CacheEvict(value = "getAllCustomerByUsernameAndCreatedDate", allEntries = true),
            @CacheEvict(value = "getAllCustomerByUsernameAndCreatedDatePagable", allEntries = true),
            @CacheEvict(value = "countCustomerByUsername", key = "#customer.assignedUsername"),
            @CacheEvict(value = "isValidCustomer", allEntries = true),
            @CacheEvict(value = "getByProsperaId", key = "#customer.prosperaId"),
            @CacheEvict(value = "getBySwId", key = "#customer.swId"),
            @CacheEvict(value = "getByPdkId", key = "#customer.pdkId"),
            @CacheEvict(value = "getByGroupId", key = "#customer.group"),
            @CacheEvict(value = "getBySentraId", allEntries = true),
            @CacheEvict(value = "getAllCustomerByLocPageable", allEntries = true),
            @CacheEvict(value = "getAllCustomerByLoc", allEntries = true),
            @CacheEvict(value = "getAllCustomerByLocAndCreatedDate", allEntries = true),
            @CacheEvict(value = "getAllCustomerByLocAndCreatedDatePagable", allEntries = true),
            @CacheEvict(value = "ppiacq.customer.countCustomerByUsername", allEntries = true),
            @CacheEvict(value = "ppiacq.customer.getBySwId", allEntries = true),
            @CacheEvict(value = "ppiacq.customer.findByCustomerId", allEntries = true),
            @CacheEvict(value = "findByCustomerId", key = "#customer.customerId")})
    public void save(Customer customer) {
        // TODO Auto-generated method stub
        customerDao.save(customer);
    }

    @Override
    @CacheEvict(allEntries = true, value = {"findByCustomerId", "getBySwId", "getByProsperaId", "isValidCustomer",
            "findByCustomerPRSbyCustomerAndPrs", "countCustomerByUsername",
            "getAllCustomerByUsernameAndCreatedDatePagable", "getAllCustomerByUsernameAndCreatedDate",
            "getAllCustomerByUsername", "getAllCustomerByUsernamePageable", "getByGroupId", "getCustomerByCifId",
            "getByPdkId", "getCustomerPRSByPrsId", "getAllCustomerByLocPageable", "getAllCustomerByLoc",
            "getAllCustomerByLocAndCreatedDate", "getAllCustomerByLocAndCreatedDatePagable", "ppiacq.customer.countCustomerByUsername",
            "getBySentraId", "ppiacq.customer.getBySwId", "ppiacq.customer.findByCustomerId"}, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "ppiacq.customer.getBySwId", unless = "#result == null")
    public Customer getBySwId(Long swId) {
        // TODO Auto-generated method stub
        return customerDao.findTopBySwId(swId);
    }

    @Override
    @Cacheable(value = "ppiacq.customer.findByCustomerId", unless = "#result == null")
    public Customer findById(long parseLong) {
        // TODO Auto-generated method stub
        return customerDao.findOne(parseLong);
    }

}
