package id.co.telkomsigma.btpns.mprospera.manager;

import java.math.BigDecimal;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomer;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerDetail;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerDropOut;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerTopUp;
import id.co.telkomsigma.btpns.mprospera.pojo.ProductEntity;
import id.co.telkomsigma.btpns.mprospera.pojo.RecPlafondEntity;

public interface PotentialCustomerManager {
	
	List<PotentialCustomer> getAllPotentialCustomer(Integer locationId);
	
	List<PotentialCustomerTopUp> getAllPotentialCustomerTopUp(Integer locationId);
	
	List<PotentialCustomerDropOut> getPotentialCustomerDropOut(Integer locationId);

	List<PotentialCustomerDetail> getPotentialCustomerDetail(String cifNumber);

	List<ProductEntity> getProduct(String cifNumber);

	int countTopUpLoan(Long customerId);
	
	List<RecPlafondEntity> getPlafond(BigDecimal maxAngsuran);
	
	String getGrade(Integer jmlAngsur,Integer transaksiBailout,Integer frekwensiAbsen);
}
