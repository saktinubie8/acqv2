package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.sw.JumlahKunjungan;

public interface JumlahKunjunganDao extends JpaRepository<JumlahKunjungan, Long>{

	
}
