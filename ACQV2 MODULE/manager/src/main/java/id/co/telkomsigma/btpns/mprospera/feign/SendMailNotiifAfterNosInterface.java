package id.co.telkomsigma.btpns.mprospera.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SentEmailNotifAfterNosPOJO;

@FeignClient("mailEngine")
public interface SendMailNotiifAfterNosInterface {
    //send notif biasa
    @HystrixCommand
    @HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds", value = "300000")
    @RequestMapping(value = "/mailengine/webservice/sendNotifReject", method = {RequestMethod.POST})
    String sendMailNotifAfterNosInterface(
            @RequestBody SentEmailNotifAfterNosPOJO request
    );

}