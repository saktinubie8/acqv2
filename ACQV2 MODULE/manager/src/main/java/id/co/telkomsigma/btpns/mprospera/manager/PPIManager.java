package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.ppi.FamilyDataPPI;
import id.co.telkomsigma.btpns.mprospera.model.ppi.PPI;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface PPIManager {

    PPI findPPIBySwId(Long swId);

    PPI findPPIByLocalId(String ppiLocalId);

    void savePPI(PPI ppi);

    FamilyDataPPI findFamilyDataByLocalId(String familyLocalId);

    void saveFamilyData(FamilyDataPPI familyDataPPI);

    int countAllPPI();

    Page<PPI> findAllByUser(List<String> userIdList);

    Page<PPI> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest);

    Page<PPI> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate);

    Page<PPI> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate,
                                               PageRequest pageRequest);

    List<PPI> findDeletedPPI();

    List<FamilyDataPPI> findFamilyDataByPpiId(Long ppiId);

}
