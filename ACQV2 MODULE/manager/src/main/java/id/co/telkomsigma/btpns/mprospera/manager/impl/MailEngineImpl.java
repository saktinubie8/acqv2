package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.MailEngineDao;
import id.co.telkomsigma.btpns.mprospera.dao.MailNotifDao;
import id.co.telkomsigma.btpns.mprospera.dao.ParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.MailEngineManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineNotif;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;

@Service("mailEngineManager")
public class MailEngineImpl implements MailEngineManager {

	@Autowired 
	MailEngineDao mailEngineDao;
	
	@Autowired
	ParamDao paramDao;
	
	@Autowired
	MailNotifDao mailNotifDao;
	
	protected final Log LOGGER = LogFactory.getLog(getClass());
	
	@Override
	public List<MailEngineRetail> getMailEngineRetail() {
		// TODO Auto-generated method stub
		return mailEngineDao.findAll();
	}


	@Override
	public List<MailEngineRetail> findbyStatus(String status) {
		// TODO Auto-generated method stub
		return mailEngineDao.getMailEngineRetail(status);
	}
	
	@Override
	public List<MailEngineNotif> getByStatus(String status) {
		// TODO Auto-generated method stub
		return mailNotifDao.getMailEngineNotif(status);
	}


//	@Override
//	public List<MailEngineRetail> findbySwId(Long swId) {
//		// TODO Auto-generated method stub
//		return mailEngineDao.findbySwId(swId);
//	}

	/*
	 * @author : Ilham
	 * @Since  : 20191126
	 * @category patch 
	 *    - Penambahan Timestamp untuk email  
	 */ 
	@Override
	public MailEngineRetail save(MailEngineRetail mail) {
		// TODO Auto-generated method stub
		if(mail.getId()!=null) {
			try {
				mail.setUpdateby("System");
				mail.setUpdatedt(new Date());
				mail.setStatus("sent");
				mailEngineDao.save(mail);
			} catch (Exception e) {
	            LOGGER.error("save mailRetail error: " + e.getMessage());
			}
		}
		return mail;
	}

	/*
	 * @author : Ilham
	 * @Since  : 20191126
	 * @category patch 
	 *    - Penambahan Timestamp untuk email  
	 */ 
	@Override
	public MailEngineNotif save(MailEngineNotif notif) {
		// TODO Auto-generated method stub
		try {
			if(notif.getId()!= null) {
				notif.setUpdateby("System");
				notif.setUpdatedt(new Date());
				notif.setStatus("sent");
				mailNotifDao.save(notif);
			}	
		} catch (Exception e) {
            LOGGER.error("save notifRetail error: " + e.getMessage());
		}
		
		return notif;
	}

	@Override
	public MailEngineRetail findById(Long id) {
		// TODO Auto-generated method stub
		return mailEngineDao.findById(id);
	}


	@Override
	public SystemParameter findByparamName(String param) {
		// TODO Auto-generated method stub
		return paramDao.findByParamName(param);
	}


	@Override
	public MailEngineNotif getById(Long id) {
		// TODO Auto-generated method stub
		return mailNotifDao.findById(id);
	}

}
