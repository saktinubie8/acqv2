package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.DataNosDao;
import id.co.telkomsigma.btpns.mprospera.dao.MailEngineDao;
import id.co.telkomsigma.btpns.mprospera.dao.ParamDao;
import id.co.telkomsigma.btpns.mprospera.dao.SWDao;
import id.co.telkomsigma.btpns.mprospera.dao.SwForEmailDao;
import id.co.telkomsigma.btpns.mprospera.manager.DataNosManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.sw.DataNOS;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwForEmail;

@Service("dataNosManager")
public class DataNosManagerImpl implements DataNosManager {

	@Autowired
	ParamDao paramDao;
	
	@Autowired
	SwForEmailDao swForEmailDao;
		
	@Autowired
	DataNosDao dataNOSDao;
	
	@Autowired
	SWDao surveyWawancaraDao;
	
	@Autowired 
	MailEngineDao mailEngineDao;
	
	@Override
	public SystemParameter findByparamName(String param) {
		// TODO Auto-generated method stub
		return paramDao.findByParamName(param);
	}

	@Override
	public SwForEmail findByRetrievalReferenceNumber(String retrievalReferenceNumber) {
		// TODO Auto-generated method stub
		return swForEmailDao.findByRetrievalReferenceNumber(retrievalReferenceNumber);
	}

	@Override
	public SwForEmail findByFlagStatusScoring(String flagStatusScoring ,String retrievalReferenceNumber) {
		// TODO Auto-generated method stub
		return swForEmailDao.getdataForUpdate(flagStatusScoring ,retrievalReferenceNumber);
	}

	@Override
	public SwForEmail update(SwForEmail swForEmail) {
		// TODO Auto-generated method stub
		if(swForEmail.getSwId()!= null) {
			try {
				swForEmail.setFlagStatusScoring("Scoring Done");
				swForEmailDao.save(swForEmail);
		} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return swForEmail;
	}


	@Override
	public DataNOS save(DataNOS dataNOS) {
		// TODO Auto-generated method stub
		return dataNOSDao.save(dataNOS);
	}

	@Override
	public List<DataNOS> findAllMessageNOS() {
		// TODO Auto-generated method stub
		return dataNOSDao.findAll();
	}

	@Override
	public SurveyWawancara findByRrn(String rrn) {
		// TODO Auto-generated method stub
		return surveyWawancaraDao.findByRrn(rrn);
	}

	@Override
	public DataNOS update(DataNOS nOS) {
		// TODO Auto-generated method stub
		try {
			if(nOS.getRetrievalReferenceNumber()!=null) {
			nOS.setStatus("Sent");
			dataNOSDao.save(nOS);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return nOS;
	}

	@Override
	public DataNOS findByRRNumber(String rrnumber) {
		// TODO Auto-generated method stub
		return dataNOSDao.findByRetrievalReferenceNumber(rrnumber);
	}

	@Override
	public DataNOS findByStatus(String status) {
		// TODO Auto-generated method stub
		return dataNOSDao.findByStatus(status);
	}

	/*
	 * @author : Ilham
	 * @Since  : 20191126
	 * @category patch 
	 *    - Penambahan Timestamp untuk email  
	 */ 
	@Override
	public MailEngineRetail save(MailEngineRetail mail) {
		// TODO Auto-generated method stub
		if(mail.getId()!=null) {
			try {
				mail.setUpdateby("System");
				mail.setUpdatedt(new Date());
				mail.setMaxRetry(mail.getMaxRetry()+1);
				mailEngineDao.save(mail);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return mail;
	}
}
