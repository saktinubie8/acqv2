package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;

public interface MailEngineDao extends JpaRepository<MailEngineRetail, Long> {
	
//	List<MailEngineRetail> findByStatus(String status);

	@Query("SELECT p FROM MailEngineRetail p WHERE(LOWER(p.status) =:keyword)")
	List<MailEngineRetail> getMailEngineRetail(@Param("keyword") String keyword);
	
	MailEngineRetail findById (Long swId);
	
//	List<MailEngineRetail> findbySwId(Long swId);	
}
