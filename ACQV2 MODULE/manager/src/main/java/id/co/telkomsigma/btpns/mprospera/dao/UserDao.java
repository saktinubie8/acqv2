package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.user.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserDao extends JpaRepository<User, String> {

    /**
     * Get single user by username
     *
     * @param username
     * @return single User object
     */
    User findByUsername(String username);

    User findByUserId(Long userId);

    List<User> findByOfficeCode(String locId);
    
	@Query("SELECT u FROM User u WHERE u.officeCode = :locationId AND u.roleUser = :role")
	List<User> getEmail(@Param("locationId") String locationId, @Param("role") String role);

    @Query("SELECT u.sessionKey FROM User u WHERE username = :username")
    String findSessionKeyByUsername(@Param("username") String username);

    @Query("SELECT u FROM User u where u.officeCode = :locationIdd AND u.roleUser = :rolee and u.email is not null OR u.username in (:userLogin)")
    List<User> findUsernameCoandBm(@Param("locationIdd") String locationId, @Param("rolee") String role, @Param("userLogin") String UserLogin);

}