package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationOfficerMapping;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("loanService")
public class LoanService extends GenericService {

    @Autowired
    private LoanManager loanManager;

    public void save(Loan loan) {
        loanManager.save(loan);
    }

    public void save(DeviationOfficerMapping deviationOfficerMapping) {
        loanManager.save(deviationOfficerMapping);
    }

    public Loan getLoanByRrn(String rrn) {
        return loanManager.getLoanByRrn(rrn);
    }

    public void save(LoanDeviation loanDeviation) {
        loanManager.save(loanDeviation);
    }

    public Loan findById(String loanId) {
        if (loanId == null || "".equals(loanId))
            return null;
        return loanManager.findById(Long.parseLong(loanId));
    }

    public Loan findByAp3rAndCustomer(Long ap3rId, Customer customer) {
        return loanManager.findByAp3rAndCustomer(ap3rId, customer);
    }

    public Loan getLoanByAp3rId(Long ap3rId) {
        return loanManager.getLoanByAp3rId(ap3rId);
    }

    public LoanDeviation findLoanDeviationByAp3rId(Long ap3rId, Boolean isDeleted) {
        return loanManager.findByAp3rId(ap3rId, isDeleted);
    }

    public int countDeviationWithStatus(Long id) {
        return loanManager.countDeviationWithStatus(id);
    }

    public int countDeviationWaiting(Long id) {
        return loanManager.countDeviationWaiting(id);
    }

    public DeviationOfficerMapping findTopByDeviationIdAndStatus(Long deviationId) {
        return loanManager.findTopByDeviationIdAndStatus(deviationId);
    }

    public List<DeviationOfficerMapping> findMappingByLoanDeviationId(String deviationId) {
        return loanManager.findByLoanDeviationId(deviationId);
    }

    public int countLoanBySWId(Long swId) {
        return loanManager.countLoanBySWId(swId);
    }

}
