package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.FamilyDataPPIDao;
import id.co.telkomsigma.btpns.mprospera.dao.PPIDao;
import id.co.telkomsigma.btpns.mprospera.manager.PPIManager;
import id.co.telkomsigma.btpns.mprospera.model.ppi.FamilyDataPPI;
import id.co.telkomsigma.btpns.mprospera.model.ppi.PPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("pPIManager")
public class PPIManagerImpl implements PPIManager {

    @Autowired
    PPIDao ppiDao;

    @Autowired
    FamilyDataPPIDao familyDataPPIDao;

    @Override
    public PPI findPPIBySwId(Long swId) {
        return ppiDao.findBySwIdAndDeletedFalse(swId);
    }

    @Override
    public PPI findPPIByLocalId(String ppiLocalId) {
        return ppiDao.findByPpiLocalIdAndDeletedFalse(ppiLocalId);
    }

    @Override
    public void savePPI(PPI ppi) {
        ppiDao.save(ppi);
    }

    @Override
    public FamilyDataPPI findFamilyDataByLocalId(String familyLocalId) {
        return familyDataPPIDao.findByFamilyDataPpiLocalId(familyLocalId);
    }

    @Override
    public void saveFamilyData(FamilyDataPPI familyDataPPI) {
        familyDataPPIDao.save(familyDataPPI);
    }

    @Override
    public int countAllPPI() {
        return ppiDao.countAll();
    }

    @Override
    public Page<PPI> findAllByUser(List<String> userIdList) {
        return ppiDao.findByCreatedByInAndPpiLocalIdIsNotNullAndDeletedFalse(userIdList, new PageRequest(0, countAllPPI()));
    }

    @Override
    public Page<PPI> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest) {
        return ppiDao.findByCreatedByInAndPpiLocalIdIsNotNullAndDeletedFalse(userIdList, pageRequest);
    }

    @Override
    public Page<PPI> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ppiDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<PPI> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ppiDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    public List<PPI> findDeletedPPI() {
        return ppiDao.findByDeletedTrue();
    }

    @Override
    public List<FamilyDataPPI> findFamilyDataByPpiId(Long ppiId) {
        return familyDataPPIDao.findByPpiIdAndDeletedFalse(ppiId);
    }
}
