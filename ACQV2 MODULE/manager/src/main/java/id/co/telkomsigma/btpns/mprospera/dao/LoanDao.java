package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;

public interface LoanDao extends JpaRepository<Loan, Long> {

    @Query("SELECT COUNT(a) FROM Loan a WHERE a.swId = :swId")
    Integer countBySwId(@Param("swId") Long swId);

    @Query("SELECT s FROM Loan s INNER JOIN s.customer c " + "INNER JOIN s.customer.group g "
            + "INNER JOIN s.customer.group.sentra st "
            + ", Location l WHERE st.locationId = l.locationId AND s.createdDate>=:startDate AND s.createdDate<:endDate AND l.locationId = :officeId ORDER BY s.createdDate")
    Page<Loan> findByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                 Pageable pageRequest, @Param("officeId") String officeId);

    Loan findByRrn(String rrn);

    Loan findByAp3rIdAndIsDeletedFalse(Long ap3rId);

    Loan findByAp3rIdAndCustomerAndAp3rIdIsNotNullAndIsDeletedFalseAndStatusIn
            (Long ap3rId, Customer customer, List<String> statusList);

}
