package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.sw.DataNOS;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwForEmail;

public interface DataNosManager {
	
    SystemParameter findByparamName (String nmparam);
    
    SwForEmail findByRetrievalReferenceNumber (String retrievalReferenceNumber);
    
    SwForEmail findByFlagStatusScoring (String flagStatusScoring ,String retrievalReferenceNumber);
    
    List <DataNOS> findAllMessageNOS();
    
    SwForEmail update (SwForEmail swForEmail);
    
    DataNOS save (DataNOS dataNOS);
    
    DataNOS update (DataNOS nOS);
    
    DataNOS findByRRNumber (String rrnumber);
    
    SurveyWawancara findByRrn (String rrn);
    
    DataNOS findByStatus (String status);
    
    MailEngineRetail save (MailEngineRetail mail);
    
}
