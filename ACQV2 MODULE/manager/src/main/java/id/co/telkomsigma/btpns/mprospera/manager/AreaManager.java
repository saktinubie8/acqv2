package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sda.*;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;

public interface AreaManager {

    AreaKelurahan findById(String id);

    Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByParentAreaId(String areaId);

    Page<AreaSubDistrictDetails> findByCreatedDate(String areaId, Date parse, Date parse2);

    Page<AreaSubDistrictDetails> findByCreatedDate(List<String> userList, Date parse, Date parse2);

    void clearCache();

}
