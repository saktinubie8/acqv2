package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.AP3RManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service("ap3rService")
public class AP3RService extends GenericService {

    @Autowired
    private AP3RManager ap3rManager;

    public void save(AP3R ap3r) {
        ap3rManager.save(ap3r);
    }

    public void delete(FundedThings thingsList) {
        ap3rManager.delete(thingsList);
    }

    public void save(FundedThings product) {
        ap3rManager.save(product);
    }
    
    public List<FundedThings> findByAp3rId(AP3R ap3r) {
        return ap3rManager.findByAp3rId(ap3r);
    }
    
    public AP3R findByLocalId(String localId) {
        return ap3rManager.findByLocalId(localId);
    }

    public List<AP3R> findAp3rListBySwId(Long swId) {
        return ap3rManager.findAp3rListBySwId(swId);
    }

    public BigDecimal sumFunds(AP3R ap3r) {
        return ap3rManager.sumFunds(ap3r);
    }

}