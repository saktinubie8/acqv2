package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessTypeAP3R;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by Dzulfiqar on 27/04/2017.
 */
public interface BusinessTypeAP3RDao extends JpaRepository<BusinessTypeAP3R, Long> {

    @Query("SELECT COUNT(b) FROM BusinessTypeAP3R b")
    int countAll();

    @Query("SELECT b FROM BusinessTypeAP3R b")
    Page<BusinessTypeAP3R> findAll(Pageable pageable);

    BusinessTypeAP3R findByBusinessId(Long businessId);

}
