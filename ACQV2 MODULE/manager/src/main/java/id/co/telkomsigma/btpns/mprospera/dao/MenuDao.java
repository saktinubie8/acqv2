package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MenuDao extends JpaRepository<Menu, String> {
    /**
     * Get certain role menuID
     *
     * @return String of Menu ID
     */
    @Query("SELECT r.menus FROM Role r WHERE r.id = :authority")
    List<Menu> getListMenu(@Param("authority") Long roleId);

    Menu findByMenuId(Long menuId);

}
