package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwForEmail;

public interface SwForMailDao extends JpaRepository<SwForEmail,Long> {

	List<SwForEmail> findSwIdBySwId (SurveyWawancara sw);
	
}
