package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineNotif;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.product.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.product.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;

public interface SWManager {

    int countAll();

    int countAllProduct();

    Page<SurveyWawancara> findAll(List<String> kelIdList);

    Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);

    Boolean isValidSw(String swId);

    Boolean isValidSwBySwId(Long swId);

    void save(SurveyWawancara surveyWawancara);

    void save(SwIdPhoto swIdPhoto);

    void save(SwSurveyPhoto swSurveyPhoto);

    SurveyWawancara getSWById(String swId);

    SurveyWawancara findByRrn(String rrn);

    SurveyWawancara findByLocalId(String localId);

    SurveyWawancara findByswId(Long swId);

    SwSurveyPhoto findByswIdPhoto(Long swId);

    LoanProduct findByProductId(String productId);

    List<SWProductMapping> findProductMapBySwId(Long swId);

    List<DirectBuyThings> findProductBySwId(SurveyWawancara sw);

    List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw);

    SWProductMapping findProductMapByLocalId(String localId);

    List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw);

    List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId);

    void save(DirectBuyThings directBuyProduct);

    void save(AWGMBuyThings awgmBuyProduct);

    void save(NeighborRecommendation neighbor);

    void save(OtherItemCalculation otherItemCalculation);

    void save(SWProductMapping map);

    void save(SWUserBWMPMapping map);

    void save(SwForEmail swForEmail);

    void save(MailEngineRetail mailEngineRetail);

    void save(MailEngineNotif mailEngineNotif);

    void clearCache();

    void deleteDirectBuy(DirectBuyThings id);

    void deleteAwgmBuy(AWGMBuyThings id);

    void deleteReffNeighbour(NeighborRecommendation id);

    void deleteOtherItemCalc(OtherItemCalculation id);

    SystemParameter findByparamName(String param);

    SWProductMapping findByTopBySwid(Long swId);

    SWUserBWMPMapping findBySwAndUser(Long sw, Long userId);

    SWUserBWMPMapping findBySwAndLevel(Long sw, String level);

    Boolean hasIdPhoto(String swId);

    Boolean hasSurveyPhoto(String swId);


}