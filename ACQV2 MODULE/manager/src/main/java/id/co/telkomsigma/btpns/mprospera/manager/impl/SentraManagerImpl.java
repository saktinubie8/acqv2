package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.GroupDao;
import id.co.telkomsigma.btpns.mprospera.dao.SentraDao;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

@SuppressWarnings({"unchecked", "RedundantIfStatement"})
@Service("sentraManager")
public class SentraManagerImpl implements SentraManager {

    @Autowired
    private SentraDao sentraDao;

    @Autowired
    private GroupDao groupDao;

    @Override
    @Caching(evict = {@CacheEvict(value = "findBySentraId", key = "#sentra.sentraId", beforeInvocation = false),
            @CacheEvict(value = "findByProsperaId", key = "#sentra.prosperaId", beforeInvocation = false),
            @CacheEvict(value = "getAllSentra", allEntries = true),
            @CacheEvict(value = "getAllSentraPageable", allEntries = true),
            @CacheEvict(value = "findSentraByCreatedDate", allEntries = true),
            @CacheEvict(value = "findSentraByCreatedDatePageable", allEntries = true),
            @CacheEvict(value = "countAllSentraNotDeleted", allEntries = true),
            @CacheEvict(value = "ppiacq.sentra.findSentraByCreatedDate", allEntries = true),
            @CacheEvict(value = "ppiacq.sentra.getSentraByRrn", allEntries = true),
            @CacheEvict(value = "isValidSentra", key = "#sentra.sentraId", beforeInvocation = false),
            @CacheEvict(value = "getSentraPhoto", key = "#sentra.sentraId", beforeInvocation = false),
            @CacheEvict(value = "getGroupBySentraId", key = "#sentra.sentraId", beforeInvocation = false),
            @CacheEvict(value = "getGroupByGroupId", allEntries = true)})
    public void save(Sentra sentra) {
        sentraDao.save(sentra);
    }

    @Override
    @Cacheable(value = "ppiacq.sentra.findSentraByCreatedDate", unless = "#result == null")
    public Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        if (username.equals(null)) {
            return sentraDao.findByCreatedDateWithLoc(loc, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
        } else {
            return sentraDao.findByCreatedDate(username, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
        }
    }

    @Override
    @CacheEvict(value = {"getGroupByProsperaId", "isValidSentra", "getSentraPhoto", "findBySentraId", "isValidGroup",
            "countAllSentraNotDeleted", "getAllSentra", "getAllSentraPageable", "findSentraByCreatedDate",
            "findSentraByCreatedDatePageable", "getGroupBySentraId", "getGroupByGroupId", "getSentraByWismaId",
            "getSentraByRrn", "getGroupByRrn", "getGroupStagingByGroupId",
            "findByProsperaId","ppiacq.sentra.findSentraByCreatedDate", "ppiacq.sentra.getSentraByRrn"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "ppiacq.sentra.getSentraByRrn", unless = "#result == null")
    public Sentra findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return sentraDao.findByRrn(rrn);
    }

    @Override
    public void save(List sentra) {
        // TODO Auto-generated method stub
        sentraDao.save(sentra);
    }

    @Override
    public List<Sentra> findAll() {
        // TODO Auto-generated method stub
        return sentraDao.findAll();
    }

    @Override
    public String findSentraNameBySentraId(Long sentraId) {
        return sentraDao.findSentraNameBySentraId(sentraId);
    }

	@Override
	public Group findById(Long idGroup) {
		// TODO Auto-generated method stub
		return groupDao.findOne(idGroup);
	}

}
