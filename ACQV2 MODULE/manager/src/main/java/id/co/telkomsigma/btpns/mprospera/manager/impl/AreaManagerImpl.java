package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.model.sda.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("areaManager")
public class AreaManagerImpl implements AreaManager {

    @Autowired
    AreaSubDistrictDao areaSubDistrictDao;

    @Autowired
    AreaSubDistrictDetailsDao areaSubDistrictDetailsDao;

    @Autowired
    AreaKelurahanDao areaKelurahanDao;

     protected final Log log = LogFactory.getLog(getClass());

    @Override
    @Cacheable(value = "wolverine.areaKelurahan.findById", unless = "#result == null")
    public AreaKelurahan findById(String id) {
        return areaKelurahanDao.findOne(id);
    }

    @Override
    @Cacheable(value = "getAllSubDistrictDetailsByParentAreaId", unless = "#result == null")
    public Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByParentAreaId(String districtAreaId) {
        List<AreaSubDistrict> subDistrictList = areaSubDistrictDao.findByParentAreaId(districtAreaId);
        if (subDistrictList == null || subDistrictList.size() == 0)
            return null;
        return areaSubDistrictDetailsDao.searchAllDetailsByParentId(subDistrictList,
                new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "findByCreatedDate", unless = "#result == null")
    public Page<AreaSubDistrictDetails> findByCreatedDate(String areaId, Date parse, Date parse2) {
        List<AreaSubDistrict> subDistrictList = areaSubDistrictDao.findByParentAreaId(areaId);
        if (subDistrictList == null || subDistrictList.size() == 0)
            return null;
        return areaSubDistrictDetailsDao.searchDetailsByParentIdAndDate(subDistrictList, parse, parse2,
                new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "findAllByCreatedDate", unless = "#result == null")
    public Page<AreaSubDistrictDetails> findByCreatedDate(List<String> userList, Date parse, Date parse2) {
        return areaSubDistrictDetailsDao.searchAllDetailByCreatedByAndDate(userList, parse, parse2, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @CacheEvict(allEntries = true, cacheNames = {"getWismaById", "getKelurahanByParentId", "getSubDistrictByParentId",
            "getKelurahanByParentId", "wolverine.areaKelurahan.findById", "allDistrict", "findById", "findDistrictById", "getDistrictById", "getProvinceById"
            , "getAllAreaProvince", "getAllAreaDistrictByParentId", "getAreaProvinceNearby", "getAllSubDistrictDetailsByParentAreaId"
            , "getAllSubDistrictDetailsByParentAreaIdPaging", "findByCreatedDate", "findByCreatedDatePageable", "getAllKelurahanDetailsByParentAreaId"
            , "getAllKelurahanDetailsByParentAreaIdPaging", "getAllKelurahanDetailsCreatedDate", "getAllKelurahanDetailsCreatedDatePageable", "getAllSubDistrictDetailsByCreatedByPaging"})
    public void clearCache() {
        // TODO Auto-generated method stub

    }

}
