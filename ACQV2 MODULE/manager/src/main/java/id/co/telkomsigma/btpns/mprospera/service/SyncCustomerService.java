package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("syncCustomerService")
public class SyncCustomerService extends GenericService {

    @Autowired
    private CustomerManager customerManager;

    public Integer countAll(String username) {
        // TODO Auto-generated method stub
        return customerManager.countCustomerByUsername(username);
    }

}
