package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.product.SWProductMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.telkomsigma.btpns.mprospera.model.product.LoanProduct;

public interface LoanProductDao extends JpaRepository<LoanProduct, Long> {

    LoanProduct findByProductId(Long productId);

    @Query("SELECT COUNT(p) FROM LoanProduct p ORDER BY p.productName ")
    int countAll();

    @Query("SELECT p FROM LoanProduct p ORDER BY p.productName")
    Page<LoanProduct> findAll(Pageable pageable);

    List<LoanProduct> findByproductId(SWProductMapping productId);
}
