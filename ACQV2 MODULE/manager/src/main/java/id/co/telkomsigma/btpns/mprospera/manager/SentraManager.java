package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

public interface SentraManager {

    void save(Sentra sentra);

    void save(List sentra);

    Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate);

    void clearCache();

    Sentra findByRrn(String rrn);

    List<Sentra> findAll();

    String findSentraNameBySentraId(Long sentraId);
    
    Group findById(Long idGroup);

}
