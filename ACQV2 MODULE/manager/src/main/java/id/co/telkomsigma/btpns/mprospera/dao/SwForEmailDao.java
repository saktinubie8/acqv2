package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.sw.SwForEmail;

public interface SwForEmailDao extends JpaRepository <SwForEmail,Long>{
	
	SwForEmail findByRetrievalReferenceNumber (String retrievalReferenceNumber);
	
	SwForEmail findByflagStatusScoring (String flagStatusScoring);
	
	@Query("SELECT u FROM SwForEmail u WHERE u.flagStatusScoring = :flagStatusScoring AND u.retrievalReferenceNumber = :retrievalReferenceNumber")
	SwForEmail getdataForUpdate(@Param("flagStatusScoring") String flagStatusScoring, @Param("retrievalReferenceNumber") String retrievalReferenceNumber);
}
