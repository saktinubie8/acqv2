package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.MailEngineManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineNotif;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SentEmailNotifAfterNosPOJO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("mailEngineService")
public class MailEngineService extends GenericService {

    @Autowired
    private MailEngineManager mailEngineManager;

    @Autowired
    private SentEmailService sentEmailService;

    public List<MailEngineRetail> findbyStatus(String status) {
        return mailEngineManager.findbyStatus(status);
    }

    public List<MailEngineNotif> getbyStatus(String status) {
        return mailEngineManager.getByStatus(status);
    }

    public SystemParameter findByParamName(String paramName) {
        return mailEngineManager.findByparamName(paramName);
    }

    public void save(MailEngineRetail mail) {
        mailEngineManager.save(mail);
    }

    public void save(MailEngineNotif notif) {
        mailEngineManager.save(notif);
    }

    public MailEngineRetail findById(Long swId) {
        return mailEngineManager.findById(swId);
    }

    public MailEngineNotif getById(Long Id) {
        return mailEngineManager.getById(Id);
    }

    public String generateNotSend(SWSentEmailPOJO sWSentEmailPOJO) {
        return sentEmailService.wsMailEngineValidation(sWSentEmailPOJO);
    }

    public String generatNotSendNotif(SentEmailNotifAfterNosPOJO mailEngineNotif) {
        return sentEmailService.wsMailSendNotifAfterNos(mailEngineNotif);
    }

}