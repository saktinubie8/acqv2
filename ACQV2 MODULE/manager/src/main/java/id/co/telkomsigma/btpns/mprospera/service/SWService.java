package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineNotif;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.product.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.product.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;

@Service("swService")
public class SWService extends GenericService {

    @Autowired
    @Qualifier("swManager")
    private SWManager swManager;

    @Autowired
    private UserManager userManager;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public Integer countAll() {
        return swManager.countAll();
    }

    public SWUserBWMPMapping findBySwAndUser(Long sw, Long userId) {
        return swManager.findBySwAndUser(sw, userId);
    }

    public SWUserBWMPMapping findBySwAndLevel(Long sw, String level) {
        return swManager.findBySwAndLevel(sw, level);
    }

    public Boolean isValidSw(String swId) {
        return swManager.isValidSw(swId);
    }

    public Boolean isValidSwBySwId(Long swId) {
        return swManager.isValidSwBySwId(swId);
    }

    public void save(SurveyWawancara surveyWawancara) {
        swManager.save(surveyWawancara);
    }

    public void save(SWUserBWMPMapping swUserBWMPMapping) {
        swManager.save(swUserBWMPMapping);
    }

    public void save(SwIdPhoto swIdPhoto) {
        swManager.save(swIdPhoto);
    }

    public void save(SwSurveyPhoto swSurveyPhoto) {
        swManager.save(swSurveyPhoto);
    }

    public void save(DirectBuyThings directBuyProduct) {
        swManager.save(directBuyProduct);
    }

    public void save(AWGMBuyThings awgmBuyProduct) {
        swManager.save(awgmBuyProduct);
    }

    public void save(NeighborRecommendation neighbor) {
        swManager.save(neighbor);
    }

    public void save(OtherItemCalculation otherItemCalculation) {
        swManager.save(otherItemCalculation);
    }

    public void save(SWProductMapping map) {
        swManager.save(map);
    }

    public void save(SwForEmail swForEmail) {
        swManager.save(swForEmail);
    }

    public void save(MailEngineRetail mailEngineRetail) {
        swManager.save(mailEngineRetail);
    }

    public void save(MailEngineNotif mailEngineNotif) {
        swManager.save(mailEngineNotif);
    }

    public SurveyWawancara getSWById(String swId) {
        return swManager.getSWById(swId);
    }

    public LoanProduct findByProductId(String productId) {
        return swManager.findByProductId(productId);
    }

    public List<SWProductMapping> findProductMapBySwId(Long swId) {
        return swManager.findProductMapBySwId(swId);
    }

    public List<DirectBuyThings> findProductBySwId(SurveyWawancara sw) {
        return swManager.findProductBySwId(sw);
    }

    public List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw) {
        return swManager.findAwgmProductBySwId(sw);
    }

    public List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw) {
        return swManager.findNeighborBySwId(sw);
    }

    public List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId) {
        return swManager.findOtherItemCalcBySwId(swId);
    }

    public SurveyWawancara findSwByLocalId(String localId) {
        return swManager.findByLocalId(localId);
    }

    public SurveyWawancara findSwByswId(Long swId) {
        return swManager.findByswId(swId);
    }

    public SwSurveyPhoto findSwByswIdPhoto(Long swId) {
        return swManager.findByswIdPhoto(swId);
    }

    public SWProductMapping findProductMapByLocalId(String localId) {
        return swManager.findProductMapByLocalId(localId);
    }

    public void deleteDirectBuy(DirectBuyThings id) {
        swManager.deleteDirectBuy(id);
    }

    public void deleteAwgmBuy(AWGMBuyThings id) {
        swManager.deleteAwgmBuy(id);
    }

    public void deleteNeigbourReff(NeighborRecommendation id) {
        swManager.deleteReffNeighbour(id);
    }

    public void deleteOtherItemCalc(OtherItemCalculation id) {
        swManager.deleteOtherItemCalc(id);
    }

    public SystemParameter findByparamName(String paramName) {
        return swManager.findByparamName(paramName);
    }

    public SWProductMapping findByTopBySwid(Long swId) {
        return swManager.findByTopBySwid(swId);
    }

    public Boolean hasIdPhoto(String swId) {
        return swManager.hasIdPhoto(swId);
    }

    public Boolean hasSurveyPhoto(String swId) {
        return swManager.hasSurveyPhoto(swId);
    }

}