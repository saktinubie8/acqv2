package id.co.telkomsigma.btpns.mprospera.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;

public class UtilCollection {
	private static UtilCollection instance=null;
	
	public static UtilCollection getInstance() {
		if(null==instance)
			instance=new UtilCollection();
		
		return instance;	
	}
	
	public String getIpAddress() {
		String ip = "";
		String localIp = "";
		String oldIp = "";
		try {
			localIp = InetAddress.getLocalHost().getHostAddress();

			String interfaceName = "eth0,eth1,eth2,eth3,eth4,eth5,ens192,lo";
			NetworkInterface networkInterface = NetworkInterface.getByName(interfaceName);
//			System.out.println("NetworkInterface: " + NetworkInterface.getNetworkInterfaces().nextElement());
			while (NetworkInterface.getNetworkInterfaces().hasMoreElements()) {
				oldIp=ip;
				
				String eth = NetworkInterface.getNetworkInterfaces().nextElement().getName();
//				System.out.println("NetworkInterface: " + NetworkInterface.getNetworkInterfaces().nextElement().getName());
				if (interfaceName.contains(eth)) {
					networkInterface = NetworkInterface.getByName(eth);
					ip = networkInterface.getInetAddresses().nextElement().getHostAddress();

					Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
					InetAddress currentAddress;
					if (inetAddress.hasMoreElements()) {
						currentAddress = inetAddress.nextElement();
						while (inetAddress.hasMoreElements()) {
							currentAddress = inetAddress.nextElement();
							if (currentAddress instanceof Inet4Address && !currentAddress.isLoopbackAddress()) {
								ip = currentAddress.toString();
//								System.out.println("interface: "+eth+" ip: "+ip);
								break;
							}
						}
					}
					
					if(oldIp.equals(ip)) 
						break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			ip = localIp;
		}

		return ip.replace("/", "");
	}

	private static AuditLog generateAuditLog(String description) {
		String reffNo = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(AuditLog.GENERATE_REPORT);
		auditLog.setCreatedBy("Mprospera");
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription(description);
		auditLog.setReffNo(reffNo);
		return auditLog;
	}

}
