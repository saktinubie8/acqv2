package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.product.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.product.ProductPlafond;

public interface ProductPlafondDao extends JpaRepository<ProductPlafond, Long> {

    List <ProductPlafond> findByProductIdOrderByPlafond(LoanProduct productId);

}
