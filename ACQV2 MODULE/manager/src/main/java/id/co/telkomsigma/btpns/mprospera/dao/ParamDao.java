package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ParamDao extends JpaRepository<SystemParameter, String> {

    SystemParameter findByParameterId(Long paramId);

    Page<SystemParameter> findByParamNameLikeOrParamDescriptionLikeAllIgnoreCaseOrderByParamNameAsc(String paramName,
                                                                                                    String paramDescription, Pageable pageable);

    @Query("SELECT DISTINCT p FROM SystemParameter p WHERE (LOWER(p.paramName) LIKE :keyword OR LOWER(p.paramDescription) LIKE :keyword) AND p.paramGroup = :paramGroup ORDER BY p.paramName ASC, p.paramDescription ASC")
    Page<SystemParameter> getParams(@Param("keyword") String keyword, @Param("paramGroup") String paramGroup,
                                    Pageable pageable);

    SystemParameter findByParamName(String paramName);

}
