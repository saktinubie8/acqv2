package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineNotif;

public interface MailNotifDao extends JpaRepository<MailEngineNotif, Long>{
	
	@Query("SELECT p FROM MailEngineNotif p WHERE(LOWER(p.status) LIKE :keyword)")
	List<MailEngineNotif> getMailEngineNotif(@Param("keyword") String keyword);
	
	MailEngineNotif findById(Long id);

}
