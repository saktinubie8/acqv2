package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;

public interface PMManager {

    void save(ProjectionMeeting pm);

    void clearCache();

    ProjectionMeeting findByRrn(String rrn);

    ProjectionMeeting findById(String pmId);

}
