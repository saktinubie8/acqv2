package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.UserDao;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userManager")
public class UserManagerImpl implements UserManager {
    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    //@Cacheable(value = "ppiacq.user.userByUsername", unless = "#result == null")
    public User getUserByUsername(String username) {
        return userDao.findByUsername(username.toLowerCase());
    }

    @Override
    public User getUserByUserId(Long userId) {
        User user = userDao.findByUserId(userId);
        return user;
    }

    @Override
    @CacheEvict(value = {"userByUserId", "allLockedUser", "failedLoginAttempt",
            "loadUserByUsernameFromUpload", "allUser", "userByLocId", "ppiacq.user.userByUsername", "ppiacq.user.userByLocId"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {

    }

    @Override
    //@Cacheable(value = "ppiacq.user.userByLocId", unless = "#result == null")
    public List<User> getUserByLocId(String locId) {
        // TODO Auto-generated method stub
        List<User> userWisma = userDao.findByOfficeCode(locId);
        return userWisma;
    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder getPasswordEncoder() {
        if (passwordEncoder == null) {
            passwordEncoder = new BCryptPasswordEncoder();
        }
        return passwordEncoder;
    }

    @Override
    //@Cacheable(value = "wln.user.sessionKeyByUsername", unless = "#result == null")
    public String getSessionKeyByUsername(String username) {
        return userDao.findSessionKeyByUsername(username.toLowerCase());
    }


    @Override
	public List<User> findEmail(String locationId, String role) {
		return userDao.getEmail(locationId, role);
	}

	@Override
	public List<User> findUsernameCoandBm(String locatiionId, String role, String createBy) {
		// TODO Auto-generated method stub
		return userDao.findUsernameCoandBm(locatiionId, role, createBy);
	}
}
