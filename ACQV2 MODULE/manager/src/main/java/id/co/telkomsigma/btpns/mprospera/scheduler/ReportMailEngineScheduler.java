package id.co.telkomsigma.btpns.mprospera.scheduler;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SentEmailNotifAfterNosPOJO;
import id.co.telkomsigma.btpns.mprospera.service.DataNosService;
import id.co.telkomsigma.btpns.mprospera.service.MailEngineService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.UtilCollection;


/*
 * @author : Ilham
 * @Since  : 20190220
 */ 

@Component
public class ReportMailEngineScheduler {

    @Autowired
    private SWService swService;

    @Autowired
    private MailEngineService mailEngineService;

    @Autowired
    private DataNosService dataNosService;
    
    @Autowired
    private UserService userService;

    protected final Log LOGGER = LogFactory.getLog(getClass());

    private UtilCollection util = UtilCollection.getInstance();

    JsonUtils jsonUtils = new JsonUtils();

    @Bean
    public String getCronMAILValue() {
    	String cron = mailEngineService.findByParamName(WebGuiConstant.CRON_TIME_MAILENGINE).getParamValue();
    	LOGGER.info("cron pattern : "+cron);
        return cron; 
    }
    
    @Scheduled(cron = "#{@getCronMAILValue}")
    public void runMailEngineScheduler() {
        try {
            String ipRun = mailEngineService.findByParamName(WebGuiConstant.IP_MAILENGINE_FROM_DB).getParamValue();
            LOGGER.info("Ip Dari DB : " + ipRun);
            String ip = util.getIpAddress();
            LOGGER.info("Ip Ambil Dari Server : " + ip);
            if (ip.equals(ipRun)) {
                LOGGER.info("MAILENGINE Scheduler running.....START...!!");
                mailEngineService.findbyStatus(WebGuiConstant.NOT_SENT);
                List<MailEngineRetail> mailStatus = mailEngineService.findbyStatus(WebGuiConstant.NOT_SENT);
                for (MailEngineRetail ms : mailStatus) {
                    SWSentEmailPOJO requ = new SWSentEmailPOJO();
                    Long swId = ms.getSwId();
                    requ.setSwId(String.valueOf(swId));
                    requ.setUserName(ms.getUsername());
                    requ.setRetrievalReferenceNumber(ms.getRetrievalReferenceNumber());
                    requ.setIsForwarded(ms.getIsForwarded());
                    requ.setEmailAddress(ms.getEmailAddres());
                    requ.setTransmissionDateAndTime(ms.getRetrievalReferenceNumber());
                    requ.setApprovalUsername(ms.getApprovalUsername());
                    requ.setNote(ms.getNote());
                    User findUser = userService.findUserByUsername(ms.getUsername());
                    if (ms.getStatus().equals(WebGuiConstant.NOT_SENT)) {
                        MailEngineRetail mailEngineRetail = mailEngineService.findById(ms.getId());
                        if (swService.isValidSwBySwId(swId).equals(true) && requ.getEmailAddress() != null) {
                            if (requ.getEmailAddress().equals("") || findUser.getRoleUser().equals("1")) {
                            	LOGGER.debug("User Bukan BM / Email Kosong");
                                mailEngineService.save(mailEngineRetail);
                            } else {
                                SurveyWawancara findSWwithRrn = dataNosService.findByRrn(ms.getRetrievalReferenceNumber());
                                if(findSWwithRrn != null) {
                                	if (swService.hasIdPhoto(findSWwithRrn.getSwId().toString()).equals(false) &&
                                            swService.hasSurveyPhoto(findSWwithRrn.getSwId().toString()).equals(false)) {
                                        SentEmailNotifAfterNosPOJO requestForNotifFoto = new SentEmailNotifAfterNosPOJO();
                                        requestForNotifFoto.setTransmissionDateAndTime(null);
                                        requestForNotifFoto.setRetrievalReferenceNumber(ms.getRetrievalReferenceNumber());
                                        requestForNotifFoto.setUserName(ms.getUsername());
                                        requestForNotifFoto.setCustomerName(ms.getCustumerName());
                                        requestForNotifFoto.setAppidNo("-");
                                        requestForNotifFoto.setSentra("-");
                                        requestForNotifFoto.setAlasan("-");
                                        requestForNotifFoto.setMms("-");
                                        LOGGER.debug("SAVE max REatry");
                                        dataNosService.save(mailEngineRetail); //save maxRetry 
                                        if( ms.getMaxRetry() == 3 ) {
                                           dataNosService.generateSendNotifNotComplite(requestForNotifFoto).contains(WebGuiConstant.RC_SUCCESS); 	
                                        }                          
                                    } else {
                                        String mailResponse = mailEngineService.generateNotSend(requ);
                                        if (mailResponse.contains(WebGuiConstant.RC_SUCCESS)) {
                                            mailEngineService.save(mailEngineRetail);
                                            LOGGER.info("SendBySceduller: " + jsonUtils.toJson(requ.getEmailAddress()));
                                        } else {
                                            LOGGER.info(WebGuiConstant.RC_GENERAL_ERROR + "SendByScedullerFailed : " + jsonUtils.toJson(requ));
                                        }
                                    }
                                }else {
                                    mailEngineService.save(mailEngineRetail);
                                }                              
                            }
                        } else {
                            mailEngineService.save(mailEngineRetail);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("runMailEngineScheduler error: " + e.getMessage());
        }
    }

}