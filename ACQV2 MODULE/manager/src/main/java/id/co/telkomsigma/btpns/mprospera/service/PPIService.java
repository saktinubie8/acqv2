package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.PPIManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.ppi.FamilyDataPPI;
import id.co.telkomsigma.btpns.mprospera.model.ppi.PPI;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service("pPIService")
public class PPIService extends GenericService {

    @Autowired
    private PPIManager ppiManager;

    @Autowired
    private UserManager userManager;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public PPI findPPIbySwId(Long swId) {
        return ppiManager.findPPIBySwId(swId);
    }

    public PPI findPPIbyLocalId(String ppiLocalId) {
        return ppiManager.findPPIByLocalId(ppiLocalId);
    }

    public void savePPI(PPI ppi) {
        ppiManager.savePPI(ppi);
    }

    public FamilyDataPPI findFamilyDataByLocalId(String familyLocalId) {
        return ppiManager.findFamilyDataByLocalId(familyLocalId);
    }

    public void saveFamilyData(FamilyDataPPI familyDataPPI) {
        ppiManager.saveFamilyData(familyDataPPI);
    }

    public Page<PPI> getPPI(String username, String page, String getCountData, String startDate,
                            String endDate) throws ParseException {
        if (getCountData == null)
            page = null;
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        int pageNumber = Integer.parseInt(page) - 1;
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        if (startDate == null || startDate.equals("")) {
            if (page == null || page.equals("")) {
                log.info("Accessing PPI Manager");
                return ppiManager.findAllByUser(userIdList);
            } else {
                log.info("Accessing PPI Manager");
                return ppiManager.findAllByUserPageable(userIdList,
                        new PageRequest(pageNumber, Integer.parseInt(getCountData)));
            }
        } else if (page == null || page.equals("")) {
            return ppiManager.findByCreatedDateAndUser(userIdList, formatter.parse(startDate), formatter.parse(endDate));
        } else {
            return ppiManager.findByCreatedDateAndUserPageable(userIdList, formatter.parse(startDate),
                    formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
        }
    }

    public List<PPI> findDeletedPPI() {
        return ppiManager.findDeletedPPI();
    }

    public List<FamilyDataPPI> findFamilyDataByPPIId(Long ppiId) {
        return ppiManager.findFamilyDataByPpiId(ppiId);
    }

}
