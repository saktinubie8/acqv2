package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.PmDao;
import id.co.telkomsigma.btpns.mprospera.manager.PMManager;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SuppressWarnings("RedundantIfStatement")
@Service("pmManager")
public class PMManagerImpl implements PMManager {

    @Autowired
    private PmDao pmDao;

    @Override
    @Caching(evict = {@CacheEvict(value = "isValidPm", key = "#pm.pmId", beforeInvocation = true), @CacheEvict(value = "countAllPmNotDeleted", beforeInvocation = true),
            @CacheEvict(value = "wolverine.pm.isValidPm", key = "#pm.pmId", beforeInvocation = true),
            @CacheEvict(value = "getAllPm", beforeInvocation = true), @CacheEvict(value = "getAllPmPageable", beforeInvocation = true),
            @CacheEvict(value = "findPMByCreatedDate", beforeInvocation = true), @CacheEvict(value = "findPMByCreatedDatePageablePagable", beforeInvocation = true),
            @CacheEvict(value = "getAllPm", beforeInvocation = true), @CacheEvict(value = "ppiacq.pm.findPMByRrn", beforeInvocation = true), @CacheEvict(value = "wolverine.pm.findById", key = "#pm.pmId", beforeInvocation = true), @CacheEvict(value = "findPMByRrn", beforeInvocation = true)})
    public void save(ProjectionMeeting pm) {
        pmDao.save(pm);
    }

    @Override
    @CacheEvict(value = {"wolverine.pm.findById", "isValidPm", "countAllPmNotDeleted", "getAllPm", "getAllPmPageable", "findPMByCreatedDate",
            "findPMByCreatedDatePageablePagable", "findPMByRrn", "wolverine.pm.isValidPm", "ppiacq.pm.findPMByRrn"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "ppiacq.pm.findPMByRrn", unless = "#result == null")
    public ProjectionMeeting findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return pmDao.findByRrn(rrn);
    }

    @Override
    @Cacheable(value = "wolverine.pm.findById", unless = "#result == null")
    public ProjectionMeeting findById(String pmId) {
        // TODO Auto-generated method stub
        return pmDao.findByPmId(Long.parseLong(pmId));
    }

}
