package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.PMManager;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("pmService")
public class PMService extends GenericService {

    @Autowired
    private PMManager pmManager;

    public void save(ProjectionMeeting pm) {
        pmManager.save(pm);
    }

    public ProjectionMeeting findById(String pmId) {
        return pmManager.findById(pmId);
    }

}
