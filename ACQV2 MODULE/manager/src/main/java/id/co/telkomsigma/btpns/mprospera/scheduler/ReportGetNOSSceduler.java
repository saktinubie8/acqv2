package id.co.telkomsigma.btpns.mprospera.scheduler;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.product.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sw.DataNOS;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.MessageNosPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.ScoringRuleNosPojo;
import id.co.telkomsigma.btpns.mprospera.service.DataNosService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;


@Component
public class ReportGetNOSSceduler {
	
	@Autowired
	private DataNosService dataNosService;
	
    @Autowired
    private SWService swService;
	
    @Autowired
    private UserService userService;
	
	protected final Log LOGGER = LogFactory.getLog(getClass());

//	@Bean
//	public String getCronNOSValue()
//	{
//	    return dataNosService.findByParamName(WebGuiConstant.CRON_TIME_NOS).getParamValue() ;
//	}
//	
	//@Scheduled(cron="#{@getCronNOSValue}")
	public void runGetNOSSceduler()  {
		try {
			String ipRun =  dataNosService.findByParamName(WebGuiConstant.IP_NOS_FROM_DB).getParamValue();
			LOGGER.info("ip Run NOS : "+ipRun );
			String ip = getIpAddress();	
			LOGGER.info("IP NOS : "+ip);
			if (ip.equals(ipRun)) {
				LOGGER.info("Sceduler Data NOS Start...!!!!");
				DataNOS dataForUpdateStatus = dataNosService.findByStatus(WebGuiConstant.NOT_SENT);
				if(dataForUpdateStatus.equals(WebGuiConstant.NOT_SENT)) {
					SurveyWawancara findSWwithRrn = dataNosService.findByRrn(dataForUpdateStatus.getRetrievalReferenceNumber());
                	User findUser = userService.findUserByUsername(findSWwithRrn.getCreatedBy());
                	List<User> findEmail = userService.findEmail(findUser.getOfficeCode(), WebGuiConstant.STATUS_ROLE_USER);
                	SWSentEmailPOJO requ =  new SWSentEmailPOJO();
            		SystemParameter valueParam = swService.findByparamName(WebGuiConstant.PLAFON_PARAM);
               		Long nilaiParam = Long.valueOf(valueParam.getParamValue());
               		SWProductMapping productRecPlacon = swService.findByTopBySwid(findSWwithRrn.getSwId());                        		 
               		BigDecimal plafonrec = productRecPlacon.getRecommendedPlafon();
               		Long convertPlafonrec = plafonrec.longValue();
               		
               		DataNOS fileJsonFromNOS = dataNosService.findByRRNumber(dataForUpdateStatus.getRetrievalReferenceNumber());
               		
               		String nosmeseage = fileJsonFromNOS.getFormatJson();
               		Object obj = new JsonUtils().fromJson(nosmeseage, MessageNosPOJO.class);
               		
    				final MessageNosPOJO messageFromNos = (MessageNosPOJO) obj;
               		
               		requ.setRetrievalReferenceNumber(findSWwithRrn.getRrn());
    				LOGGER.info("RetrievalReferenceNumber    : "+requ.getRetrievalReferenceNumber());
    				requ.setUserName(findSWwithRrn.getCreatedBy());
    				LOGGER.info("CreatedBy    : "+requ.getUserName());		
    				for (User userEmail : findEmail) {
    					String email = userEmail.getEmail();
    					requ.setEmailAddress(email);
    					requ.setApprovalUsername(userEmail.getUsername());	
    				}
    				Long swId = findSWwithRrn.getSwId();
    				requ.setSwId(String.valueOf(swId));
    				if (convertPlafonrec > nilaiParam) {
    					requ.setIsForwarded(true);
    				} else {
    					requ.setIsForwarded(false);
    				}
    				
    				if("APPROVED".equals(messageFromNos.getStatus())) {
    					requ.setNote(messageFromNos.getStatus());
    					LOGGER.info("NOTE    : "+requ.getNote());
    				}else {
    					String note = " ";
    					List <ScoringRuleNosPojo> noted = messageFromNos.getScoringResult();
    					for(ScoringRuleNosPojo ws : noted) {
    						if(ws.getResult().equals("FAILED")) {
    					    note = ""+ws.getRejectDesc()+", "+ws.getScoringRule()+", "+ws.getParam()+", "+ws.getResult();	
    						}
    					}
    					requ.setNote(note);
    					LOGGER.info("NOTE    : "+requ.getNote());
    				}
               		
               		
                	if(dataNosService.generateNotSend(requ).contains(WebGuiConstant.RC_SUCCESS)) {
                		dataNosService.update(dataForUpdateStatus);		
                	}else {
                		LOGGER.info("GAGAL SEND EMAIL "+WebGuiConstant.RC_GENERAL_ERROR);
                	}
				}		
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("SCEDULLER GA JALAN,!!!");
		}

	}
	
	private String getIpAddress() {
		String ip = "";
		try {
//			ip = InetAddress.getLocalHost().getHostAddress();
			String interfaceName = "eth0";
			NetworkInterface networkInterface = NetworkInterface.getByName(interfaceName);
			Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
			InetAddress currentAddress;
			currentAddress = inetAddress.nextElement();
			while (inetAddress.hasMoreElements()) {
				currentAddress = inetAddress.nextElement();
				if (currentAddress instanceof InetAddress && !currentAddress.isLoopbackAddress()) {
					ip = currentAddress.toString();
					break;
				}
			}
			
			if(ip.equals("")) {
				final NetworkInterface netInf = NetworkInterface.getNetworkInterfaces().nextElement();
				Enumeration<InetAddress> addresses = netInf.getInetAddresses();
				
				ip = addresses.nextElement().getHostAddress();
			}

		} catch (Exception e) {
			LOGGER.error("error: "+e.getMessage());
		}

		return ip.replace("/", "");
	}
	
	private static AuditLog generateAuditLog(String description) {
		String reffNo = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(AuditLog.GENERATE_REPORT);
		auditLog.setCreatedBy("Mprospera");
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription(description);
		auditLog.setReffNo(reffNo);
		return auditLog;
	}
}
