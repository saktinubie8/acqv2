package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationOfficerMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
public interface DeviationOfficerMapDao extends JpaRepository<DeviationOfficerMapping, Long> {

    List<DeviationOfficerMapping> findByDeviationId(Long id);

    DeviationOfficerMapping findTopByDeviationIdAndDateNotOrderByUpdatedDateDesc(Long id, String status);

    @Query("SELECT COUNT(d) FROM DeviationOfficerMapping d WHERE d.deviationId = :deviationId AND d.date <> :status")
    int countDeviation(@Param("deviationId") Long deviationId, @Param("status") String status);

    @Query("SELECT COUNT(d) FROM DeviationOfficerMapping d WHERE d.deviationId = :deviationId AND d.date = :status")
    int countDeviationWaiting(@Param("deviationId") Long deviationId, @Param("status") String status);

}
