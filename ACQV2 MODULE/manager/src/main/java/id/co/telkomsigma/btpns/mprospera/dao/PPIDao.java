package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.ppi.PPI;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PPIDao extends JpaRepository<PPI, Long> {

    PPI findBySwIdAndDeletedFalse(Long swId);

    PPI findByPpiLocalIdAndDeletedFalse(String ppiLocalId);

    List<PPI> findByDeletedTrue();

    @Query("SELECT COUNT(p) FROM PPI p WHERE p.deleted = false AND p.ppiLocalId is not null")
    int countAll();

    Page<PPI> findByCreatedByInAndPpiLocalIdIsNotNullAndDeletedFalse(List<String> userIdList, Pageable pageRequest);

    @Query("SELECT p FROM PPI p WHERE p.createdDate>=:startDate AND p.createdDate<:endDate AND p.deleted = false " +
            "AND p.createdBy in :userList AND p.ppiLocalId is not null ORDER BY p.createdBy ASC")
    Page<PPI> findByCreatedDateAndUser(@Param("userList") List<String> userIdList,
                                       @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

}
