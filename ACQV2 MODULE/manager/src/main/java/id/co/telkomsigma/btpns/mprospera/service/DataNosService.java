package id.co.telkomsigma.btpns.mprospera.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.DataNosManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailEngineRetail;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.sw.DataNOS;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwForEmail;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SentEmailNotifAfterNosPOJO;

@Service("dataNosService")
public class DataNosService extends GenericService {

    @Autowired
    private DataNosManager dataNosManager;

    @Autowired
    private SentEmailService sentEmailService;

    public SystemParameter findByParamName(String paramName) {
        return dataNosManager.findByparamName(paramName);
    }

    public String generateNotSend(SWSentEmailPOJO sWSentEmailPOJO) {    // send email approval
        return sentEmailService.wsMailEngineValidation(sWSentEmailPOJO);
    }

    public String generateSendNotifAfterNOS(SentEmailNotifAfterNosPOJO sentEmailNotifAfterNosPOJO) { //notif Rejected
        return sentEmailService.wsMailSendNotifAfterNos(sentEmailNotifAfterNosPOJO);
    }

    public String generateSendNotifNotComplite(SentEmailNotifAfterNosPOJO sentEmailNotifAfterNosPOJO) { //notif tanpa photo
        return sentEmailService.wsMailSendNotification(sentEmailNotifAfterNosPOJO);
    }

    public DataNOS findByRRNumber(String rrnumber) {
        return dataNosManager.findByRRNumber(rrnumber);
    }

    public SwForEmail findByFlagStatusScoring(String flagStatusScoring, String retrievalReferenceNumber) {
        return dataNosManager.findByFlagStatusScoring(flagStatusScoring, retrievalReferenceNumber);
    }

    public void update(SwForEmail swForEmail) {
        dataNosManager.update(swForEmail);
    }

    public void save(DataNOS dataNOS) {
        dataNosManager.save(dataNOS);
    }

    public void update(DataNOS nOS) {
        dataNosManager.update(nOS);
    }

    public SurveyWawancara findByRrn(String rrn) {
        return dataNosManager.findByRrn(rrn);
    }

    public DataNOS findByStatus(String status) {
        return dataNosManager.findByStatus(status);
    }

    public void save(MailEngineRetail mail) {
    	dataNosManager.save(mail);
    }
    
}
