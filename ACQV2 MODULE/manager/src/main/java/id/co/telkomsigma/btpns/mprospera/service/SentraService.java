package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("sentraService")
public class SentraService extends GenericService {

    @Autowired
    private SentraManager sentraManager;

    @Autowired
    private UserManager userManager;

    public void save(Sentra sentra) {
        sentraManager.save(sentra);
    }

    public List<Sentra> findAll() {
        // TODO Auto-generated method stub
        return sentraManager.findAll();
    }

    public String findSentraNameById(Long sentraId) {
        return sentraManager.findSentraNameBySentraId(sentraId);
    }
    
    public Group findById(String sentraId) {
        if (sentraId == null || "".equals(sentraId))
          return null;
        return sentraManager.findById(Long.parseLong(sentraId));
    }
}
