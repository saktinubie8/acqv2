package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationOfficerMapping;
import org.springframework.data.domain.Page;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;

public interface LoanManager {

    void save(Loan loan);

    void save(DeviationOfficerMapping deviationOfficerMapping);

    void save(LoanDeviation loanDeviation);

    Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId);

    void clearCache();

    Loan getLoanByRrn(String rrn);

    Loan findById(long parseLong);

    Loan findByAp3rAndCustomer(Long ap3rId, Customer customer);

    Loan getLoanByAp3rId(Long ap3rId);

    LoanDeviation findByAp3rId(Long ap3rId, Boolean isDeleted);

    int countDeviationWithStatus(Long id);

    int countDeviationWaiting(Long id);

    DeviationOfficerMapping findTopByDeviationIdAndStatus(Long deviationId);

    List<DeviationOfficerMapping> findByLoanDeviationId(String deviationId);

    int countLoanBySWId(Long swId);

}
