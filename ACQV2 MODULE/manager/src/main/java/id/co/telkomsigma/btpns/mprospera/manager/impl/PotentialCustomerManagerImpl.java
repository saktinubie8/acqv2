package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.PotentialCustomerDao;
import id.co.telkomsigma.btpns.mprospera.manager.PotentialCustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomer;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerDetail;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerDropOut;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerTopUp;
import id.co.telkomsigma.btpns.mprospera.pojo.ProductEntity;
import id.co.telkomsigma.btpns.mprospera.pojo.RecPlafondEntity;

@Service("potentialCustomerManager")
public class PotentialCustomerManagerImpl implements PotentialCustomerManager {
	
	private final Log log = LogFactory.getLog(getClass());

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	PotentialCustomerDao potentialCustomerDao;
	

	@Override
	public List<PotentialCustomer> getAllPotentialCustomer(Integer locationId) {
		List<PotentialCustomer> results = null;
		try {
			Query query = this.em.createNativeQuery(syncPotentialCustomer, "PotentialCustomer");
			query.setParameter(1, locationId);
			results = query.getResultList();
			em.close();
		} catch (Exception e) {
			log.error("potential customer manutrity manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}

	@Override
	public List<PotentialCustomerTopUp> getAllPotentialCustomerTopUp(Integer locationId) {
		List<PotentialCustomerTopUp> results = null;
		try {
			Query query = this.em.createNativeQuery(syncPotentialCustomerTopUp, "PotentialCustomerTopUp");
			query.setParameter(1, locationId);
			results = query.getResultList();
			em.close();
		} catch (Exception e) {
			log.error("potential customer topup manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}
	
	@Override
	public List<PotentialCustomerDropOut> getPotentialCustomerDropOut(Integer locationId) {
		List<PotentialCustomerDropOut> results = null;
		try {
			Query query = this.em.createNativeQuery(syncPotentialCustomerDropOut, "PotentialCustomerDropOut");
			query.setParameter(1, locationId);
			results = query.getResultList();
			em.close();
		} catch (Exception e) {
			log.error("potential customer dropout manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}
	
	@Override
	public List<PotentialCustomerDetail> getPotentialCustomerDetail(String cifNumber) {
		List<PotentialCustomerDetail> results = null;
		try {
			Query query = this.em.createNativeQuery(syncPotentialCustomerDetail,"PotentialCustomerDetail");
			query.setParameter("cifNumber",cifNumber);
			results = query.getResultList();
			em.close();
		} catch (Exception e) {
			log.error("potential customer details manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}
	
	@Override
	public List<ProductEntity> getProduct(String cifNumber) {
		List<ProductEntity> results=null;
		try {
			Query query = this.em.createNativeQuery(syncProduct,"ProductEntity");
			query.setParameter("cifNumber",cifNumber);
			results = query.getResultList();
			em.close();
		} catch (Exception e) {
			log.error("potential customer syncProduct manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}

	@Override
	public int countTopUpLoan(Long customerId) {
		Integer results = null;
		try {
			Query query = this.em.createNativeQuery(countTopUpLoan);
			query.setParameter("customerId",customerId);
			results = query.getFirstResult();
			em.close();
		} catch (Exception e) {
			log.error("potential customer countTopUpLoan manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}
	
	@Override
	public List<RecPlafondEntity> getPlafond(BigDecimal maxAngsuran) {
		List<RecPlafondEntity> results = null;
		try {
			Query query = this.em.createNativeQuery(recPlafond,"RecPlafondEntity");
			query.setParameter("maxAngsuran",maxAngsuran);
			results = query.getResultList();
			em.close();
		} catch (Exception e) {
			log.error("potential customer getPlafond manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}
	
	@Override
	public String getGrade(Integer jmlAngsur,Integer transaksiBailout,Integer frekwensiAbsen) {
		String tenorMatrix = null;
		
		if (jmlAngsur==25 || jmlAngsur==26) {
			tenorMatrix="matrix_12";
		}else if (jmlAngsur==37 || jmlAngsur==38) {
			tenorMatrix="matrix_18";
		}else if (jmlAngsur==50 || jmlAngsur==51) {
			tenorMatrix="matrix_24";
		}
		return potentialCustomerDao.getGrade(tenorMatrix, transaksiBailout, frekwensiAbsen);
	}


	String syncPotentialCustomer = "exec syncPotentialCustomerMaturity @sentra_location=?1";
	
	String syncPotentialCustomerTopUp = "EXEC syncPotentialCustomerTopUp @sentra_location=?1";
	
	String syncPotentialCustomerDropOut = "EXEC syncPotentialCustomerDropOut @sentra_location=?1";
	
	String syncPotentialCustomerDetail = "EXEC syncPotentialCustomerDetail @cifNumber=:cifNumber";
	
	String syncProduct = "EXEC syncProduct @cifNumber=:cifNumber";

	String countTopUpLoan = "select count(1) from t_loan_account la inner join t_loan_product lp on lp.id = la.product_id " +
			"inner join t_loan_prs lprs on lprs.loan_id = la.id " +
			"where la.customer_id = :customerId and lp.loan_type = 'TipePembiayaan-PembiayaanTopUp' and lprs.overdue_days = 0";
	
	String recPlafond = "EXEC recPlafond @maxAngsuran=:maxAngsuran";
}