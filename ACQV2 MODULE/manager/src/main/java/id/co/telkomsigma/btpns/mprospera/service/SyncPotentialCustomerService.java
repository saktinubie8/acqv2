package id.co.telkomsigma.btpns.mprospera.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.PotentialCustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomer;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerDetail;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerDropOut;
import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomerTopUp;
import id.co.telkomsigma.btpns.mprospera.pojo.ProductEntity;
import id.co.telkomsigma.btpns.mprospera.pojo.RecPlafondEntity;

@Service("syncPotentialCustomerService")
public class SyncPotentialCustomerService {

	@Autowired
	private PotentialCustomerManager potentialCustomerManager;
	
	public List<PotentialCustomer> getPotentialCustomer(Integer locationId) {
		return potentialCustomerManager.getAllPotentialCustomer(locationId);
	}
	
	public List<PotentialCustomerTopUp> getPotentialCustomerTopUp(Integer locationId) {
		return potentialCustomerManager.getAllPotentialCustomerTopUp(locationId);
	}
	
	public List<PotentialCustomerDropOut> getPotentialCustomerDropOut(Integer locationId) {
		return potentialCustomerManager.getPotentialCustomerDropOut(locationId);
	}
	
	public List<PotentialCustomerDetail> getPotentialCustomerDetail(String cifNumber){
		return potentialCustomerManager.getPotentialCustomerDetail(cifNumber);
	}
	
	public List<ProductEntity> getProduct(String cifNumber){
		return potentialCustomerManager.getProduct(cifNumber);
	}

	public int countTopUpLoan(Long customerId){
		return potentialCustomerManager.countTopUpLoan(customerId);
	}
	
	public List<RecPlafondEntity> getPlafond(BigDecimal maxAngsuran){
		return potentialCustomerManager.getPlafond(maxAngsuran);
	}
	
	public String getGrade(Integer jmlAngsur,Integer transaksiBailout,Integer frekwensiAbsen) {
		return potentialCustomerManager.getGrade(jmlAngsur, transaksiBailout, frekwensiAbsen);
	}

}
