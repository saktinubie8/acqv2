package id.co.telkomsigma.btpns.mprospera.constant;

public interface CustomerConstant {

    String Religion_Islam = "716";
    String Religion_Kristen = "717";
    String Religion_Katolik = "718";
    String Religion_Hindu = "719";
    String Religion_Budha = "720";
    String Religion_Lainnya = "721";

    String Residence_Status_Milik_Sendiri = "724";
    String Residence_Status_Orang_Tua = "725";
    String Residence_Status_Mengontrak = "10137";
    String Residence_Status_Desc_Milik_Sendiri = "Milik Sendiri / Pasangan";
    String Residence_Status_Desc_Orang_Tua = "Orang Tua / Anak / Keluarga";
    String Residence_Status_Desc_Mengontrak = "Mengontrak / Pihak Ketiga";

    String Marital_Tidak_Menikah = "710";
    String Marital_Menikah = "714";
    String Marital_Single = "715";
    String Marital_Desc_Tidak_Menikah = "Tidak Menikah";
    String Marital_Desc_Menikah = "Menikah";
    String Marital_Desc_Single = "Single";

    String Edu_SD = "706";
    String Edu_SLTP = "707";
    String Edu_SLTA = "708";
    String Edu_S1 = "709";
    String Edu_Desc_SD = "< SD";
    String Edu_Desc_SLTP = "SLTP";
    String Edu_Desc_SLTA = "SLTA";
    String Edu_Desc_S1 = "S1";

    String Fund_Source_Hasil_Usaha = "726";
    String Fund_Source_Lainnya = "727";
    String Fund_Source_Gaji = "10160";

    String Reason_Open_Account_Tabungan = "728";
    String Reason_Open_Account_Lainnya = "729";
    String Reason_Open_Transaksi_Usaha = "10163";

    String Gender_Laki_Laki = "49";
    String Gender_Perempuan = "50";

}