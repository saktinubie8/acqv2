package id.co.telkomsigma.btpns.mprospera.pojo.sw;

public class SWSentEmailPOJO {

    private String userName;
    private String customerName;
    private String retrievalReferenceNumber;
    private Boolean isForwarded ;
    private String emailAddress;
    private String swId;
    private String transmissionDateAndTime;
    private String approvalUsername;
    private String note;
    private Boolean isInReview;
    private String kodeDeviasi;
 
    
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	public boolean getIsForwarded() {
		return isForwarded;
	}
	public void setIsForwarded(boolean isForwarded) {	
		this.isForwarded = isForwarded;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getSwId() {
		return swId;
	}
	public void setSwId(String swId) {
		this.swId = swId;
	}
	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}
	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}
	public String getApprovalUsername() {
		return approvalUsername;
	}
	public void setApprovalUsername(String approvalUsername) {
		this.approvalUsername = approvalUsername;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	public Boolean getIsInReview() {
		return isInReview;
	}
	public void setIsInReview(Boolean isInReview) {
		this.isInReview = isInReview;
	}
	public String getKodeDeviasi() {
		return kodeDeviasi;
	}
	public void setKodeDeviasi(String kodeDeviasi) {
		this.kodeDeviasi = kodeDeviasi;
	}
	
	
}
