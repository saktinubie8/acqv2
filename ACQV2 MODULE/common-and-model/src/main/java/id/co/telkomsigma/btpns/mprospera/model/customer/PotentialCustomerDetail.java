package id.co.telkomsigma.btpns.mprospera.model.customer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "PotentialCustomerDetail", entities = {
		@EntityResult(entityClass = PotentialCustomerDetail.class, fields = {
				@FieldResult(name = "customerId", column = "id"),
				@FieldResult(name = "customerName", column = "name"),
				@FieldResult(name = "cifNumber", column = "cif_number"),
				@FieldResult(name = "address", column = "address"),
				@FieldResult(name = "phoneNumber", column = "phone_number"),
				@FieldResult(name = "numberOfVisit", column = "jumlah_kunjungan"),
				@FieldResult(name = "pbu", column = "PBU"),
				@FieldResult(name = "iir", column = "iir"),
				
		}) })

@Entity
public class PotentialCustomerDetail implements Serializable{

	
/**
	 * 
	 */
	private static final long serialVersionUID = 1787789760347984820L;
@Id protected Long customerId;
	private String customerName;
	private String cifNumber;
	private String address;
	private String phoneNumber;
	private Integer numberOfVisit;
	private BigDecimal pbu;
	private BigDecimal iir;
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCifNumber() {
		return cifNumber;
	}
	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Integer getNumberOfVisit() {
		return numberOfVisit;
	}
	public void setNumberOfVisit(Integer numberOfVisit) {
		this.numberOfVisit = numberOfVisit;
	}
	public BigDecimal getPbu() {
		return pbu;
	}
	public void setPbu(BigDecimal pbu) {
		this.pbu = pbu;
	}
	public BigDecimal getIir() {
		return iir;
	}
	public void setIir(BigDecimal iir) {
		this.iir = iir;
	}
}
