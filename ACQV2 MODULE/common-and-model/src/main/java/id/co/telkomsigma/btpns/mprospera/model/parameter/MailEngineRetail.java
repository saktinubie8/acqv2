package id.co.telkomsigma.btpns.mprospera.model.parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "M_MAIL_REQUEST_RETAIL")
public class MailEngineRetail extends GenericModel{
	
    private static final long serialVersionUID = 1L;
	private Long id;
	private Long swId;
	private String status;
	private String username;
	private String imei;
	private String sessionkey;
	private String retrievalReferenceNumber;
	private String custumerName;
	private Boolean isForwarded;
	private String emailAddres;
	private String transmissionDateAndTime;
	private String approvalUsername;
	private String note;
    private Boolean isInReview;
    private String kodeDeviasi;
    private Integer maxRetry;
    private Date createdt;
    private String createby;
    private String updateby;
    private Date updatedt;
	
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "sw_id")
	public Long getSwId() {
		return swId;
	}
	public void setSwId(Long swId) {
		this.swId = swId;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name = "imei")
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	
	@Column(name = "sessionkey")
	public String getSessionkey() {
		return sessionkey;
	}
	public void setSessionkey(String sessionkey) {
		this.sessionkey = sessionkey;
	}
	
	@Column(name = "retrieval_reference_number")
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	
	@Column(name = "custumer_name")
	public String getCustumerName() {
		return custumerName;
	}
	public void setCustumerName(String custumerName) {
		this.custumerName = custumerName;
	}
	
	@Column(name = "is_Forwarded")
	public Boolean getIsForwarded() {
		return isForwarded;
	}
	public void setIsForwarded(Boolean isForwarded) {
		this.isForwarded = isForwarded;
	}
	
	@Column(name = "email_Address")
	public String getEmailAddres() {
		return emailAddres;
	}
	public void setEmailAddres(String emailAddres) {
		this.emailAddres = emailAddres;
	}
	
	@Column(name = "transmission_Date_And_Time")
	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}
	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}
	
	@Column(name = "approval_Username")
	public String getApprovalUsername() {
		return approvalUsername;
	}
	public void setApprovalUsername(String approvalUsername) {
		this.approvalUsername = approvalUsername;
	}
	
	@Column(name = "note")
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	@Column(name = "kode_Deviasi")
	public String getKodeDeviasi() {
		return kodeDeviasi;
	}
	public void setKodeDeviasi(String kodeDeviasi) {
		this.kodeDeviasi = kodeDeviasi;
	}	
	
	@Column(name = "isIn_Review")
	public boolean isInReview() {
		return isInReview;
	}
	public void setInReview(boolean isInReview) {
		this.isInReview = isInReview;
	}
	
	@Column(name = "max_Try")
	public Integer getMaxRetry() {
		return maxRetry;
	}
	public void setMaxRetry(Integer maxRetry) {
		this.maxRetry = maxRetry;
	}
	
	@Column(name = "create_Dt")
	public Date getCreatedt() {
		return createdt;
	}
	public void setCreatedt(Date createdt) {
		this.createdt = createdt;
	}
	
	@Column(name = "create_By")
	public String getCreateby() {
		return createby;
	}
	public void setCreateby(String createby) {
		this.createby = createby;
	}
	
	@Column(name = "update_By")
	public String getUpdateby() {
		return updateby;
	}
	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}
	
	@Column(name = "update_Dt")
	public Date getUpdatedt() {
		return updatedt;
	}
	public void setUpdatedt(Date updatedt) {
		this.updatedt = updatedt;
	}
}
