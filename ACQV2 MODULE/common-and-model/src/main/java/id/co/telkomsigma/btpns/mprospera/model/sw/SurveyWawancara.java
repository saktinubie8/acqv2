package id.co.telkomsigma.btpns.mprospera.model.sw;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;

import javax.persistence.*;

@Entity
@Table(name = "T_SURVEY_WAWANCARA")
public class SurveyWawancara extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 8383613399759073380L;
    private ProjectionMeeting pmId;
    private Long swId;
    private String localId;
    private String wismaId;
    private String areaId;
    private String customerIdNumber;
    private String customerName;
    private String swLocation;
    private String phoneNumber;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;
    private Character customerRegistrationType;
    private String customerAliasName;
    private String customerIdName;
    private String religion;
    private String gender;
    private String birthPlace;
    private String birthPlaceRegencyName;
    private Date birthDate;
    private Date idExpiryDate;
    private String maritalStatus;
    private String npwp;
    private String motherName;
    private Integer dependentCount;
    private String education;
    private Long customerId;
    private String workType;
    private Date surveyDate;
    private String address;
    private String rtrw;
    private String postalCode;
    private String province;
    private String city;
    private String kecamatan;
    private String kelurahan;
    private String domisiliAddress;
    private String houseType;
    private Character houseCertificate;
    private String coupleName;
    private String coupleBirthPlace;
    private String coupleBirthPlaceRegencyName;
    private Date coupleBirthDate;
    private Character coupleJob;
    private String businessField;
    private String businessDescription;
    private Character businessOwnerStatus;
    private Character businessShariaType;
    private Character businessLocation;
    private Character businessWorkStatus;
    private Character hasOtherBankLoan;
    private String otherBankLoanName;
    private String businessName;
    private String businessAddress;
    private Integer businessAgeYear;
    private Integer businessAgeMonth;
    private Character businessCycle;
    private String businessDaysOperation;
    private BigDecimal otherIncome;
    private BigDecimal totalIncome;
    private BigDecimal nettIncome;
    private BigDecimal oneDayBusyIncome;
    private Integer totalDaysInMonthBusy;
    private BigDecimal oneDayLessIncome;
    private Integer totalDaysInMonthLess;
    private BigDecimal totalLessBusyIncome;
    private BigDecimal firstDayIncome;
    private BigDecimal secondDayIncome;
    private BigDecimal thirdDayIncome;
    private BigDecimal threePeriodIncome;
    private BigDecimal avgThreePeriodIncome;
    private Time openTime;
    private Time closeTime;
    private Integer workTime;
    private Integer workTimeAfterOpen;
    private BigDecimal cashOpenTime;
    private BigDecimal cashCurrentTime;
    private BigDecimal cashExpense;
    private BigDecimal expenditure;
    private BigDecimal oneDayPredictIncome;
    private BigDecimal oneDayCashIncome;
    private String otherBusinessIncomeName;
    private Integer otherBusinessAmount;
    private Integer otherBusinessPeriod;
    private Integer otherBusinessMonthlyIncome;
    private BigDecimal totalBuy;
    private BigDecimal transportCost;
    private BigDecimal utilityCost;
    private BigDecimal staffSalary;
    private BigDecimal rentCost;
    private BigDecimal businessCost;
    private BigDecimal foodCost;
    private BigDecimal privateUtilityCost;
    private BigDecimal educationCost;
    private BigDecimal healthCost;
    private BigDecimal privateTransportCost;
    private BigDecimal installmentCost;
    private BigDecimal otherCost;
    private BigDecimal nonBusinessCost;
    private String time;
    private Boolean isFinalRecommendation = false;
    private String longitude;
    private String latitude;
    private Boolean isDeleted = false;
    private String rrn;
    private String status;
    private String rejectedReason;
    private Boolean isLifeTime = false;
    private Boolean hasApprovedMs = false;
    private BigDecimal residualIncome;
    private BigDecimal iir;
    private BigDecimal idir;
    private Boolean swPPI = false;
    private Boolean hasPPI = false;
    private Boolean isNabasahPotensial = false;
    private Date disbursementDate;
    private Boolean isNeedDevition;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getSwId() {
        return swId;
    }

    public void setSwId(Long swId) {
        this.swId = swId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ProjectionMeeting.class)
    @JoinColumn(name = "pm_id", referencedColumnName = "id", nullable = true)
    public ProjectionMeeting getPmId() {
        return pmId;
    }

    public void setPmId(ProjectionMeeting pmId) {
        this.pmId = pmId;
    }

    @Column(name = "area_id", nullable = true)
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Column(name = "cust_id_number", nullable = true)
    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    @Column(name = "cust_name", nullable = false)
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(name = "created_by", nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "created_date", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "cust_reg_type", nullable = true)
    public Character getCustomerRegistrationType() {
        return customerRegistrationType;
    }

    public void setCustomerRegistrationType(Character customerRegistrationType) {
        this.customerRegistrationType = customerRegistrationType;
    }

    @Column(name = "work_type", nullable = true)
    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    @Column(name = "survey_date", nullable = true)
    public Date getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(Date surveyDate) {
        this.surveyDate = surveyDate;
    }

    @Column(name = "house_type", nullable = true)
    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    @Column(name = "is_final_recommedation", nullable = true)
    public Boolean getIsFinalRecommendation() {
        return isFinalRecommendation;
    }

    public void setIsFinalRecommendation(Boolean isFinalRecommendation) {
        this.isFinalRecommendation = isFinalRecommendation;
    }

    @Column(name = "longitude", nullable = true)
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude", nullable = true)
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "is_deleted", nullable = true)
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    @Column(name = "sw_location")
    public String getSwLocation() {
        return swLocation;
    }

    public void setSwLocation(String swLocation) {
        this.swLocation = swLocation;
    }

    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "timer")
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "customer_id")
    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Column(name = "alias_name")
    public String getCustomerAliasName() {
        return customerAliasName;
    }

    public void setCustomerAliasName(String customerAliasName) {
        this.customerAliasName = customerAliasName;
    }

    @Column(name = "id_name")
    public String getCustomerIdName() {
        return customerIdName;
    }

    public void setCustomerIdName(String customerIdName) {
        this.customerIdName = customerIdName;
    }

    @Column(name = "religion")
    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    @Column(name = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "birth_place")
    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    @Column(name = "birth_date")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Column(name = "id_expiry_date")
    public Date getIdExpiryDate() {
        return idExpiryDate;
    }

    public void setIdExpiryDate(Date idExpiryDate) {
        this.idExpiryDate = idExpiryDate;
    }

    @Column(name = "marital_status")
    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Column(name = "npwp")
    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    @Column(name = "mother_name")
    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    @Column(name = "dependent_count")
    public Integer getDependentCount() {
        return dependentCount;
    }

    public void setDependentCount(Integer dependentCount) {
        this.dependentCount = dependentCount;
    }

    @Column(name = "education")
    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "rt_rw")
    public String getRtrw() {
        return rtrw;
    }

    public void setRtrw(String rtrw) {
        this.rtrw = rtrw;
    }

    @Column(name = "postal_code")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Column(name = "province")
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "kecamatan")
    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    @Column(name = "kelurahan")
    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    @Column(name = "domisili")
    public String getDomisiliAddress() {
        return domisiliAddress;
    }

    public void setDomisiliAddress(String domisiliAddress) {
        this.domisiliAddress = domisiliAddress;
    }

    @Column(name = "house_cert")
    public Character getHouseCertificate() {
        return houseCertificate;
    }

    public void setHouseCertificate(Character houseCertificate) {
        this.houseCertificate = houseCertificate;
    }

    @Column(name = "couple_name")
    public String getCoupleName() {
        return coupleName;
    }

    public void setCoupleName(String coupleName) {
        this.coupleName = coupleName;
    }

    @Column(name = "couple_birth_place")
    public String getCoupleBirthPlace() {
        return coupleBirthPlace;
    }

    public void setCoupleBirthPlace(String coupleBirthPlace) {
        this.coupleBirthPlace = coupleBirthPlace;
    }

    @Column(name = "couple_birth_date")
    public Date getCoupleBirthDate() {
        return coupleBirthDate;
    }

    public void setCoupleBirthDate(Date coupleBirthDate) {
        this.coupleBirthDate = coupleBirthDate;
    }

    @Column(name = "couple_job")
    public Character getCoupleJob() {
        return coupleJob;
    }

    public void setCoupleJob(Character coupleJob) {
        this.coupleJob = coupleJob;
    }

    @Column(name = "business_field")
    public String getBusinessField() {
        return businessField;
    }

    public void setBusinessField(String businessField) {
        this.businessField = businessField;
    }

    @Column(name = "business_desc")
    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }

    @Column(name = "business_owner_status")
    public Character getBusinessOwnerStatus() {
        return businessOwnerStatus;
    }

    public void setBusinessOwnerStatus(Character businessOwnerStatus) {
        this.businessOwnerStatus = businessOwnerStatus;
    }

    @Column(name = "business_sharia_type")
    public Character getBusinessShariaType() {
        return businessShariaType;
    }

    public void setBusinessShariaType(Character businessShariaType) {
        this.businessShariaType = businessShariaType;
    }

    @Column(name = "business_location")
    public Character getBusinessLocation() {
        return businessLocation;
    }

    public void setBusinessLocation(Character businessLocation) {
        this.businessLocation = businessLocation;
    }

    @Column(name = "business_work_status")
    public Character getBusinessWorkStatus() {
        return businessWorkStatus;
    }

    public void setBusinessWorkStatus(Character businessWorkStatus) {
        this.businessWorkStatus = businessWorkStatus;
    }

    @Column(name = "business_name")
    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    @Column(name = "business_address")
    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    @Column(name = "business_age_year")
    public Integer getBusinessAgeYear() {
        return businessAgeYear;
    }

    public void setBusinessAgeYear(Integer businessAgeYear) {
        this.businessAgeYear = businessAgeYear;
    }

    @Column(name = "business_age_month")
    public Integer getBusinessAgeMonth() {
        return businessAgeMonth;
    }

    public void setBusinessAgeMonth(Integer businessAgeMonth) {
        this.businessAgeMonth = businessAgeMonth;
    }

    @Column(name = "business_cycle")
    public Character getBusinessCycle() {
        return businessCycle;
    }

    public void setBusinessCycle(Character businessCycle) {
        this.businessCycle = businessCycle;
    }

    @Column(name = "business_days_operation")
    public String getBusinessDaysOperation() {
        return businessDaysOperation;
    }

    public void setBusinessDaysOperation(String businessDaysOperation) {
        this.businessDaysOperation = businessDaysOperation;
    }

    @Column(name = "other_income")
    public BigDecimal getOtherIncome() {
        return otherIncome;
    }

    public void setOtherIncome(BigDecimal otherIncome) {
        this.otherIncome = otherIncome;
    }

    @Column(name = "total_income")
    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    @Column(name = "one_day_busy_income")
    public BigDecimal getOneDayBusyIncome() {
        return oneDayBusyIncome;
    }

    public void setOneDayBusyIncome(BigDecimal oneDayBusyIncome) {
        this.oneDayBusyIncome = oneDayBusyIncome;
    }

    @Column(name = "total_days_month_busy")
    public Integer getTotalDaysInMonthBusy() {
        return totalDaysInMonthBusy;
    }

    public void setTotalDaysInMonthBusy(Integer totalDaysInMonthBusy) {
        this.totalDaysInMonthBusy = totalDaysInMonthBusy;
    }

    @Column(name = "one_day_less_income")
    public BigDecimal getOneDayLessIncome() {
        return oneDayLessIncome;
    }

    public void setOneDayLessIncome(BigDecimal oneDayLessIncome) {
        this.oneDayLessIncome = oneDayLessIncome;
    }

    @Column(name = "total_days_month_less")
    public Integer getTotalDaysInMonthLess() {
        return totalDaysInMonthLess;
    }

    public void setTotalDaysInMonthLess(Integer totalDaysInMonthLess) {
        this.totalDaysInMonthLess = totalDaysInMonthLess;
    }

    @Column(name = "first_day_income")
    public BigDecimal getFirstDayIncome() {
        return firstDayIncome;
    }

    public void setFirstDayIncome(BigDecimal firstDayIncome) {
        this.firstDayIncome = firstDayIncome;
    }

    @Column(name = "second_day_income")
    public BigDecimal getSecondDayIncome() {
        return secondDayIncome;
    }

    public void setSecondDayIncome(BigDecimal secondDayIncome) {
        this.secondDayIncome = secondDayIncome;
    }

    @Column(name = "third_day_income")
    public BigDecimal getThirdDayIncome() {
        return thirdDayIncome;
    }

    public void setThirdDayIncome(BigDecimal thirdDayIncome) {
        this.thirdDayIncome = thirdDayIncome;
    }

    @Column(name = "open_time")
    public Time getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Time openTime) {
        this.openTime = openTime;
    }

    @Column(name = "close_time")
    public Time getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Time closeTime) {
        this.closeTime = closeTime;
    }

    @Column(name = "cash_open_time")
    public BigDecimal getCashOpenTime() {
        return cashOpenTime;
    }

    public void setCashOpenTime(BigDecimal cashOpenTime) {
        this.cashOpenTime = cashOpenTime;
    }

    @Column(name = "cash_current_time")
    public BigDecimal getCashCurrentTime() {
        return cashCurrentTime;
    }

    public void setCashCurrentTime(BigDecimal cashCurrentTime) {
        this.cashCurrentTime = cashCurrentTime;
    }

    @Column(name = "expenditure")
    public BigDecimal getExpenditure() {
        return expenditure;
    }

    public void setExpenditure(BigDecimal expenditure) {
        this.expenditure = expenditure;
    }

    @Column(name = "total_buy")
    public BigDecimal getTotalBuy() {
        return totalBuy;
    }

    public void setTotalBuy(BigDecimal totalBuy) {
        this.totalBuy = totalBuy;
    }

    @Column(name = "transport_cost")
    public BigDecimal getTransportCost() {
        return transportCost;
    }

    public void setTransportCost(BigDecimal transportCost) {
        this.transportCost = transportCost;
    }

    @Column(name = "utility_cost")
    public BigDecimal getUtilityCost() {
        return utilityCost;
    }

    public void setUtilityCost(BigDecimal utilityCost) {
        this.utilityCost = utilityCost;
    }

    @Column(name = "staff_salary")
    public BigDecimal getStaffSalary() {
        return staffSalary;
    }

    public void setStaffSalary(BigDecimal staffSalary) {
        this.staffSalary = staffSalary;
    }

    @Column(name = "rent_cost")
    public BigDecimal getRentCost() {
        return rentCost;
    }

    public void setRentCost(BigDecimal rentCost) {
        this.rentCost = rentCost;
    }

    @Column(name = "business_cost")
    public BigDecimal getBusinessCost() {
        return businessCost;
    }

    public void setBusinessCost(BigDecimal businessCost) {
        this.businessCost = businessCost;
    }

    @Column(name = "food_cost")
    public BigDecimal getFoodCost() {
        return foodCost;
    }

    public void setFoodCost(BigDecimal foodCost) {
        this.foodCost = foodCost;
    }

    @Column(name = "private_utility_cost")
    public BigDecimal getPrivateUtilityCost() {
        return privateUtilityCost;
    }

    public void setPrivateUtilityCost(BigDecimal privateUtilityCost) {
        this.privateUtilityCost = privateUtilityCost;
    }

    @Column(name = "education_cost")
    public BigDecimal getEducationCost() {
        return educationCost;
    }

    public void setEducationCost(BigDecimal educationCost) {
        this.educationCost = educationCost;
    }

    @Column(name = "health_cost")
    public BigDecimal getHealthCost() {
        return healthCost;
    }

    public void setHealthCost(BigDecimal healthCost) {
        this.healthCost = healthCost;
    }

    @Column(name = "private_transport_cost")
    public BigDecimal getPrivateTransportCost() {
        return privateTransportCost;
    }

    public void setPrivateTransportCost(BigDecimal privateTransportCost) {
        this.privateTransportCost = privateTransportCost;
    }

    @Column(name = "installment_cost")
    public BigDecimal getInstallmentCost() {
        return installmentCost;
    }

    public void setInstallmentCost(BigDecimal installmentCost) {
        this.installmentCost = installmentCost;
    }

    @Column(name = "other_cost")
    public BigDecimal getOtherCost() {
        return otherCost;
    }

    public void setOtherCost(BigDecimal otherCost) {
        this.otherCost = otherCost;
    }

    @Column(name = "non_business_cost")
    public BigDecimal getNonBusinessCost() {
        return nonBusinessCost;
    }

    public void setNonBusinessCost(BigDecimal nonBusinessCost) {
        this.nonBusinessCost = nonBusinessCost;
    }

    @Column(name = "rejected_reason")
    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }

    @Column(name = "is_lifetime")
    public Boolean getLifeTime() {
        return isLifeTime;
    }

    public void setLifeTime(Boolean lifeTime) {
        isLifeTime = lifeTime;
    }

    @Column(name = "wisma_id")
    public String getWismaId() {
        return wismaId;
    }

    public void setWismaId(String wismaId) {
        this.wismaId = wismaId;
    }

    @Column(name = "has_approved_ms")
    public Boolean getHasApprovedMs() {
        return hasApprovedMs;
    }

    public void setHasApprovedMs(Boolean hasApprovedMs) {
        this.hasApprovedMs = hasApprovedMs;
    }

    @Column(name = "total_less_busy_income")
    public BigDecimal getTotalLessBusyIncome() {
        return totalLessBusyIncome;
    }

    public void setTotalLessBusyIncome(BigDecimal totalLessBusyIncome) {
        this.totalLessBusyIncome = totalLessBusyIncome;
    }

    @Column(name = "three_period_income")
    public BigDecimal getThreePeriodIncome() {
        return threePeriodIncome;
    }

    public void setThreePeriodIncome(BigDecimal threePeriodIncome) {
        this.threePeriodIncome = threePeriodIncome;
    }

    @Column(name = "work_time")
    public Integer getWorkTime() {
        return workTime;
    }

    public void setWorkTime(Integer workTime) {
        this.workTime = workTime;
    }

    @Column(name = "work_time_after_open")
    public Integer getWorkTimeAfterOpen() {
        return workTimeAfterOpen;
    }

    public void setWorkTimeAfterOpen(Integer workTimeAfterOpen) {
        this.workTimeAfterOpen = workTimeAfterOpen;
    }

    @Column(name = "one_day_predict_income")
    public BigDecimal getOneDayPredictIncome() {
        return oneDayPredictIncome;
    }

    public void setOneDayPredictIncome(BigDecimal oneDayPredictIncome) {
        this.oneDayPredictIncome = oneDayPredictIncome;
    }

    @Column(name = "one_day_cash_income")
    public BigDecimal getOneDayCashIncome() {
        return oneDayCashIncome;
    }

    public void setOneDayCashIncome(BigDecimal oneDayCashIncome) {
        this.oneDayCashIncome = oneDayCashIncome;
    }

    @Column(name = "other_business_income_name")
    public String getOtherBusinessIncomeName() {
        return otherBusinessIncomeName;
    }

    public void setOtherBusinessIncomeName(String otherBusinessIncomeName) {
        this.otherBusinessIncomeName = otherBusinessIncomeName;
    }

    @Column(name = "other_business_amount")
    public Integer getOtherBusinessAmount() {
        return otherBusinessAmount;
    }

    public void setOtherBusinessAmount(Integer otherBusinessAmount) {
        this.otherBusinessAmount = otherBusinessAmount;
    }

    @Column(name = "other_business_period")
    public Integer getOtherBusinessPeriod() {
        return otherBusinessPeriod;
    }

    public void setOtherBusinessPeriod(Integer otherBusinessPeriod) {
        this.otherBusinessPeriod = otherBusinessPeriod;
    }

    @Column(name = "other_business_monthly_income")
    public Integer getOtherBusinessMonthlyIncome() {
        return otherBusinessMonthlyIncome;
    }

    public void setOtherBusinessMonthlyIncome(Integer otherBusinessMonthlyIncome) {
        this.otherBusinessMonthlyIncome = otherBusinessMonthlyIncome;
    }

    @Column(name = "birth_place_name")
    public String getBirthPlaceRegencyName() {
        return birthPlaceRegencyName;
    }

    public void setBirthPlaceRegencyName(String birthPlaceRegencyName) {
        this.birthPlaceRegencyName = birthPlaceRegencyName;
    }

    @Column(name = "couple_birth_place_name")
    public String getCoupleBirthPlaceRegencyName() {
        return coupleBirthPlaceRegencyName;
    }

    public void setCoupleBirthPlaceRegencyName(String coupleBirthPlaceRegencyName) {
        this.coupleBirthPlaceRegencyName = coupleBirthPlaceRegencyName;
    }

    @Column(name = "nett_income")
    public BigDecimal getNettIncome() {
        return nettIncome;
    }

    public void setNettIncome(BigDecimal nettIncome) {
        this.nettIncome = nettIncome;
    }

    @Column(name = "cash_expense")
    public BigDecimal getCashExpense() {
        return cashExpense;
    }

    public void setCashExpense(BigDecimal cashExpense) {
        this.cashExpense = cashExpense;
    }

    @Column(name = "local_id")
    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    @Column(name = "residual_income")
    public BigDecimal getResidualIncome() {
        return residualIncome;
    }

    public void setResidualIncome(BigDecimal residualIncome) {
        this.residualIncome = residualIncome;
    }

    @Column(name = "iir")
    public BigDecimal getIir() {
        return iir;
    }

    public void setIir(BigDecimal iir) {
        this.iir = iir;
    }

    @Column(name = "idir")
    public BigDecimal getIdir() {
        return idir;
    }

    public void setIdir(BigDecimal idir) {
        this.idir = idir;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "updated_dt")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "avg_three_period_income")
    public BigDecimal getAvgThreePeriodIncome() {
        return avgThreePeriodIncome;
    }

    public void setAvgThreePeriodIncome(BigDecimal avgThreePeriodIncome) {
        this.avgThreePeriodIncome = avgThreePeriodIncome;
    }

    @Column(name = "has_other_bank_loan")
    public Character getHasOtherBankLoan() {
        return hasOtherBankLoan;
    }

    public void setHasOtherBankLoan(Character hasOtherBankLoan) {
        this.hasOtherBankLoan = hasOtherBankLoan;
    }

    @Column(name = "other_bank_loan_name")
    public String getOtherBankLoanName() {
        return otherBankLoanName;
    }

    public void setOtherBankLoanName(String otherBankLoanName) {
        this.otherBankLoanName = otherBankLoanName;
    }

    @Column(name = "sw_ppi")
    public Boolean getSwPPI() {
        return swPPI;
    }

    public void setSwPPI(Boolean swPPI) {
        this.swPPI = swPPI;
    }

    @Column(name = "has_ppi")
    public Boolean getHasPPI() {
        return hasPPI;
    }

    public void setHasPPI(Boolean hasPPI) {
        this.hasPPI = hasPPI;
    }

    @Column(name = "disbursement_date")
    public Date getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(Date disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    @Column(name = "is_nasabah_potensial")
    public Boolean getNabasahPotensial() {
        return isNabasahPotensial;
    }

    public void setNabasahPotensial(Boolean nabasahPotensial) {
        isNabasahPotensial = nabasahPotensial;
    }
    
    @Column(name = "is_need_devition")
	public Boolean getIsNeedDevition() {
		return isNeedDevition;
	}

	public void setIsNeedDevition(Boolean isNeedDevition) {
		this.isNeedDevition = isNeedDevition;
	}
    
    

}