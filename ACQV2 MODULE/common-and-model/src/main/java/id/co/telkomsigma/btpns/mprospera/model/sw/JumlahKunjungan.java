package id.co.telkomsigma.btpns.mprospera.model.sw;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name="t_jumlah_kunjungan")
public class JumlahKunjungan extends GenericModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1460629770691722143L;
	private Long id;
	private String cifNumber;
	private String createdBy;
	private Date visitDate;
	private Boolean isReset = false;
	private Boolean meetCustDirect = false;
	private Boolean custBusinessExists = false;
	private Boolean custBusinessRun = false;
	private Boolean custApplyLoan = false;
	private String notApplyLoanReason;
	
	@Id
	@GeneratedValue
	@Column(name="id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="cif_number", nullable=false)
	public String getCifNumber() {
		return cifNumber;
	}
	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}
	
	@Column(name="created_by", nullable=false)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="visit_date", nullable=false)
	public Date getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}
	
	@Column(name="is_reset", nullable=false)
	public Boolean getIsReset() {
		return isReset;
	}
	public void setIsReset(Boolean isReset) {
		this.isReset = isReset;
	}

	@Column(name="meet_cust_direct")
	public Boolean getMeetCustDirect() {
		return meetCustDirect;
	}

	public void setMeetCustDirect(Boolean meetCustDirect) {
		this.meetCustDirect = meetCustDirect;
	}

	@Column(name="cust_business_exists")
	public Boolean getCustBusinessExists() {
		return custBusinessExists;
	}

	public void setCustBusinessExists(Boolean custBusinessExists) {
		this.custBusinessExists = custBusinessExists;
	}

	@Column(name="cust_business_run")
	public Boolean getCustBusinessRun() {
		return custBusinessRun;
	}

	public void setCustBusinessRun(Boolean custBusinessRun) {
		this.custBusinessRun = custBusinessRun;
	}

	@Column(name="cust_apply_loan")
	public Boolean getCustApplyLoan() {
		return custApplyLoan;
	}

	public void setCustApplyLoan(Boolean custApplyLoan) {
		this.custApplyLoan = custApplyLoan;
	}

	@Column(name="not_apply_loan_reason")
	public String getNotApplyLoanReason() {
		return notApplyLoanReason;
	}

	public void setNotApplyLoanReason(String notApplyLoanReason) {
		this.notApplyLoanReason = notApplyLoanReason;
	}
}
