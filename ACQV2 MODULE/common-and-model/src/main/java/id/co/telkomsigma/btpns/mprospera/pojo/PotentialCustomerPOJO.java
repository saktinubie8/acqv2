package id.co.telkomsigma.btpns.mprospera.pojo;

public class PotentialCustomerPOJO {

	private String customerId;
    private String customerName;
    private String cifNumber;
    private String maturityDate;
    private String productName;
    private Integer plafond;
    private String sentraName;
    private Integer jmlKunjungan;
    private Boolean maturityFlag = false;
    private Boolean topUpFlag = false;
    private Boolean dropOutFlag = false;
    
    public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCifNumber() {
        return cifNumber;
    }

    public void setCifNumber(String cifNumber) {
        this.cifNumber = cifNumber;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getPlafond() {
        return plafond;
    }

    public void setPlafond(Integer plafond) {
        this.plafond = plafond;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public Integer getJmlKunjungan() {
		return jmlKunjungan;
	}

	public void setJmlKunjungan(Integer jmlKunjungan) {
		this.jmlKunjungan = jmlKunjungan;
	}

	public Boolean getMaturityFlag() {
        return maturityFlag;
    }

    public void setMaturityFlag(Boolean maturityFlag) {
        this.maturityFlag = maturityFlag;
    }

    public Boolean getDropOutFlag() {
        return dropOutFlag;
    }

    public void setDropOutFlag(Boolean dropOutFlag) {
        this.dropOutFlag = dropOutFlag;
    }

    public Boolean getTopUpFlag() {
        return topUpFlag;
    }

    public void setTopUpFlag(Boolean topUpFlag) {
        this.topUpFlag = topUpFlag;
    }

}