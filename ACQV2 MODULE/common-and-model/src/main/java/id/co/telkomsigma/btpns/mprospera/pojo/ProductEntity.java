package id.co.telkomsigma.btpns.mprospera.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "ProductEntity", entities = {
		@EntityResult(entityClass = ProductEntity.class, fields = {
				@FieldResult(name = "productId", column = "id"),
				@FieldResult(name = "productName", column = "product_name"),
				@FieldResult(name = "angsuran", column = "angsuran"),
				@FieldResult(name = "maturityDate", column = "due_date"),
				@FieldResult(name = "plafond", column = "plafond"),
				@FieldResult(name = "tenor", column = "tenor")
		}) })

@Entity
public class ProductEntity {

@Id private Long productId;
	private String productName;
	private Integer angsuran;
	private Date maturityDate;
	private Long plafond;
	private Long tenor;
	
	
	public Long getProduct_id() {
		return productId;
	}
	public void setProduct_id(Long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getAngsuran() {
		return angsuran;
	}
	public void setAngsuran(Integer angsuran) {
		this.angsuran = angsuran;
	}
	public Date getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}
	public Long getPlafond() {
		return plafond;
	}
	public void setPlafond(Long plafond) {
		this.plafond = plafond;
	}
	public Long getTenor() {
		return tenor;
	}
	public void setTenor(Long tenor) {
		this.tenor = tenor;
	}
}
