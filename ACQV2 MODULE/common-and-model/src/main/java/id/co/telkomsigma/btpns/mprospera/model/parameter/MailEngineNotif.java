package id.co.telkomsigma.btpns.mprospera.model.parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "M_MAIL_REQUEST_NOTIF")
public class MailEngineNotif extends GenericModel {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String status;
    private String username;
    private String sessionkey;
    private String imei;
    private String recieverName;
    private String emailAddress;
    private String custumerName;
    private String retrievalReferenceNumber;
    private String transmissionDateAndTime;
    private String appidNo;
    private String sentra;
    private String alasan;
    private String mms;
    private Date createdt;
    private String createby;
    private String updateby;
    private Date updatedt;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "sessionkey")
    public String getSessionkey() {
        return sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    @Column(name = "imei")
    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    @Column(name = "recieverName")
    public String getRecieverName() {
        return recieverName;
    }

    public void setRecieverName(String recieverName) {
        this.recieverName = recieverName;
    }

    @Column(name = "email_Address")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Column(name = "custumer_name")
    public String getCustumerName() {
        return custumerName;
    }

    public void setCustumerName(String custumerName) {
        this.custumerName = custumerName;
    }

    @Column(name = "retrieval_reference_number")
    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    @Column(name = "trans_mission_Date_And_Time")
    public String getTransmissionDateAndTime() {
        return transmissionDateAndTime;
    }

    public void setTransmissionDateAndTime(String transmissionDateAndTime) {
        this.transmissionDateAndTime = transmissionDateAndTime;
    }

    @Column(name = "appid_No")
    public String getAppidNo() {
        return appidNo;
    }

    public void setAppidNo(String appidNo) {
        this.appidNo = appidNo;
    }

    @Column(name = "sentra")
    public String getSentra() {
        return sentra;
    }

    public void setSentra(String sentra) {
        this.sentra = sentra;
    }

    @Column(name = "alasan")
    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }

    @Column(name = "mms")
    public String getMms() {
        return mms;
    }

    public void setMms(String mms) {
        this.mms = mms;
    }

    @Column(name = "create_Dt")
	public Date getCreatedt() {
		return createdt;
	}

	public void setCreatedt(Date createdt) {
		this.createdt = createdt;
	}
	
	@Column(name = "create_By")
	public String getCreateby() {
		return createby;
	}

	public void setCreateby(String createby) {
		this.createby = createby;
	}

	@Column(name = "update_By")
	public String getUpdateby() {
		return updateby;
	}

	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}

	@Column(name = "update_Dt")
	public Date getUpdatedt() {
		return updatedt;
	}

	public void setUpdatedt(Date updatedt) {
		this.updatedt = updatedt;
	}
}