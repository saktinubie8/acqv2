package id.co.telkomsigma.btpns.mprospera.model.sw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_DATA_NOS")
public class DataNOS   extends GenericModel {

	private static final long serialVersionUID = 1L;
	
	private Long dataNosMessageId;
	private String formatJson;
	private String retrievalReferenceNumber;
	private String status;
	
	 @Id
	 @Column(name = "id", nullable = false, unique = true)
	 @GeneratedValue
	public Long getDataNosMessageId() {
		return dataNosMessageId;
	}
	public void setDataNosMessageId(Long dataNosMessageId) {
		this.dataNosMessageId = dataNosMessageId;
	}
	
	@Column(name = "Format_Json")	
	public String getFormatJson() {
		return formatJson;
	}
	public void setFormatJson(String formatJson) {
		this.formatJson = formatJson;
	}
	
	@Column(name = "retrieval_Reference_Number")	
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	
	@Column(name = "status")	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
