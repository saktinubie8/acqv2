package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SWResponse extends BaseResponse {

    /* untuk addSW */
    private String swId;
    private String localId;
    private List<ProductIdListResponse> swProducts;
    /* untuk list SW */
    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<ListSWResponse> swList;
    private List<String> deletedSwList;

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public List<ListSWResponse> getSwList() {
        return swList;
    }

    public void setSwList(List<ListSWResponse> swList) {
        this.swList = swList;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public List<ProductIdListResponse> getSwProducts() {
        return swProducts;
    }

    public void setSwProducts(List<ProductIdListResponse> swProducts) {
        this.swProducts = swProducts;
    }

    public List<String> getDeletedSwList() {
        return deletedSwList;
    }

    public void setDeletedSwList(List<String> deletedSwList) {
        this.deletedSwList = deletedSwList;
    }

    @Override
    public String toString() {
        return "SWResponse{" +
                "swId='" + swId + '\'' +
                ", localId='" + localId + '\'' +
                ", swProducts=" + swProducts +
                ", grandTotal='" + grandTotal + '\'' +
                ", currentTotal='" + currentTotal + '\'' +
                ", totalPage='" + totalPage + '\'' +
                ", swList=" + swList +
                '}';
    }

}