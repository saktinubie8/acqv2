package id.co.telkomsigma.btpns.mprospera.request;

public class VisitDetailRequest extends BaseRequest{

	
	private String username;
    private String sessionKey;
    private String imei;
    private String cifNumber;
    private String visitDate;
    private String meetCustDirect;
    private String custBusinessExists;
    private String custBusinessRun;
    private String custApplyLoan;
    private String notApplyLoanReason;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public String getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}

    public String getMeetCustDirect() {
        return meetCustDirect;
    }

    public void setMeetCustDirect(String meetCustDirect) {
        this.meetCustDirect = meetCustDirect;
    }

    public String getCustBusinessExists() {
        return custBusinessExists;
    }

    public void setCustBusinessExists(String custBusinessExists) {
        this.custBusinessExists = custBusinessExists;
    }

    public String getCustBusinessRun() {
        return custBusinessRun;
    }

    public void setCustBusinessRun(String custBusinessRun) {
        this.custBusinessRun = custBusinessRun;
    }

    public String getCustApplyLoan() {
        return custApplyLoan;
    }

    public void setCustApplyLoan(String custApplyLoan) {
        this.custApplyLoan = custApplyLoan;
    }

    public String getNotApplyLoanReason() {
        return notApplyLoanReason;
    }

    public void setNotApplyLoanReason(String notApplyLoanReason) {
        this.notApplyLoanReason = notApplyLoanReason;
    }
}
