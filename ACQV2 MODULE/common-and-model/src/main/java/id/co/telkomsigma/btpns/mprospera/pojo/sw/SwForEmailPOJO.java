package id.co.telkomsigma.btpns.mprospera.pojo.sw;

import java.math.BigDecimal;

public class SwForEmailPOJO {

	private BigDecimal swEmailId;
	private String flagStatusScoring;
	
	
	public BigDecimal getSwEmailId() {
		return swEmailId;
	}
	public void setSwEmailId(BigDecimal swEmailId) {
		this.swEmailId = swEmailId;
	}
	public String getFlagStatusScoring() {
		return flagStatusScoring;
	}
	public void setFlagStatusScoring(String flagStatusScoring) {
		this.flagStatusScoring = flagStatusScoring;
	}
	
	
	
	
	
}
