package id.co.telkomsigma.btpns.mprospera.request;


import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWPOJO;

public class AddSWRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String longitude;
    private String latitude;
    private String action;
    private SWPOJO sw;
    private String totalPendapatan;
    private String localId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public SWPOJO getSw() {
        return sw;
    }

    public void setSw(SWPOJO sw) {
        this.sw = sw;
    }

    public String getTotalPendapatan() {
        return totalPendapatan;
    }

    public void setTotalPendapatan(String totalPendapatan) {
        this.totalPendapatan = totalPendapatan;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

}
