package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class AddPPIRequest extends BaseRequest {

    private String username;
    private String imei;
    private String sessionKey;
    private String longitude;
    private String latitude;
    private String swId;
    private String ppiId;
    private String action;
    private String jumlahAnggotaKeluarga;
    private String keluargaBersekolah;
    private String pendidikanKepalaKeluarga;
    private String pekerjaanKepalaKeluarga;
    private String jenisLantai;
    private String jenisWC;
    private String bahanBakarUtama;
    private String tabungGas;
    private String kulkas;
    private String sepedaMotor;
    private List<FamilyListRequest> familyList;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getPpiId() {
        return ppiId;
    }

    public void setPpiId(String ppiId) {
        this.ppiId = ppiId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getJumlahAnggotaKeluarga() {
        return jumlahAnggotaKeluarga;
    }

    public void setJumlahAnggotaKeluarga(String jumlahAnggotaKeluarga) {
        this.jumlahAnggotaKeluarga = jumlahAnggotaKeluarga;
    }

    public String getKeluargaBersekolah() {
        return keluargaBersekolah;
    }

    public void setKeluargaBersekolah(String keluargaBersekolah) {
        this.keluargaBersekolah = keluargaBersekolah;
    }

    public String getPendidikanKepalaKeluarga() {
        return pendidikanKepalaKeluarga;
    }

    public void setPendidikanKepalaKeluarga(String pendidikanKepalaKeluarga) {
        this.pendidikanKepalaKeluarga = pendidikanKepalaKeluarga;
    }

    public String getPekerjaanKepalaKeluarga() {
        return pekerjaanKepalaKeluarga;
    }

    public void setPekerjaanKepalaKeluarga(String pekerjaanKepalaKeluarga) {
        this.pekerjaanKepalaKeluarga = pekerjaanKepalaKeluarga;
    }

    public String getJenisLantai() {
        return jenisLantai;
    }

    public void setJenisLantai(String jenisLantai) {
        this.jenisLantai = jenisLantai;
    }

    public String getJenisWC() {
        return jenisWC;
    }

    public void setJenisWC(String jenisWC) {
        this.jenisWC = jenisWC;
    }

    public String getBahanBakarUtama() {
        return bahanBakarUtama;
    }

    public void setBahanBakarUtama(String bahanBakarUtama) {
        this.bahanBakarUtama = bahanBakarUtama;
    }

    public String getTabungGas() {
        return tabungGas;
    }

    public void setTabungGas(String tabungGas) {
        this.tabungGas = tabungGas;
    }

    public String getKulkas() {
        return kulkas;
    }

    public void setKulkas(String kulkas) {
        this.kulkas = kulkas;
    }

    public String getSepedaMotor() {
        return sepedaMotor;
    }

    public void setSepedaMotor(String sepedaMotor) {
        this.sepedaMotor = sepedaMotor;
    }

    public List<FamilyListRequest> getFamilyList() {
        return familyList;
    }

    public void setFamilyList(List<FamilyListRequest> familyList) {
        this.familyList = familyList;
    }

}
