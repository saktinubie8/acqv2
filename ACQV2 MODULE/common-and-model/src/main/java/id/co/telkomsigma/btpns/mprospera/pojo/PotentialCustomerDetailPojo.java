package id.co.telkomsigma.btpns.mprospera.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.response.BaseResponse;

public class PotentialCustomerDetailPojo extends BaseResponse{

	private String customerId;
	private String customerName;
	private String cifNumber;
	private String address;
	private String phoneNumber;
	private Integer jmlKunjungan;
	private List<ProductPOJO> product;
	private BigDecimal totalInstallment;
	private BigDecimal pbu;
	private BigDecimal iirMax;
	private List<RecPlafondEntity> listPlafondRecMax;
	
	
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCifNumber() {
		return cifNumber;
	}
	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Integer getJmlKunjungan() {
		return jmlKunjungan;
	}
	public void setJmlKunjungan(Integer jmlKunjungan) {
		this.jmlKunjungan = jmlKunjungan;
	}
	public List<ProductPOJO> getProduct() {
		if(product==null) {
			product = new ArrayList<ProductPOJO>();
		}
		return product;
	}
	public void setProduct(List<ProductPOJO> product) {
		this.product = product;
	}
	public BigDecimal getTotalInstallment() {
		return totalInstallment;
	}
	public void setTotalInstallment(BigDecimal totalInstallment) {
		this.totalInstallment = totalInstallment;
	}
	public BigDecimal getPbu() {
		return pbu;
	}
	public void setPbu(BigDecimal pbu) {
		this.pbu = pbu;
	}
	public BigDecimal getIirMax() {
		return iirMax;
	}
	public void setIirMax(BigDecimal iirMax) {
		this.iirMax = iirMax;
	}
	public List<RecPlafondEntity> getListPlafondRecMax() {
		return listPlafondRecMax;
	}
	public void setListPlafondRecMax(List<RecPlafondEntity> listPlafondRecMax) {
		this.listPlafondRecMax = listPlafondRecMax;
	}
}
