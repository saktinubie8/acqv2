package id.co.telkomsigma.btpns.mprospera.model.terminal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.OrderBy;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "M_TERMINAL")
public class Terminal extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long terminalId;
    private String imei;
    private Date createdDate;
    private String modelNumber;
    private String androidVersion;
    private String androidBuildNumber;
    private String androidKernelVersion;
    private String androidBasebandVersion;
    private String processor;
    private String ram;
    private String gpsVersion;
    private String totalDeviceMemory;
    private String freeDeviceMemory;
    private String applicationVersion;
    private boolean login = false;
    private String pdkProgress;
    private String loanProgress;
    private String mmProgress;
    private String pmProgress;
    private String sdaProgress;
    private String sentraProgress;
    private String swProgress;
    private String surveyProgress;

    /*****************************
     * - Transient Field -
     ****************************/
    private String loginStr;

    private List<TerminalActivity> terminalActivities = new ArrayList<TerminalActivity>();

    @Column(name = "APPLICATION_VERSION", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    @OneToMany(fetch = FetchType.LAZY, targetEntity = TerminalActivity.class)
    @JoinColumn(name = "TERMINAL_ID")
    @OrderBy(clause = "id asc")
    public List<TerminalActivity> getTerminalActivities() {
        return terminalActivities;
    }

    public void setTerminalActivities(List<TerminalActivity> terminalActivities) {
        this.terminalActivities = terminalActivities;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getTerminalId() {
        return terminalId;
    }

    @Column(name = "IMEI", nullable = false, unique = true, length = MAX_LENGTH_IMEI)
    public String getImei() {
        return imei;
    }

    @Column(name = "CREATED_DT", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    @Column(name = "TOTAL_DEVICE_MEMORY", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getTotalDeviceMemory() {
        return totalDeviceMemory;
    }

    @Column(name = "FREE_DEVICE_MEMORY", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getFreeDeviceMemory() {
        return freeDeviceMemory;
    }

    @Column(name = "MODEL_NUMBER", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getModelNumber() {
        return modelNumber;
    }

    @Column(name = "ANDROID_VERSION", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getAndroidVersion() {
        return androidVersion;
    }

    @Column(name = "ANDROID_BUILD_NUMBER", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getAndroidBuildNumber() {
        return androidBuildNumber;
    }

    @Column(name = "ANDROID_KERNEL_VERSION", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getAndroidKernelVersion() {
        return androidKernelVersion;
    }

    @Column(name = "ANDROID_BASEBAND_VERSION", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getAndroidBasebandVersion() {
        return androidBasebandVersion;
    }

    @Column(name = "PROCESSOR", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getProcessor() {
        return processor;
    }

    @Column(name = "RAM", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getRam() {
        return ram;
    }

    @Column(name = "GPS_VERSION", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getGpsVersion() {
        return gpsVersion;
    }

    @Column(name = "IS_LOGIN")
    public boolean isLogin() {
        return login;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public void setAndroidBuildNumber(String androidBuildNumber) {
        this.androidBuildNumber = androidBuildNumber;
    }

    public void setAndroidKernelVersion(String androidKernelVersion) {
        this.androidKernelVersion = androidKernelVersion;
    }

    public void setAndroidBasebandVersion(String androidBasebandVersion) {
        this.androidBasebandVersion = androidBasebandVersion;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public void setGpsVersion(String gpsVersion) {
        this.gpsVersion = gpsVersion;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setTotalDeviceMemory(String totalDeviceMemory) {
        this.totalDeviceMemory = totalDeviceMemory;
    }

    public void setFreeDeviceMemory(String freeDeviceMemory) {
        this.freeDeviceMemory = freeDeviceMemory;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public void setLogin() {
        this.login = loginStr.equalsIgnoreCase("1");
    }

    @Transient
    public String getLoginStr() {
        return loginStr;
    }

    public void setLoginStr(String loginStr) {
        this.loginStr = loginStr;
    }

    public void setLoginStr() {
        if (login) {
            loginStr = "1";
        } else {
            loginStr = "0";
        }
    }

    @Column(name = "loan_progress", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getLoanProgress() {
        return loanProgress;
    }

    public void setLoanProgress(String loanProgress) {
        this.loanProgress = loanProgress;
    }

    @Column(name = "mm_progress", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getMmProgress() {
        return mmProgress;
    }

    public void setMmProgress(String mmProgress) {
        this.mmProgress = mmProgress;
    }

    @Column(name = "pm_progress", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getPmProgress() {
        return pmProgress;
    }

    public void setPmProgress(String pmProgress) {
        this.pmProgress = pmProgress;
    }

    @Column(name = "sda_progress", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getSdaProgress() {
        return sdaProgress;
    }

    public void setSdaProgress(String sdaProgress) {
        this.sdaProgress = sdaProgress;
    }

    @Column(name = "sentra_progress", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getSentraProgress() {
        return sentraProgress;
    }

    public void setSentraProgress(String sentraProgress) {
        this.sentraProgress = sentraProgress;
    }

    @Column(name = "sw_progress", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getSwProgress() {
        return swProgress;
    }

    public void setSwProgress(String swProgress) {
        this.swProgress = swProgress;
    }

    @Column(name = "survey_progress", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getSurveyProgress() {
        return surveyProgress;
    }

    public void setSurveyProgress(String surveyProgress) {
        this.surveyProgress = surveyProgress;
    }

    @Column(name = "pdk_progress", nullable = true, unique = false, length = MAX_LENGTH_GENERIC_STRING)
    public String getPdkProgress() {
        return pdkProgress;
    }

    public void setPdkProgress(String pdkProgress) {
        this.pdkProgress = pdkProgress;
    }

}
