package id.co.telkomsigma.btpns.mprospera.pojo;

import java.util.Date;

public class ProductPOJO {

	private String productName;
	private String angsuran;
	private String maturityDate;
	private String plafond;
	private String tenor;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getAngsuran() {
		return angsuran;
	}
	public void setAngsuran(String angsuran) {
		this.angsuran = angsuran;
	}
	public String getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	public String getPlafond() {
		return plafond;
	}
	public void setPlafond(String plafond) {
		this.plafond = plafond;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
}
