package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class SWProductListRequest {

    private String action;
    private String id;
    private String installmentCount;
    private String installmentFrequency;
    private String installmentUnitFrequency;
    private List<String> longs;
    private String margin;
    private String name;
    private String productId;
    private String reInstallmentCount;
    private String reInstallmentFrequency;
    private String reInstallmentUnitFrequency;
    private String reMargin;
    private String reName;
    private String recommendationProductId;
    private String recommendationSelectedPlafon;
    private String reType;
    private String reWeeklyTenor;
    private String selectedPlafon;
    private String status;
    private String swId;
    private String swLocalId;
    private String swProductId;
    private String syncStatus;
    private String type;
    private String weeklyTenor;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstallmentCount() {
        return installmentCount;
    }

    public void setInstallmentCount(String installmentCount) {
        this.installmentCount = installmentCount;
    }

    public String getInstallmentFrequency() {
        return installmentFrequency;
    }

    public void setInstallmentFrequency(String installmentFrequency) {
        this.installmentFrequency = installmentFrequency;
    }

    public String getInstallmentUnitFrequency() {
        return installmentUnitFrequency;
    }

    public void setInstallmentUnitFrequency(String installmentUnitFrequency) {
        this.installmentUnitFrequency = installmentUnitFrequency;
    }

    public List<String> getLongs() {
        return longs;
    }

    public void setLongs(List<String> longs) {
        this.longs = longs;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReName() {
        return reName;
    }

    public void setReName(String reName) {
        this.reName = reName;
    }

    public String getRecommendationProductId() {
        return recommendationProductId;
    }

    public void setRecommendationProductId(String recommendationProductId) {
        this.recommendationProductId = recommendationProductId;
    }

    public String getRecommendationSelectedPlafon() {
        return recommendationSelectedPlafon;
    }

    public void setRecommendationSelectedPlafon(String recommendationSelectedPlafon) {
        this.recommendationSelectedPlafon = recommendationSelectedPlafon;
    }

    public String getReType() {
        return reType;
    }

    public void setReType(String reType) {
        this.reType = reType;
    }

    public String getReWeeklyTenor() {
        return reWeeklyTenor;
    }

    public void setReWeeklyTenor(String reWeeklyTenor) {
        this.reWeeklyTenor = reWeeklyTenor;
    }

    public String getSelectedPlafon() {
        return selectedPlafon;
    }

    public void setSelectedPlafon(String selectedPlafon) {
        this.selectedPlafon = selectedPlafon;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getSwLocalId() {
        return swLocalId;
    }

    public void setSwLocalId(String swLocalId) {
        this.swLocalId = swLocalId;
    }

    public String getSwProductId() {
        return swProductId;
    }

    public void setSwProductId(String swProductId) {
        this.swProductId = swProductId;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWeeklyTenor() {
        return weeklyTenor;
    }

    public void setWeeklyTenor(String weeklyTenor) {
        this.weeklyTenor = weeklyTenor;
    }

    public String getReInstallmentCount() {
        return reInstallmentCount;
    }

    public void setReInstallmentCount(String reInstallmentCount) {
        this.reInstallmentCount = reInstallmentCount;
    }

    public String getReInstallmentFrequency() {
        return reInstallmentFrequency;
    }

    public void setReInstallmentFrequency(String reInstallmentFrequency) {
        this.reInstallmentFrequency = reInstallmentFrequency;
    }

    public String getReInstallmentUnitFrequency() {
        return reInstallmentUnitFrequency;
    }

    public void setReInstallmentUnitFrequency(String reInstallmentUnitFrequency) {
        this.reInstallmentUnitFrequency = reInstallmentUnitFrequency;
    }

    public String getReMargin() {
        return reMargin;
    }

    public void setReMargin(String reMargin) {
        this.reMargin = reMargin;
    }

    @Override
    public String toString() {
        return "SWProductListRequest{" +
                "action='" + action + '\'' +
                ", id='" + id + '\'' +
                ", installmentCount='" + installmentCount + '\'' +
                ", installmentFrequency='" + installmentFrequency + '\'' +
                ", installmentUnitFrequency='" + installmentUnitFrequency + '\'' +
                ", longs=" + longs +
                ", margin='" + margin + '\'' +
                ", name='" + name + '\'' +
                ", productId='" + productId + '\'' +
                ", reInstallmentCount='" + reInstallmentCount + '\'' +
                ", reInstallmentFrequency='" + reInstallmentFrequency + '\'' +
                ", reInstallmentUnitFrequency='" + reInstallmentUnitFrequency + '\'' +
                ", reMargin='" + reMargin + '\'' +
                ", reName='" + reName + '\'' +
                ", recommendationProductId='" + recommendationProductId + '\'' +
                ", recommendationSelectedPlafon='" + recommendationSelectedPlafon + '\'' +
                ", reType='" + reType + '\'' +
                ", reWeeklyTenor='" + reWeeklyTenor + '\'' +
                ", selectedPlafon='" + selectedPlafon + '\'' +
                ", status='" + status + '\'' +
                ", swId='" + swId + '\'' +
                ", swLocalId='" + swLocalId + '\'' +
                ", swProductId='" + swProductId + '\'' +
                ", syncStatus='" + syncStatus + '\'' +
                ", type='" + type + '\'' +
                ", weeklyTenor='" + weeklyTenor + '\'' +
                '}';
    }

}