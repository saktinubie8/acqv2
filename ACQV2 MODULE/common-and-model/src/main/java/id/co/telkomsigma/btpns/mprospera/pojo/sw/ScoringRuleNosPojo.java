package id.co.telkomsigma.btpns.mprospera.pojo.sw;

public class ScoringRuleNosPojo {
	
	private String scoringRule;
	private String param;
	private String value;
	private String result;
	private String rejectCode;
	private String rejectDesc;
	private String deviationCode;
	private String deviationDesc;
	public String getScoringRule() {
		return scoringRule;
	}
	public void setScoringRule(String scoringRule) {
		this.scoringRule = scoringRule;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getRejectCode() {
		return rejectCode;
	}
	public void setRejectCode(String rejectCode) {
		this.rejectCode = rejectCode;
	}
	public String getRejectDesc() {
		return rejectDesc;
	}
	public void setRejectDesc(String rejectDesc) {
		this.rejectDesc = rejectDesc;
	}
	public String getDeviationCode() {
		return deviationCode;
	}
	public void setDeviationCode(String deviationCode) {
		this.deviationCode = deviationCode;
	}
	public String getDeviationDesc() {
		return deviationDesc;
	}
	public void setDeviationDesc(String deviationDesc) {
		this.deviationDesc = deviationDesc;
	}
	
	

}
