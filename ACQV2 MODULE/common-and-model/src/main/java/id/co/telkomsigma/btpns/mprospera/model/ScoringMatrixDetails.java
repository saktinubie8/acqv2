package id.co.telkomsigma.btpns.mprospera.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "scoring_matrix_details")
public class ScoringMatrixDetails {

	private Long id;
	private Long matrixId;
	private Integer minRow;
	private Integer maxRow;
	private Integer minColumn;
	private Integer maxColumn;
	private Character grade;
	
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "matrix_id", unique = true, nullable = false)
	public Long getMatrixId() {
		return matrixId;
	}
	public void setMatrixId(Long matrixId) {
		this.matrixId = matrixId;
	}
	
	@Column(name = "min_row", unique = true, nullable = false)
	public Integer getMinRow() {
		return minRow;
	}
	public void setMinRow(Integer minRow) {
		this.minRow = minRow;
	}
	
	@Column(name = "max_row", unique = true, nullable = false)
	public Integer getMaxRow() {
		return maxRow;
	}
	public void setMaxRow(Integer maxRow) {
		this.maxRow = maxRow;
	}
	
	@Column(name = "min_column", unique = true, nullable = false)
	public Integer getMinColumn() {
		return minColumn;
	}
	public void setMinColumn(Integer minColumn) {
		this.minColumn = minColumn;
	}
	
	@Column(name = "max_column", unique = true, nullable = false)
	public Integer getMaxColumn() {
		return maxColumn;
	}
	public void setMaxColumn(Integer maxColumn) {
		this.maxColumn = maxColumn;
	}
	
	@Column(name = "grade", unique = true, nullable = false)
	public Character getGrade() {
		return grade;
	}
	public void setGrade(Character grade) {
		this.grade = grade;
	}
	
	
}
