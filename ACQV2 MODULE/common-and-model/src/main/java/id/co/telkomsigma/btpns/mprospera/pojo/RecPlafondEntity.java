package id.co.telkomsigma.btpns.mprospera.pojo;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "RecPlafondEntity", entities = {
		@EntityResult(entityClass = RecPlafondEntity.class, fields = {
				@FieldResult(name = "plafondId", column = "id"),
				@FieldResult(name = "tenor", column = "tenor"),
				@FieldResult(name = "plafond", column = "plafon"),
		}) })

@Entity
public class RecPlafondEntity {

	@Id private Long plafondId;
	private Integer tenor;
	private Integer plafond;
	
	
	public Long getPlafondId() {
		return plafondId;
	}
	public void setPlafondId(Long plafondId) {
		this.plafondId = plafondId;
	}
	public Integer getTenor() {
		return tenor;
	}
	public void setTenor(Integer tenor) {
		this.tenor = tenor;
	}
	public Integer getPlafond() {
		return plafond;
	}
	public void setPlafond(Integer plafond) {
		this.plafond = plafond;
	}
}
