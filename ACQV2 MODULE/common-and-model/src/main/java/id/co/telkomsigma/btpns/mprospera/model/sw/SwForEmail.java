package id.co.telkomsigma.btpns.mprospera.model.sw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_SW_FOR_EMAIL")
public class SwForEmail  extends GenericModel {

	private static final long serialVersionUID = 1L;

	private Long swEmailId;
	private Long swId;
	private String flagStatusScoring;
	private String retrievalReferenceNumber;
	
	 @Id
	 @Column(name = "id", nullable = false, unique = true)
	 @GeneratedValue
	public Long getSwEmailId() {
		return swEmailId;
	}
	public void setSwEmailId(Long swEmailId) {
		this.swEmailId = swEmailId;
	}
	
	@Column(name = "sw_id")	
    public Long getSwId() {
		return swId;
	}
	public void setSwId(Long swId) {
		this.swId = swId;
	}
	
	@Column(name = "flag_Status_Scoring")
	public String getFlagStatusScoring() {
		return flagStatusScoring;
	}
	public void setFlagStatusScoring(String flagStatusScoring) {
		this.flagStatusScoring = flagStatusScoring;
	}
	
	@Column(name = "retrieval_reference_number")
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	
	
	
	
	
	
}
