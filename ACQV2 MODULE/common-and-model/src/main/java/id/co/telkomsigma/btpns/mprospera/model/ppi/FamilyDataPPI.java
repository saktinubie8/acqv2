package id.co.telkomsigma.btpns.mprospera.model.ppi;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "T_FAMILY_DATA_PPI")
public class FamilyDataPPI extends GenericModel {

    private Long familyDataPpi;
    private String familyDataPpiLocalId;
    private Long ppiId;
    private String name;
    private String familyStatus;
    private Character hasDifferentBusiness;
    private Integer age;
    private Character stillInSchool;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;
    private String deletedBy;
    private Date deletedDate;
    private Boolean isDeleted = false;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, unique = true)
    public Long getFamilyDataPpi() {
        return familyDataPpi;
    }

    public void setFamilyDataPpi(Long familyDataPpi) {
        this.familyDataPpi = familyDataPpi;
    }

    @Column(name = "ppi_id")
    public Long getPpiId() {
        return ppiId;
    }

    public void setPpiId(Long ppiId) {
        this.ppiId = ppiId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "family_status")
    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    @Column(name = "has_different_business")
    public Character getHasDifferentBusiness() {
        return hasDifferentBusiness;
    }

    public void setHasDifferentBusiness(Character hasDifferentBusiness) {
        this.hasDifferentBusiness = hasDifferentBusiness;
    }

    @Column(name = "age")
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Column(name = "still_in_school")
    public Character getStillInSchool() {
        return stillInSchool;
    }

    public void setStillInSchool(Character stillInSchool) {
        this.stillInSchool = stillInSchool;
    }

    @Column(name = "is_deleted")
    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Column(name = "local_id")
    public String getFamilyDataPpiLocalId() {
        return familyDataPpiLocalId;
    }

    public void setFamilyDataPpiLocalId(String familyDataPpiLocalId) {
        this.familyDataPpiLocalId = familyDataPpiLocalId;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "deleted_by")
    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Column(name = "deleted_date")
    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

}
