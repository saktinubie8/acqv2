package id.co.telkomsigma.btpns.mprospera.model.product;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "T_PLAFON_MAPPING")
public class PlafonMapping {

    private Long id;
    private Integer tenor;
    private BigDecimal plafon;
    private BigDecimal monthlyInstallment;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, unique = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "tenor")
    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    @Column(name = "plafon")
    public BigDecimal getPlafon() {
        return plafon;
    }

    public void setPlafon(BigDecimal plafon) {
        this.plafon = plafon;
    }

    @Column(name = "monthly_installment")
    public BigDecimal getMonthlyInstallment() {
        return monthlyInstallment;
    }

    public void setMonthlyInstallment(BigDecimal monthlyInstallment) {
        this.monthlyInstallment = monthlyInstallment;
    }

}