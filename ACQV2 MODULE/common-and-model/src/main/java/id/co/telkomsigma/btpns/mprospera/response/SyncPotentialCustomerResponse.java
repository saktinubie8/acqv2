package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.customer.PotentialCustomer;
import id.co.telkomsigma.btpns.mprospera.pojo.PotentialCustomerPOJO;

public class SyncPotentialCustomerResponse extends BaseResponse {

	List<PotentialCustomerPOJO> customerList;

	public List<PotentialCustomerPOJO> getCustomerList() {
		if(customerList==null) {
			customerList = new ArrayList<PotentialCustomerPOJO>();
		}
		return customerList;
	}

	public void setCustomerList(List<PotentialCustomerPOJO> customerList) {
		this.customerList = customerList;
	}
}
