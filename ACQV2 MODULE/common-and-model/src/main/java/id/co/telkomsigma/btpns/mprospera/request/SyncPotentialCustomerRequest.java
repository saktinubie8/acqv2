package id.co.telkomsigma.btpns.mprospera.request;

public class SyncPotentialCustomerRequest extends BaseRequest{

	private String username;
    private String sessionKey;
    private String imei;
    private String getCountData;
    private String page;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(String getCountData) {
        this.getCountData = getCountData;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
