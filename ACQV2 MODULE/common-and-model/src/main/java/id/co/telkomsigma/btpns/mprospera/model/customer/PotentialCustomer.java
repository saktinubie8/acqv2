package id.co.telkomsigma.btpns.mprospera.model.customer;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "PotentialCustomer", entities = {
		@EntityResult(entityClass = PotentialCustomer.class, fields = {
				@FieldResult(name = "customerId", column = "id"),
				@FieldResult(name = "customerName", column = "name"),
				@FieldResult(name = "cifNumber", column = "cif_number"),
				@FieldResult(name = "maturityDate", column = "maturity_date"),
				@FieldResult(name = "productName", column = "product_name"),
				@FieldResult(name = "plafond", column = "plafond"),
				@FieldResult(name = "sentraName", column = "sentra_Name"),
				@FieldResult(name = "numberOfVisit", column = "jumlah_kunjungan"),
				@FieldResult(name = "status", column = "status"),
		}) })

@Entity
public class PotentialCustomer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7448425909257316663L;
@Id protected Long customerId;
	private String customerName;
	private String cifNumber;
	private Date maturityDate;
	private String productName;
	private Integer plafond;
	private String sentraName;
	private Integer numberOfVisit;
	private String status;
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getPlafond() {
		return plafond;
	}

	public void setPlafond(Integer plafond) {
		this.plafond = plafond;
	}

	public String getSentraName() {
		return sentraName;
	}

	public void setSentraName(String sentraName) {
		this.sentraName = sentraName;
	}
	
	public Integer getNumberOfVisit() {
		return numberOfVisit;
	}
	public void setNumberOfVisit(Integer numberOfVisit) {
		this.numberOfVisit = numberOfVisit;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
