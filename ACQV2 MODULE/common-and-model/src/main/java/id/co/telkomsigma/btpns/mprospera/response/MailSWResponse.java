package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWSentEmailPOJO;

public class MailSWResponse extends BaseResponse{
    private String swId;
    private String localId;
    
    private SWSentEmailPOJO swRequestEmail;

	public String getSwId() {
		return swId;
	}

	public void setSwId(String swId) {
		this.swId = swId;
	}

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(String localId) {
		this.localId = localId;
	}

	public SWSentEmailPOJO getSwRequestEmail() {
		return swRequestEmail;
	}

	public void setSwRequestEmail(SWSentEmailPOJO swRequestEmail) {
		this.swRequestEmail = swRequestEmail;
	}


}
