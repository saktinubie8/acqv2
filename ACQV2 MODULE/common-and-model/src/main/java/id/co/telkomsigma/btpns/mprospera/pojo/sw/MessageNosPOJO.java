package id.co.telkomsigma.btpns.mprospera.pojo.sw;

import java.util.List;

public class MessageNosPOJO {
	
	private String transmissionDateAndTime;
	private String retrievalReferenceNumber;
	private String channelDstn;
	private String status;
	private List<ScoringRuleNosPojo>scoringResult;
	
	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}
	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	public String getChannelDstn() {
		return channelDstn;
	}
	public void setChannelDstn(String channelDstn) {
		this.channelDstn = channelDstn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<ScoringRuleNosPojo> getScoringResult() {
		return scoringResult;
	}
	public void setScoringResult(List<ScoringRuleNosPojo> scoringResult) {
		this.scoringResult = scoringResult;
	}
	
	

}
