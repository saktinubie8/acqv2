package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.telkomsigma.btpns.mprospera.model.sw.SwForEmail;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWPOJO;

@SuppressWarnings("all")
public class SWForEmailResponse extends BaseResponse {

	//"responseCode":null,"responseMessage":null,"retrievalReferenceNumber":null,
	private String financingReferenceId;
	private String channelId;
	private String cif;
	private String birthDate;
	private String maritalStatus;
	private String productCode; 
	private boolean businessFlag;
	private BigDecimal plafond;
	private Integer tenor;
	private String applicationDate;
	private String notes;
	private String financingStatus;
	private String disbursementDate;
	private String noAppid;
	private boolean deviationFlag;
	
	public String getFinancingReferenceId() {
		return financingReferenceId;
	}
	public void setFinancingReferenceId(String financingReferenceId) {
		this.financingReferenceId = financingReferenceId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getCif() {
		return cif;
	}
	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public boolean isBusinessFlag() {
		return businessFlag;
	}
	public void setBusinessFlag(boolean businessFlag) {
		this.businessFlag = businessFlag;
	}
	public BigDecimal getPlafond() {
		return plafond;
	}
	public void setPlafond(BigDecimal plafond) {
		this.plafond = plafond;
	}
	public Integer getTenor() {
		return tenor;
	}
	public void setTenor(Integer tenor) {
		this.tenor = tenor;
	}
	public String getApplicationDate() {
		return applicationDate;
	}
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getFinancingStatus() {
		return financingStatus;
	}
	public void setFinancingStatus(String financingStatus) {
		this.financingStatus = financingStatus;
	}	
	public String getDisbursementDate() {
		return disbursementDate;
	}
	public void setDisbursementDate(String disbursementDate) {
		this.disbursementDate = disbursementDate;
	}
	public String getNoAppid() {
		return noAppid;
	}
	public void setNoAppid(String noAppid) {
		this.noAppid = noAppid;
	}
	public boolean isDeviationFlag() {
		return deviationFlag;
	}
	public void setDeviationFlag(boolean deviationFlag) {
		this.deviationFlag = deviationFlag;
	}


	
}
