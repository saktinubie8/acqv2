package id.co.telkomsigma.btpns.mprospera.response;

import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWPOJO;

public class SWDetailResponse extends BaseResponse {

    private SWPOJO swDetail;

    public SWPOJO getSwDetail() {
        return swDetail;
    }

    public void setSwDetail(SWPOJO swDetail) {
        this.swDetail = swDetail;
    }

}
