package id.co.telkomsigma.btpns.mprospera.pojo.sw;

public class SentEmailNotifAfterNosPOJO {
	
	private String transmissionDateAndTime;
	private String retrievalReferenceNumber;
	private String userName;
	private String emailAddress;
	private String customerName;
	private String appidNo;
	private String sentra;
	private String alasan;
	private String mms;
	
	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}
	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAppidNo() {
		return appidNo;
	}
	public void setAppidNo(String appidNo) {
		this.appidNo = appidNo;
	}
	public String getSentra() {
		return sentra;
	}
	public void setSentra(String sentra) {
		this.sentra = sentra;
	}
	public String getAlasan() {
		return alasan;
	}
	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}
	public String getMms() {
		return mms;
	}
	public void setMms(String mms) {
		this.mms = mms;
	}
}
