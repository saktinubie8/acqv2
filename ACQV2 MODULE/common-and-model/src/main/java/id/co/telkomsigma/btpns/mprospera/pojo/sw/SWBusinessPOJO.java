package id.co.telkomsigma.btpns.mprospera.pojo.sw;

public class SWBusinessPOJO {

    private String address;
    private String ageMonth;
    private String ageYear;
    private String businessLocation;
    private String businessOwnership;
    private String businessRunner;
    private String businessShariaCompliance;
    private String desc;
    private String name;
    private String type;
    private String hasOtherBankLoan;
    private String otherBankName;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAgeMonth() {
        return ageMonth;
    }

    public void setAgeMonth(String ageMonth) {
        this.ageMonth = ageMonth;
    }

    public String getAgeYear() {
        return ageYear;
    }

    public void setAgeYear(String ageYear) {
        this.ageYear = ageYear;
    }

    public String getBusinessLocation() {
        return businessLocation;
    }

    public void setBusinessLocation(String businessLocation) {
        this.businessLocation = businessLocation;
    }

    public String getBusinessOwnership() {
        return businessOwnership;
    }

    public void setBusinessOwnership(String businessOwnership) {
        this.businessOwnership = businessOwnership;
    }

    public String getBusinessRunner() {
        return businessRunner;
    }

    public void setBusinessRunner(String businessRunner) {
        this.businessRunner = businessRunner;
    }

    public String getBusinessShariaCompliance() {
        return businessShariaCompliance;
    }

    public void setBusinessShariaCompliance(String businessShariaCompliance) {
        this.businessShariaCompliance = businessShariaCompliance;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHasOtherBankLoan() {
        return hasOtherBankLoan;
    }

    public void setHasOtherBankLoan(String hasOtherBankLoan) {
        this.hasOtherBankLoan = hasOtherBankLoan;
    }

    public String getOtherBankName() {
        return otherBankName;
    }

    public void setOtherBankName(String otherBankName) {
        this.otherBankName = otherBankName;
    }

}
