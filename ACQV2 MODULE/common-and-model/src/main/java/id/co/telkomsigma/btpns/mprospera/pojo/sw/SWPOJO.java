package id.co.telkomsigma.btpns.mprospera.pojo.sw;

import id.co.telkomsigma.btpns.mprospera.pojo.WismaPOJO;

import java.util.List;

public class SWPOJO {

    private String swId;
    private String swPPI;
    private String hasPPI;
    private String isNasabahPotensial;
    private String swLocalId;
    private String swLocation;
    private String status;
    private String action;
    private String page;
    private String appId;
    private String createdAt;
    private String createdBy;
    private String updatedAt;
    private String dayLight;
    private String flag;
    private String sentraId;
    private String sentraName;
    private String groupId;
    private String groupName;
    private String customerCif;
    private String customerId;
    private String customerName;
    private String customerType;
    private String customerExists;
    private String loanExists;
    private String idCardName;
    private String idCardNumber;
    private String pmId;
    private String productString;
    private String rejectedReason;
    private String surveyDate;
    private String disbursementDate;
    private String syncStatus;
    private String totalPendapatan;
    private String income;
    private String netIncome;
    private String spending;
    private String hasApprovedMs;
    private String hasBusinessPlacePhoto;
    private String hasIdPhoto;
    private String iir;
    private WismaPOJO wisma;
    private SWProfilePOJO profile;
    private SWAddressPOJO address;
    private SWBusinessPOJO business;
    private SWCalculationVariablePOJO calcVariable;
    private SWExpensePOJO expense;
    private SWSpousePOJO partner;
    private SWBusinessPlacePhotoPOJO businessImageFile;
    private SWIDPhotoPOJO idImageFile;
    private List<SWAWGMBuyPOJO> awgmPurchasing;
    private List<SWDirectBuyPOJO> directPurchasing;
    private List<SWNeighborRecommendationPOJO> referenceList;
    private List<SWProductMapPOJO> swProducts;
    private List<SWApprovalHistoryPOJO> approvalHistories;

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getSwPPI() {
        return swPPI;
    }

    public void setSwPPI(String swPPI) {
        this.swPPI = swPPI;
    }

    public String getSwLocalId() {
        return swLocalId;
    }

    public void setSwLocalId(String swLocalId) {
        this.swLocalId = swLocalId;
    }

    public String getSwLocation() {
        return swLocation;
    }

    public void setSwLocation(String swLocation) {
        this.swLocation = swLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDayLight() {
        return dayLight;
    }

    public void setDayLight(String dayLight) {
        this.dayLight = dayLight;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCustomerCif() {
        return customerCif;
    }

    public void setCustomerCif(String customerCif) {
        this.customerCif = customerCif;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerExists() {
        return customerExists;
    }

    public void setCustomerExists(String customerExists) {
        this.customerExists = customerExists;
    }

    public String getLoanExists() {
        return loanExists;
    }

    public void setLoanExists(String loanExists) {
        this.loanExists = loanExists;
    }

    public String getIdCardName() {
        return idCardName;
    }

    public void setIdCardName(String idCardName) {
        this.idCardName = idCardName;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public String getProductString() {
        return productString;
    }

    public void setProductString(String productString) {
        this.productString = productString;
    }

    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getTotalPendapatan() {
        return totalPendapatan;
    }

    public void setTotalPendapatan(String totalPendapatan) {
        this.totalPendapatan = totalPendapatan;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(String netIncome) {
        this.netIncome = netIncome;
    }

    public String getSpending() {
        return spending;
    }

    public void setSpending(String spending) {
        this.spending = spending;
    }

    public String getHasApprovedMs() {
        return hasApprovedMs;
    }

    public void setHasApprovedMs(String hasApprovedMs) {
        this.hasApprovedMs = hasApprovedMs;
    }

    public String getHasBusinessPlacePhoto() {
        return hasBusinessPlacePhoto;
    }

    public void setHasBusinessPlacePhoto(String hasBusinessPlacePhoto) {
        this.hasBusinessPlacePhoto = hasBusinessPlacePhoto;
    }

    public String getHasIdPhoto() {
        return hasIdPhoto;
    }

    public void setHasIdPhoto(String hasIdPhoto) {
        this.hasIdPhoto = hasIdPhoto;
    }

    public WismaPOJO getWisma() {
        return wisma;
    }

    public void setWisma(WismaPOJO wisma) {
        this.wisma = wisma;
    }

    public SWProfilePOJO getProfile() {
        return profile;
    }

    public void setProfile(SWProfilePOJO profile) {
        this.profile = profile;
    }

    public SWAddressPOJO getAddress() {
        return address;
    }

    public void setAddress(SWAddressPOJO address) {
        this.address = address;
    }

    public SWBusinessPOJO getBusiness() {
        return business;
    }

    public void setBusiness(SWBusinessPOJO business) {
        this.business = business;
    }

    public SWCalculationVariablePOJO getCalcVariable() {
        return calcVariable;
    }

    public void setCalcVariable(SWCalculationVariablePOJO calcVariable) {
        this.calcVariable = calcVariable;
    }

    public SWExpensePOJO getExpense() {
        return expense;
    }

    public void setExpense(SWExpensePOJO expense) {
        this.expense = expense;
    }

    public SWSpousePOJO getPartner() {
        return partner;
    }

    public void setPartner(SWSpousePOJO partner) {
        this.partner = partner;
    }

    public SWBusinessPlacePhotoPOJO getBusinessImageFile() {
        return businessImageFile;
    }

    public void setBusinessImageFile(SWBusinessPlacePhotoPOJO businessImageFile) {
        this.businessImageFile = businessImageFile;
    }

    public SWIDPhotoPOJO getIdImageFile() {
        return idImageFile;
    }

    public void setIdImageFile(SWIDPhotoPOJO idImageFile) {
        this.idImageFile = idImageFile;
    }

    public List<SWAWGMBuyPOJO> getAwgmPurchasing() {
        return awgmPurchasing;
    }

    public void setAwgmPurchasing(List<SWAWGMBuyPOJO> awgmPurchasing) {
        this.awgmPurchasing = awgmPurchasing;
    }

    public List<SWDirectBuyPOJO> getDirectPurchasing() {
        return directPurchasing;
    }

    public void setDirectPurchasing(List<SWDirectBuyPOJO> directPurchasing) {
        this.directPurchasing = directPurchasing;
    }

    public List<SWNeighborRecommendationPOJO> getReferenceList() {
        return referenceList;
    }

    public void setReferenceList(List<SWNeighborRecommendationPOJO> referenceList) {
        this.referenceList = referenceList;
    }

    public List<SWProductMapPOJO> getSwProducts() {
        return swProducts;
    }

    public void setSwProducts(List<SWProductMapPOJO> swProducts) {
        this.swProducts = swProducts;
    }

    public List<SWApprovalHistoryPOJO> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<SWApprovalHistoryPOJO> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

    public String getHasPPI() {
        return hasPPI;
    }

    public void setHasPPI(String hasPPI) {
        this.hasPPI = hasPPI;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public String getIsNasabahPotensial() {
        return isNasabahPotensial;
    }

    public void setIsNasabahPotensial(String isNasabahPotensial) {
        this.isNasabahPotensial = isNasabahPotensial;
    }

    public String getIir() {
        return iir;
    }

    public void setIir(String iir) {
        this.iir = iir;
    }
}