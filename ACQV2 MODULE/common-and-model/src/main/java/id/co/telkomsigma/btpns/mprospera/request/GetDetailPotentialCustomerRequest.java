package id.co.telkomsigma.btpns.mprospera.request;

public class GetDetailPotentialCustomerRequest extends BaseRequest{
	
	private String username;
    private String sessionKey;
    private String imei;
    private String cifNumber;
    private String maturityFlag;
    private String topUpFlag;
    private String dropOutFlag;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public String getMaturityFlag() {
		return maturityFlag;
	}

	public void setMaturityFlag(String maturityFlag) {
		this.maturityFlag = maturityFlag;
	}

	public String getTopUpFlag() {
		return topUpFlag;
	}

	public void setTopUpFlag(String topUpFlag) {
		this.topUpFlag = topUpFlag;
	}

	public String getDropOutFlag() {
		return dropOutFlag;
	}

	public void setDropOutFlag(String dropOutFlag) {
		this.dropOutFlag = dropOutFlag;
	}
}
