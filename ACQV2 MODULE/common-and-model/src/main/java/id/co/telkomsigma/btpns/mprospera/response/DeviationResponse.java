package id.co.telkomsigma.btpns.mprospera.response;

import id.co.telkomsigma.btpns.mprospera.pojo.WismaPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.deviation.DeviationApprovalHistoryPOJO;

import java.util.List;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
public class DeviationResponse extends BaseResponse {

    private String deviationId;
    private String status;
    private String createdBy;
    private String createdDate;
    private String loanId;
    private String ap3rId;
    private String deviationReason;
    private String deviationCode;
    private String localId;
    private String batch;
    private WismaPOJO wisma;
    private List<DeviationApprovalHistoryPOJO> approvalHistories;
    private List<String> deletedIds;
    private List<String> deletedSwList;
    private List<String> deletedCustomerList;

    // forList
    private List<DeviationResponse> deviationList;

    public String getDeviationId() {
        return deviationId;
    }

    public void setDeviationId(String deviationId) {
        this.deviationId = deviationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public List<DeviationResponse> getDeviationList() {
        return deviationList;
    }

    public void setDeviationList(List<DeviationResponse> deviationList) {
        this.deviationList = deviationList;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public List<DeviationApprovalHistoryPOJO> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<DeviationApprovalHistoryPOJO> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public String getDeviationReason() {
        return deviationReason;
    }

    public void setDeviationReason(String deviationReason) {
        this.deviationReason = deviationReason;
    }

    public String getDeviationCode() {
        return deviationCode;
    }

    public void setDeviationCode(String deviationCode) {
        this.deviationCode = deviationCode;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public WismaPOJO getWisma() {
        return wisma;
    }

    public void setWisma(WismaPOJO wisma) {
        this.wisma = wisma;
    }

    public List<String> getDeletedIds() {
        return deletedIds;
    }

    public void setDeletedIds(List<String> deletedIds) {
        this.deletedIds = deletedIds;
    }

    public List<String> getDeletedSwList() {
        return deletedSwList;
    }

    public void setDeletedSwList(List<String> deletedSwList) {
        this.deletedSwList = deletedSwList;
    }

    public List<String> getDeletedCustomerList() {
        return deletedCustomerList;
    }

    public void setDeletedCustomerList(List<String> deletedCustomerList) {
        this.deletedCustomerList = deletedCustomerList;
    }

    @Override
    public String toString() {
        return "DeviationResponse{" +
                "deviationId='" + deviationId + '\'' +
                ", status='" + status + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", loanId='" + loanId + '\'' +
                ", ap3rId='" + ap3rId + '\'' +
                ", deviationReason='" + deviationReason + '\'' +
                ", deviationCode='" + deviationCode + '\'' +
                ", localId='" + localId + '\'' +
                ", batch='" + batch + '\'' +
                ", wisma=" + wisma +
                ", approvalHistories=" + approvalHistories +
                ", deletedIds=" + deletedIds +
                ", deletedSwList=" + deletedSwList +
                ", deletedCustomerList=" + deletedCustomerList +
                ", deviationList=" + deviationList +
                '}';
    }

}
