package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class SyncPPIResponse extends BaseResponse {

    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<String> deletedPPIList;
    private List<PPIResponse> ppiList;


    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<String> getDeletedPPIList() {
        return deletedPPIList;
    }

    public void setDeletedPPIList(List<String> deletedPPIList) {
        this.deletedPPIList = deletedPPIList;
    }

    public List<PPIResponse> getPpiList() {
        return ppiList;
    }

    public void setPpiList(List<PPIResponse> ppiList) {
        this.ppiList = ppiList;
    }

}